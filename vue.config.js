module.exports = {
  lintOnSave: false,
  transpileDependencies: [
    'vuetify',
  ],
  chainWebpack: (config) => {
    config
      .plugin('html')
      .tap((args) => {
        // eslint-disable-next-line no-param-reassign
        args[0].title = 'Határtalan Skanzen';
        return args;
      });
    config.module.rule('pdf')
      .test(/\.pdf$/)
      .use('file-loader').loader('file-loader');
  },
};
