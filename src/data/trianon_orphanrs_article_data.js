const data = [{
  id: 0,
  title: '',
  name: 'Trianon árvái',
  content: [
    'Az Osztrák–Magyar Monarchia felbomlása után a régi-új utódállamok már az <b style="cursor: pointer;" onclick="onImage(1)">1920. június 4-én megkötött trianoni békeszerződés</b> előtt megkezdték a magyar területek elfoglalását, aminek következtében 1918 végén tömeges és nagyrészt kényszerű menekülés kezdődött meg az ország belseje felé.',
    'Az 1920-ban létrehozott Országos Menekültügyi Hivatal statisztikája szerint az összes elcsatolt területről 1924-ig nagyjából 350 ezer ember hagyta el otthonát és jött a trianoni határok által kijelölt országrészbe, utólagos becslések ezt a számot 425 ezerre teszik. Közülük nagyjából 20–20 ezren voltak az egykori államapparátust működtető <b style="cursor: pointer;" onclick="onImage(2)">hivatalnokok </b>, valamint <b style="cursor: pointer;" onclick="onImage(3)">vasúti alkalmazottak</b>. A határon túlra került országrészekben a magyar közalkalmazottaknak ahhoz, hogy továbbra is betölthessék állásaikat, hűségesküt kellett tenniük a Csehszlovák Köztársaságra vagy a Román vagy a Szerb–Horvát–Szlovén Királyságra. Akik ezt megtagadták, azokat kiutasították és vagyonukat vesztve menekülniük kellett, mások pedig „önként” hagyták hátra – sokszor <b style="cursor: pointer;" onclick="onImage(4)">vasúti tehervagonokban </b> – egész addigi életüket, szülőföldjüket, gyökereiket. Nincsenek pontos adatok arról, hogy hányan követték hivatalukat az új határok közé, hány főt utasítottak ki a csehek, a románok és a szerbek, és hogy mennyien voltak azok, akik „csak” elhagyták szülőföldjüket.',
    'Az országba özönlő menekültek nagyobb részének az állam képtelen volt lakást biztosítani. Sokan <b style="cursor: pointer;" onclick="onImage(5)">pályaudvarokon félretolt tehervagonokban </b> húzták meg magukat, akár évekig. Budapest mellett a nagyobb vidéki városok: Szolnok, Kecskemét, Nagykanizsa, Barcs vagy Debrecen vasútállomásai is sok vagonlakónak adtak otthont. A vágányok mellett új, zsúfolt városrészek jelentek meg. A menekültek többnyire átmenetinek gondolták helyzetüket, és egészen 1920. június 4-ig reménykedtek a hazatérésben. <b style="cursor: pointer;" onclick="onImage(6)">A vagonok mindennapi életre teljesen alkalmatlanok voltak</b>: nyáron felforrósodtak, <b style="cursor: pointer;" onclick="onImage(7)">télen</b> pedig nem lehetett őket rendesen befűteni. A szerencsésebbeknek két kocsi is jutott, az egyikben „éltek”, a másikban pedig az átmenekített bútoraikat tárolták. A nehézségeket fokozta a víz és a tüzelőanyag beszerzésének problémája, az illemhelyek hiánya, a folyamatos füst- és koromszennyezés és a tűzveszély. Nem egy vagon kapott lángra a kályhákból kipattanó szikra miatt. Közművek híján a sínek közé öntötték a hulladékot és a szennyvizet, így a patkányok és az élősködők elszaporodása a fertőző betegségek, például a tífusz és a vérhas terjedéséhez vezettek. <b style="cursor: pointer;" onclick="onImage(8)">Az életet a világítás nélküli, sáros és télen hideg esték tették még unalmasabbá és elviselhetetlenebbé. </b> A kocsikban sokszor több generáció élt együtt, a tiltások ellenére számos család albérlőt is fogadott. 1920 után néhányan már bérraktárban tárolták a magukkal hozott ingóságokat, ami némileg enyhítette a zsúfoltságot. A munkához jutó férfiak a kocsikból jártak be dolgozni, az asszonyok itt mostak és főztek, a gyermekek pedig innen mentek iskolába. A síneken, a vagonvárosok „utcáin” játszó <b style="cursor: pointer;" onclick="onImage(9)">gyermekek</b><b style="cursor: pointer;" onclick="onImage(10)">(a)</b>  közül sokan lelték halálukat a tolatómozdonyok balesetei miatt. A vagonlakók a tilalom ellenére háziállatok tartásával is próbálkoztak. A nélkülözés miatt sokszor a közelükben található földek termését tizedelték meg, vagy a környékről szereztek tűzifát. Az ország lakossága számos alkalommal adta tanúbizonyságát önzetlenségének, a menekülők részére több jótékonysági akciót szerveztek, készpénzsegélyeket gyűjtöttek, a környék lakói pedig élelmet is vittek számukra. Voltak azonban olyanok is, akik hasznot próbáltak húzni a menekültek kiszolgáltatott helyzetéből.',
    'Nem minden menekült volt egyúttal vagonlakó. Összesen 40–50 ezren lehettek – azonban 16 ezer főnél többen egyszerre sosem – azok, akik vasútállomásokon félretolt teherkocsikban tengették mindennapjaikat. A menekültek egy másik része <b style="cursor: pointer;" onclick="onImage(11)">tömegszállásokon és gyorsan hírhedtté váló külvárosi barakktelepekenM</b> vagy rokonoknál lelt menedéket, esetleg minden ingatlanukat és ingóságukat eladva új házat tudtak venni. ',
    'A menekültek tömegeinek beilleszkedése a trianoni magyar társadalomba hosszan tartó és sok feszültséggel járó folyamat volt. Integrációjuk ugyanakkor nemcsak a kudarcról szólt, hiszen többeknek sikerült korábbi beosztása szerint – vagy teljesen új munkakörben – állást találnia, így pedig sikerrel alkalmazkodnia a megváltozott körülményekhez.',
    'A húszas évek végére a MÁV vagonjai lassan felszabadultak, a beköltözési moratórium és a szükséglakások építésének hatására a vagonlakók száma <b style="cursor: pointer;" onclick="onImage(12)">1922-től csökkenni kezdett.</b> Az utolsó vagonlakó 1927 februárjában hagyhatta el a vasúti kocsiját.',
    '2019-ben a Trianon 100 MTA-Lendület Kutatócsoport – együttműködésben az adatbázis összeállítójával, Dékány Istvánnal, a Trianoni árvák című könyv szerzőjével és a Noran Libro kiadóval – egy több mint 15 ezer tételből álló adatbázist tett közzé a <a href="http://trianon100.hu/menekultek" target="_blank">http://trianon100.hu/menekultek</a> oldalon, mely nemcsak az 1918 és 1928 között Magyarországra érkezett menekültek nevét, hanem korábbi lakóhelyüket, foglalkozásukat és az érkezésük pontos helyét is kereshető formában tárja az érdeklődők elé.',
    'Virtuális kiállításunkban személyes történeteken keresztül mutatunk be megtörtént eseteket. Dokumentumok, fotók, és interjúk segítségével mutatjuk be a szülőföldjüket kényszerűségből elhagyók életét.',
  ],
  slug: 'trianoni-arvak',
  thumbnail: '/trianon_images/mozdony_foszoveg/1a_THUMBNAIL.jpg',
  images: [{
    src: '/trianon_images/mozdony_foszoveg/1a.jpg',
    description: '(1) a) A trianoni békeszerződés, 1920. (MTA BTK TTI)',
  },
  {
    src: '/trianon_images/mozdony_foszoveg/1b.jpg',
    description: '(1) b) A békeszerződés magyar aláírói: Benárd Ágost (balra, cilinderrel a kezében) és Drasche-Lázár Alfréd (jobbra, fedetlen fővel) távoznak az aláírás után a versailles-i Nagy Trianon kastélyból, 1920. június 4. (Wikimedia Commons/National Library of France)',
  },
  {
    src: '/trianon_images/mozdony_foszoveg/2.jpg',
    description: '(2) Vagonlakó és családja az egyik pályaudvaron, 1919–1920. (Fővárosi Szabó Ervin Könyvtár, Budapest gyűjtemény, 021009)',
  },
  {
    src: '/trianon_images/mozdony_foszoveg/3.jpg',
    description: '(3) Menekültek egy tehervagon előtt, 1919 ősz. Harsányi Gyula felvétele. (Magyar Nemzeti Múzeum Történeti Fényképtár, 2494/1957 fk)',
  },
  {
    src: '/trianon_images/mozdony_foszoveg/4.png',
    description: '(4) MÁV G 120000 sorozatú, G 132487 pályaszámú, kéttengelyes, fékbódé nélküli, fedett vasúti teherkocsi. Raksúlya: 15 tonna, raktérfogata: 43,2 köbméter. A sorozat kocsijait 1915–1926 között gyártották. Főleg ilyen kocsik alkották az 1920. június 4-én megkötött trianoni békeszerződés után az elcsatolt területekről az anyaországra menekülők vagonlakásait. (Magyar Műszaki és Közlekedési Múzeum, MMKM TFGY 2016.64.1)',
  },
  {
    src: '/trianon_images/mozdony_foszoveg/5.jpg',
    description: '(5) Menekült vagonlakók a pályaudvar külső vágányán, 1919. ősz. Harsányi Gyula felvétele. (Magyar Nemzeti Múzeum, Történeti Fényképtár, 2770/1957 fk)',
  },
  {
    src: '/trianon_images/mozdony_foszoveg/6.jpg',
    description: '(6) Menekült család a vagonjában, 1920. (Magyar Nemzeti Múzeum Történeti Fényképtár, 2008.253.1.)',
  },
  {
    src: '/trianon_images/mozdony_foszoveg/7.png',
    description: '(7) Helbing Aranka: A Délvidéki Liga gyűjtése a vagonlakók és menekültek gyámolítására, 1920. (Országos Széchényi Könyvtár, Plakát- és Kisnyomtatványtár, PKG.1920/39)',
  },
  {
    src: '/trianon_images/mozdony_foszoveg/8.png',
    description: '(8) Élet a vagonok között, 1920. január. (Vasárnapi Újság, 67. évf. 2. sz. 16. 1920. január 11.)',
  },
  {
    src: '/trianon_images/mozdony_foszoveg/9.jpg',
    description: '(9) Vagonlakó asszony gyermekek gyűrűjében, 1919. december. Vagonlakó asszony gyermekek gyűrűjében, 1919. december. (Képes Krónika, 1919. december 9., I. évf. 10. sz. 292.)',
  },
  {
    src: '/trianon_images/mozdony_foszoveg/10.jpg',
    description: '(10) Vagonlakó menekültek egy pályaudvaron, Budapest, 1920. Müllner János felvétele. (Magyar Nemzeti Múzeum Történeti Fényképtár, 321/1948 fk)',
  },
  {
    src: '/trianon_images/mozdony_foszoveg/11.jpg',
    description: '(11) Előtérben az 1920-as években menekültek százainak otthont adó Mária Valéria telep kislakásos telepe és a József Attila lakótelep az Üllői út felől nézve, 1964. Fortepan/Budapest Főváros Levéltára. Levéltári jelzet: HU_BFL_XV_19_c_11)',
  },
  {
    src: '/trianon_images/mozdony_foszoveg/12.jpg',
    description: '(12) Légifelvétel. Fent a Kerepesi út, átlósan a ceglédi vasútvonal és a Kőbánya teherpályaudvar, közöttük a Lóversenypálya (ma Kincsem Park), alatta a Mezőgazdasági Kiállítás területe. Lenn a gáztartálytól jobbra a Pongráctelep (1923–1925 között épült lakótelep a trianoni menekültek lakhatásának biztosítására) házai. Feljebb a Salgótarjáni (Tomcsányi) út másik oldalán az első világháborús barakk-kórházból kialakult, Auguszta főhercegnőről elnevezett nyomortelep. 1944. április 14. (Fortepan/Magyar Királyi Honvéd Légierő/109120)',
  },
  ],
  lead: '',
  afterText: '<p><strong>Budapesti &eacute;s vid&eacute;ki vagonlak&oacute;k 1920&ndash;1924 k&ouml;z&ouml;tt, az &eacute;vi legmagasabb sz&aacute;madatot alapul v&eacute;ve</strong></p>\n'
            + '<table style="width: 638px; border-color: #000000;" border="1px">\n'
            + '<tbody>\n'
            + '<tr style="height: 35px;">\n'
            + '<td style="width: 132px; height: 35px;">&nbsp;</td>\n'
            + '<td style="width: 236px; height: 35px;" colspan="2">\n'
            + '<p><strong>Budapest főv&aacute;ros</strong></p>\n'
            + '</td>\n'
            + '<td style="width: 236px; height: 35px;" colspan="2">\n'
            + '<p><strong>Az orsz&aacute;g eg&eacute;sz ter&uuml;let&eacute;n</strong></p>\n'
            + '</td>\n'
            + '</tr>\n'
            + '<tr style="height: 35px;">\n'
            + '<td style="width: 132px; height: 35px;">\n'
            + '<p><strong>&Eacute;v</strong></p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p><strong>Vagonok sz&aacute;ma</strong></p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p><strong>Lak&oacute;k sz&aacute;ma</strong></p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p><strong>Vagonok sz&aacute;ma</strong></p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p><strong>Lak&oacute;k sz&aacute;ma</strong></p>\n'
            + '</td>\n'
            + '</tr>\n'
            + '<tr style="height: 35px;">\n'
            + '<td style="width: 132px; height: 35px;">\n'
            + '<p><strong>1920</strong></p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>1 540</p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>3 840</p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>4 137</p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>16 500</p>\n'
            + '</td>\n'
            + '</tr>\n'
            + '<tr style="height: 35px;">\n'
            + '<td style="width: 132px; height: 35px;">\n'
            + '<p><strong>1921</strong></p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>1 231</p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>4 213</p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>4 617</p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>16 098</p>\n'
            + '</td>\n'
            + '</tr>\n'
            + '<tr style="height: 35px;">\n'
            + '<td style="width: 132px; height: 35px;">\n'
            + '<p><strong>1922</strong></p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>531</p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>1 480</p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>2 708</p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>7 600</p>\n'
            + '</td>\n'
            + '</tr>\n'
            + '<tr style="height: 35px;">\n'
            + '<td style="width: 132px; height: 35px;">\n'
            + '<p><strong>1923</strong></p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>146</p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>365</p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>1 268</p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>3 834</p>\n'
            + '</td>\n'
            + '</tr>\n'
            + '<tr style="height: 35px;">\n'
            + '<td style="width: 132px; height: 35px;">\n'
            + '<p><strong>1924</strong></p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>124</p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>310</p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>898</p>\n'
            + '</td>\n'
            + '<td style="width: 118px; height: 35px;">\n'
            + '<p>2&nbsp;694</p>\n'
            + '</td>\n'
            + '</tr>\n'
            + '</tbody>\n'
            + '</table>\n'
            + '<p>Forr&aacute;s: Dr. Petrichevich Horv&aacute;th Emil munk&aacute;ja alapj&aacute;n (PETRICHEVICH HORV&Aacute;TH Emil: <em>Je&shy;len&shy;t&eacute;s az Or&shy;sz&aacute;&shy;gos Me&shy;ne&shy;k&uuml;lt&shy;&uuml;gyi Hi&shy;va&shy;tal n&eacute;gy&shy;&eacute;ves mű&shy;k&ouml;&shy;d&eacute;&shy;s&eacute;&shy;ről.</em>&nbsp;OMH, Bu&shy;da&shy;pest. 1924. 38&ndash;39.) k&eacute;sz&iacute;tette: FR&Aacute;TER Oliv&eacute;r: Kiv&aacute;ndorl&aacute;s &eacute;s menek&uuml;lt&uuml;gyi k&eacute;rd&eacute;s a sz&eacute;kelyudvarhelyi reform&aacute;tus egyh&aacute;zk&ouml;zs&eacute;g sz&aacute;madatai t&uuml;kr&eacute;ben (1908&ndash;1936). In: <em>Magyar Kisebbs&eacute;g,</em> 7. &eacute;vf. (2002) 1. sz. <a href="http://epa.oszk.hu/02100/02169/00018/pdf/020118.pdf" target="_blank">http://epa.oszk.hu/02100/02169/00018/pdf/020118.pdf</a> (Let&ouml;lt&eacute;s ideje: 2020. augusztus 30.)</p>',
},
{
  id: 1,
  title: '',
  name: 'Czucza Emma',
  content: [
    '<b>Honnan: Bánnfyhunyad (románul Huedin, a mócok nyelvjárásában Hogyínu, németül Heynod, jiddisül הוניאד, cigányul Ohodino) város Romániában, Erdélyben, Kolozs megyében<br />'
            + 'Hová: Pápa</b>'
            + '<b style="cursor: pointer;" onclick="onImage(1)">A menekült: Czucza Emma tanítónő</b>',
    '<b>Életrajz</b><br />',
    'Czucza Emma 1886-ban született Kalotaszentkirályon, református családban. Édesapja Czucza János igazgató-tanító, etnográfus volt, aki <b style="cursor: pointer;" onclick="onImage(2)">Jankó János</b> útitársa volt annak kalotaszegi gyűjtőútján. A neves néprajztudós Czucza János segítségével írta meg 1892-ben a magyar etnográfia első modern monográfiáját, a Kalotaszeg magyar népét. <br />',
    '1899-ben a Czucza család <b style="cursor: pointer;" onclick="onImage(3)">Bánffyhunyadra</b> <b style="cursor: pointer;" onclick="onImage(4)">(b)</b> költözött, Emma itt járt a felső leányiskolába, majd tanulmányait a kolozsvári állami tanítóképzőben fejezte be 1908-ban, ahol tanítónői képesítést szerzett. Ezután Bánffyhunyadon kezdett el tanítani a frissen felépített állami elemi népiskolában. Emma testvére, Lajos, ketesdi tanító a nagy háború végén vesztette életét 1918 nyarán az olasz fronton.<br />',
    'A pápai Jókai Mór Városi Könyvtár 2006 és 2010 között több naplóját is megjelentette, melyek életének fontos eseményeit rögzítik. Az alább közölt részeket az 1918-től vezetett <b style="cursor: pointer;" onclick="onContentImage()">naplója</b> tartalmazza, mely szubjektív, de hiteles forrás. A naplóírást a második világháború alatt is folytatta, helytörténeti eseményekről is megemlékezett, többek között az új református templom építéséről vagy az erdélyi írók látogatásáról. Az 1956-os forradalom és szabadságharc eseményeit is feljegyezte, írása hitelesen és tárgyilagosan követi és érzékelteti Pápa korabeli hangulatát. Czucza Emma sosem feledte Erdélyt, naplója folyamatosan végigkíséri az erdélyi eseményeket. Szomorúan ír a kisebbségben élő magyarságról is: „Nekem két hazát adott a végzetem, úgy fáj értük a szívem”. Pápán hunyt el 1974-ben. ',
    '<b>A menekülés története:</b><br />',
    '1919 januárjában a román hadsereg lefoglalta a bánffyhunyadi elemi népiskolát, majd februárban egy rendelettel kötelezték a Romániához csatolt területek magyar állami iskoláinak pedagógusait, hogy tegyék le a hűségesküt <b style="cursor: pointer;" onclick="onImage(5)">I. Ferdinánd román királynak</b>, különben elveszítik munkájukat és nyugdíjjogosultságukat. Czucza Emma egy ideig ennek letétele nélkül tanított, majd egy feljelentés után kötelezték az eskütételre, melyet megtagadott, ezért 1921-ben eltávolították állásából és kiutasították az országból. Mielőtt elhagyta szülőföldjét, még 1921 áprilisában beiktatott egy magyarországi utazást, mely során felmérte az anyaországi lehetőségeket. Először Budapestre ment munka után érdeklődni, ahonnan Veszprém felé vette az irányt. Mivel református volt, ezért Veszprémben nem tudták elhelyezni, itt javasolták számára, hogy próbáljon szerencsét Pápán. A Vallás- és Közoktatásügyi Minisztériumban azonban azt mondták neki, hogy amíg nincs kezében a román kiutasítási végzés, addig nem tudnak neki állást ajánlani. Emma ekkor visszament Bánffyhunyadra, hogy beszerezze a szükséges papírokat.<br />',
    '1921. május 28-án intett búcsút szüleinek, hazájának, egész addigi életének. Egy <b style="cursor: pointer;" onclick="onImage(6)">tehervagonban</b> lépte át az újonnan meghúzott határt, majd június 4-én az 1800-1921 sz. 8352/1920 M.E. számú rendelet értelmében megkapta a beköltözési engedélyt Biharkeresztesen (mely egyike volt azoknak az ellenőrzőpontoknak, melyekről az ország belsejébe irányították a menekülteket). A június 9-i Pápára való megérkezését követően Emma egy sorstársával két vagont is kapott egy félreeső vágányon, melyek átmeneti otthonukat jelentették. Czucza Emma már június 14-én állásra jelentkezett a minisztériumban, végül július 31-én a pápai <b style="cursor: pointer;" onclick="onImage(7)">Református Nőnevelő Intézetben</b> kapott <b style="cursor: pointer;" onclick="onImage(8)">tanítónői munkát</b> <b style="cursor: pointer;" onclick="onImage(9)">(b)</b> <b style="cursor: pointer;" onclick="onImage(10)">(c)</b>, ahol munkája során nagy megbecsülésre tett szert. „Emma néni” 1921 és 1949 között, nyugdíjazásáig az intézmény <b style="cursor: pointer;" onclick="onImage(11)">meghatározó személyisége</b> volt. <br />',
    '<b>Források</b><br />'
            + 'CZUCZA Emma: Áttelepülésem és első pápai éveim. Naplórészlet. 1918–1936. Hermann István (szerk.): Jókai Füzetek 2007. 52. Jókai Mór Városi Könyvtár, Pápa, 2007.<br />'
            + 'KŐRÖS Endre (szerk.): A Dunántúli Református Egyházkerület pápai Nőnevelő-intézetének (XXI.) értesítője az 1921–22. iskolai évről. Főiskolai Nyomda, Pápa, 1922.<br />'
            + 'ORTUTAY Gyula (főszerk.): Magyar Néprajzi Lexikon. II. kötet. Akadémiai Kiadó, Budapest, 1979.',
  ],
  slug: 'czucza-emma',
  thumbnail: '/trianon_images/czucza_emma/__________0_THUMBNAIL.jpg',
  images: [{
    src: '/trianon_images/czucza_emma/__________0.jpg',
    description: '(0) Czucza Emma fotója, 1930 körül. (Jókai Mór Városi Könyvtár, Pápa)',
  }, {
    src: '/trianon_images/czucza_emma/__________1.jpg',
    description: '(1)   Jankó János (1868–1902): Pesten született, Budapesten folytatott földrajzi tanulmányokat, majd 1894-ben a Magyar Nemzeti Múzeum néprajzi osztályán dolgozott. Beutazta Magyarországot és megbízásra elvégezte az Ezredéves (millenniumi) Országos Kiállítás néprajzi falujának építési munkálatait. Megírta Kalotaszeg, Torockó és Aranyosszék, valamint a Balatonmellék néprajzi monográfiáját, és egy összehasonlító tematikus feldolgozást készített a halászatról. Kalotaszegről szóló munkájának elkészítésekor segítségére volt Czucza János kalotaszentkirályi tanító. Jankó korának egyik legsokoldalúbban képzett, nagy nemzetközi tapasztalattal és jó terepismerettel rendelkező magyar etnográfusa volt. Megalapozta a tudományos igényű magyarországii néprajzi muzeológiát és tudományos kutatóhellyé szervezte a Néprajzi Múzeumot. Jankó János portréja, 1902. (Vasárnapi Újság, 49. évf. (1902) 31. sz. 495.)',
  }, {
    src: '/trianon_images/czucza_emma/__________2a.jpg',
    description: '(2a)  Bánffyhunyad, Kossuth Lajos utca, háttérben a római katolikus templommal, 1907. (Fortepan/Magyar Földrajzi Múzeum/Erdélyi Mór cége/96253)',
  }, {
    src: '/trianon_images/czucza_emma/__________2b.png',
    description: '*2b)  Bánffyhunyad Fő utca, 1910-es évek. (Zempléni Múzeum, 0097246)',
  }, {
    src: '/trianon_images/czucza_emma/__________3.png',
    description: '(3) I. Ferdinánd román király, 1918 körül. (Library of Congress)',
  }, {
    src: '/trianon_images/czucza_emma/__________4.png',
    description: '(4)	Vagonlakó menekültek tehervagonok előtt egy pályaudvaron, Budapest, 1920. Müllner János felvétele. (Magyar Nemzeti Múzeum Történeti Fényképtár, 321/1948 fk)',
  }, {
    src: '/trianon_images/czucza_emma/__________5.png',
    description: '(5)	A pápai Református Nőnevelő Intézet, 1913. (Zempléni Múzeum, 0166972)',
  }, {
    src: '/trianon_images/czucza_emma/__________6a.png',
    description: '(6)	A pápai Református Nőnevelő Intézet 1921-es értesítője, benne Czucza Emma kinevezése és munkarendje. (Kőrös Endre (szerk.): A Dunántúli Református Egyházkerület pápai Nőnevelő-intézetének (XXI.) értesítője az 1921–22. iskolai évről. Főiskolai Nyomda, Pápa, 1922. 6; 12.)',
  }, {
    src: '/trianon_images/czucza_emma/__________6b.png',
    description: '(6)	A pápai Református Nőnevelő Intézet 1921-es értesítője, benne Czucza Emma kinevezése és munkarendje. (Kőrös Endre (szerk.): A Dunántúli Református Egyházkerület pápai Nőnevelő-intézetének (XXI.) értesítője az 1921–22. iskolai évről. Főiskolai Nyomda, Pápa, 1922. 6; 12.)',
  }, {
    src: '/trianon_images/czucza_emma/__________6c.png',
    description: '(6)	A pápai Református Nőnevelő Intézet 1921-es értesítője, benne Czucza Emma kinevezése és munkarendje. (Kőrös Endre (szerk.): A Dunántúli Református Egyházkerület pápai Nőnevelő-intézetének (XXI.) értesítője az 1921–22. iskolai évről. Főiskolai Nyomda, Pápa, 1922. 6; 12.)',
  }, {
    src: '/trianon_images/czucza_emma/__________7.jpg',
    description: '(7)	Czucza Emma (jobbról a hatodik) és a pápai Református Nőnevelő Intézet végzős diákjai, 1929. (Jókai Mór Városi Könyvtér, Pápa)',
  }],
  contentImages: [
    '/trianon_images/czucza_emma/konyv/czucza emma_0.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_1.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_2.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_3.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_4.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_5.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_6.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_7.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_8.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_9.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_10.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_11.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_12.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_13.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_14.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_15.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_16.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_17.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_18.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_19.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_20.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_21.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_22.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_23.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_24.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_25.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_26.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_27.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_28.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_29.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_30.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_31.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_32.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_33.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_34.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_35.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_36.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_37.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_38.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_39.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_40.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_41.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_42.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_43.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_44.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_45.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_46.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_47.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_48.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_49.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_50.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_51.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_52.png',
    '/trianon_images/czucza_emma/konyv/czucza emma_53.png',
  ],
  contentText: 'Czucza Emma: Áttelepülésem és első pápai éveim. Naplórészlet. 1918–1936. Hermann István (szerk.): Jókai Füzetek 2007. 52. Jókai Mór Városi Könyvtár, Pápa, 2007.',
  lead: '',
},
{
  id: 2,
  name: 'Ébersz Gyula ',
  title: '',
  content: [
    '<b>Honnan</b>: Munkács (ukránul Мукачево [Mukacsevo] (hivatalosan) vagy Мукачеве [Mukacseve] vagy Мукачів [Mukacsiv], ruszinul Мукачово [Mukacsovo], oroszul: Мукачево [Mukacsevo], szlovákul: Mukačevo, németül Munkatsch vagy Munkatz, lengyelül: Mukaczewo, románul Munceag vagy Muncaci, jiddis: מונקאטש Minkács) területi jelentőségű város Ukrajnában, Kárpátalján.<br />'
            + '<b>Hová</b>: Debrecen<br />'
            + '<b style="cursor: pointer;" onclick="onImage(1)">A menekült: Ébersz Gyula szemafor-felvigyázó és családja</b>',
    '<b>Életrajz:</b><br />',
    'Ébersz Gyula 1875-ben született a Nógrád megyei Baglyasalján. Édesanyja ungvári magyar leány volt, édesapja német származású nógrádi bányász. Apjáról, Ebers Ertmanról maradt fenn a családi legendáriumban a következő történetet: Ertman rendszeresen járt vadászni Ungvár környékére. A téli vadászatokhoz szarvasbőrből jégeralsót, nadrágot és inget készíttetett magának. Erre csak egy könnyű kabátot vett fel, így fürgén tudott mozogni, míg többi vadásztársa nehézkesen mozgott nagy, hosszú bundájában. Nem tudták, hogy Ertmant a szarvasbőr alsóruhája védi meg a hidegtől. Tréfásan ugratták és mondogatták neki: „Ez a huncut német bezzeg nem fázik.”<br />',
    'Gyula 1900-ban kötött házasságot Szabó Lujzával, aki 1880-ban született Csapon (ma Ukrajna) jobb módú gazdálkodó családban. Lujza 1966-ban halt meg Debrecenben. Három gyermekük született: Oszkár, Gyula és Lenke. Az 1910-es évekig Csapon éltek.<br />',
    'A vonatok, a szerelvények és a vasútállomás izgalmas, modern dolgot jelentett Csapon (ma Ukrajna) is, a haladást szimbolizálta. A vasút fejlődése a „boldog békeidők” egyik megtestesítője volt. Gyulának és családjának egyik kedvenc vasárnap délutáni időtöltése volt, hogy kimentek a <b style="cursor: pointer;" onclick="onImage(2)">csapi állomásra </b> és az induló/érkező vonatokat figyelték.<br />',
    'Ébersz Gyula 1908. május 26-án lépett be a MÁV-hoz, szolgálati idejét Sátoraljaújhely–Királyházán kezdte meg <b style="cursor: pointer;" onclick="onImage(3)">szemafor-felvigyázóként</b>, de már korábban, 1905 februárjában gépkezelői vizsgát tett.<br />',
    'Első munkakörében még nem fizetést, hanem napidíjat kapott. 1910 végén helyezték át <b style="cursor: pointer;" onclick="onImage(4)">Munkácsra</b>, ahol először segéd szemafor-felvigyázóként, majd néhány hónap múlva az altiszti képesítő elvégzése és kinevezése után már szemafor-felvigyázóként dolgozott. Fizetése ekkor 1200 koronát tett ki, majd előléptetéseinek köszönhetően javadalmazása 1918 végére már 1800 korona volt. Az első világháború során, 1917-ben <b style="cursor: pointer;" onclick="onImage(5)">IV. osztályú Polgári Hadi Érdemkereszt</b> kitüntetést kapott. A <b style="cursor: pointer;" onclick="onImage(6)">MÁV szolgálati táblája</b> és Gyula arcképes igazolványának bizonysága szerint 1919 elején, „státuszrendezés” után első osztályú „semaphor-felvigyázóvá” lépett elő, ekkor évi fizetése már elérte a 2200 koronát.',
    '<b>A menekülés története:</b><br />',
    'Az Osztrák–Magyar Monarchia felbomlása után a régi-új utódállamok már az 1920. június 4-én megkötött trianoni békeszerződés előtt megkezdték a magyar területek elfoglalását, aminek következtében 1918 végén tömeges és nagyrészt kényszerű menekülés kezdődött meg az ország belseje felé. Az 1920-ban létrehozott Országos Menekültügyi Hivatal statisztikája szerint az összes elcsatolt területről 1924-ig mintegy 350 ezer ember hagyta el otthonát és jött a trianoni határok által kijelölt országrészbe, utólagos becslések ezt a számot 425 ezerre teszik. <b style="cursor: pointer;" onclick="onImage(7)">A menekültek egy jelentős része – az egykori magyar államapparátusát működtető hivatalnokok, értelmiségiek és a középosztály különböző csoportjai mellett – a vasúti dolgozók közül került ki.</b> A békeszerződés után készült statisztikákból az látható, hogy az 1920-ban, a trianoni Magyarország területén regisztrált 64 758 főnyi vasúti alkalmazotti létszám 1924-re mintegy 10 ezerrel nőtt, azt azonban nem tudjuk, hogy köztük mennyi lehetett a trianoni menekült. Ha elfogadjuk Kelety Dénes, MÁV kormánybiztos 1921 tavaszán készült beszámolóját, akkor megkockáztathatjuk, hogy a 10 ezer dolgozó többsége korábban elbocsátott vasúti alkalmazott lehetett. Kelety a beszámolóban 13 225 vasúti alkalmazottról és családtagjaikról, tehát összesen csaknem 50 ezer emberről értekezik. Mivel a menekültek többsége az év tavaszáig jött az országba, végső számukat 20 000 főben határozhatjuk meg, így családtagjaikkal együtt közel 70–80 ezer emberről kell szót ejtenünk. A vasúti dolgozók és családjaik tehát mindenképpen említésre méltó részét jelentették a trianoni békeszerződés következtében Magyarországra menekült határon túlra került magyaroknak.<br />',
    '1920 májusában – áthelyezés következtében – szolgálati helye <b style="cursor: pointer;" onclick="onImage(8)">Debrecenre</b> változott. Az egyszerű munkahelyváltásnak tűnő esemény mögött azonban egy nehéz döntés állt, amit hosszú hónapokig tartó kiszolgáltatott helyzet követett. Munkács 1919-es cseh megszállása után Gyula nem tette le a hűségesküt az új Csehszlovák államra, így feleségével, Lujzával, valamint három gyermekükkel, <b style="cursor: pointer;" onclick="onImage(9)">Oszkárral, Gyulával</b> és Lenkével hosszú, kilátástalan útra indultak az ország belseje felé. Hónapokig laktak egy vasúti kocsiban, míg végre a családfő Debrecenben kapott állást és szolgálati férőhelyet. <br />',
    'A családi emlékezet szerint Gyula nagyon szépen tudott furulyázni, szerette és becsülte feleségét és gyermekeiket. Rendkívül megbízható, a végletekig becsületes ember volt: az első világháború végén a menekülők számos értéket hagyták hátra a munkácsi vasútállomáson, így egyes vasúti dolgozók vagyonokat tudtak összeharácsolni az elhagyott, ottfelejtett javakból. Ébersz Gyula az egyik nap egy <b style="cursor: pointer;" onclick="onImage(10)">behorpadt rohamsisakot</b> hozott haza az állomásról, mondván, jó lesz az majd csirkeitatónak.<br />',
    'Rövid élete a trianoni békediktátum miatti menekülés viszontagságai után Debrecenben fejeződött be, 1920-ban. Feleségéről és gyermekeikről a MÁV a későbbiekben is gondoskodott, özvegyi nyugdíjat, gyermeksegélyt és lakbértámogatást biztosított számukra.Lujza mint „vasúti özvegy” Püspökladányban kapott bérlakást és árusítási lehetőséget egy trafikban.<br />',
    '<b style="cursor: pointer;" onclick="onImage(11)">Zöld bőrtokba foglalt vasutas igazolványára</b> felettébb büszke volt, az első pecsétet 1911 januárjában ütötték bele. Az igazolvány fedőlapján német–magyar kétnyelvű, aranybetűs felirat olvasható: „Legitimation… Igazolvány vasúti dolgozók részére”. Arcképes igazolványának fényképe alatt két kis cédulát tartott, melyeken saját kézírásával egy-egy rövid fohász olvasható.',
    '<b>Források</b><br />'
            + 'MAGYARI Márta: Igazolványba rejtett fohász. Ébersz Gyula vasutas-igazolványa a Skanzenben. 2011. https://archiv.magyarmuzeumok.hu/targy/323_igazolvanyba_rejtett_fohasz <br />'
            + 'NAGY Tamás: Trianon és a magyar vasút. In: DÖBÖR András – KISS Gábor Ferenc (szerk.): Magyarország és Európa 1919–1939. Belvedere Meridionale, Szeged, 2001. 149–166.<br />'
            + 'PETRICHEVICH HORVÁTH Emil: Jelentés az Országos Menekültügyi Hivatal négyévi működéséről. Pesti Nyomda, Budapest 1924.<br />'
            + 'SZŰCS István Gergely: Vasutas vagonlakók és a MÁV menekültpolitikája 1918–1924. In: Múltunk, 57. évf. (2012) 4. sz. 89–112.<br />'
            + 'SZŰTS István Gergely: Elűzöttek, menekültek, optánsok és vagonlakók. In: Rubicon, 2017/7–8. 52–61.<br />',
  ],
  slug: 'ebersz-gyula',
  thumbnail: '/trianon_images/ebersz_gyula/__________0_ES_8_THUMBNAIL.png',
  images: [{
    src: '/trianon_images/ebersz_gyula/__________0_ES_8.png',
    description: '(0) (8)	Ébersz Gyula gyermekeivel, Oszkárral és Gyulával, 1905 körül. (Magántulajdon)',
  }, {
    src: '/trianon_images/ebersz_gyula/__________1.png',
    description: '(1)	Csap, pályaudvar, 1917. (Zempléni Múzeum, 0085076)',
  }, {
    src: '/trianon_images/ebersz_gyula/__________2.jpg',
    description: '(2) A szemafor, vagy más néven alakjelző, a vasúti pálya mellett felállított, a vonatforgalmat szabályozó jelzőberendezés. A szemafor árbócának (vas tartószerkezetének) tetején található vörös-fehérre mázolt jelzőkar vagy jelzőkarok helyzetével (sötétben petróleumlámpával megvilágított jelzőüvegeken keresztül jelzőfényekkel) ad jelzést, azaz parancsot a mozdonyvezetőnek és a mozdonyszemélyzetnek. A jelzések megtiltják vagy engedélyezik a vonatközlekedést, a továbbhaladást engedélyező jelzés pedig a közlekedés módját és mikéntjét is meghatározza. A jelzőkarokat vonóvezetékeken keresztül a forgalomszabályzást végző dolgozók állítják továbbhaladást engedélyező vagy megtiltó állásba. Szemafor-jelzőlámpa (Szabadtéri Néprajzi Múzeum, 2011.03.04)',
  }, {
    src: '/trianon_images/ebersz_gyula/__________3.png',
    description: '(3)	A munkácsi pályaudvar 1916 körül. (Zempléni Múzeum, 85794)',
  }, {
    src: '/trianon_images/ebersz_gyula/__________4.jpg',
    description: '(4)	A kitüntetést (németül Kriegskreuz für Zivilverdienste) 1916. február 18-án alapította I. Ferenc József osztrák császár és magyar király. Azokat a polgári személyeket jutalmazták vele, akik az Osztrák–Magyar Monarchia közigazgatásában példás teljesítményükkel kimagasló eredményeket értek el. Azok a katonai személyek is kiérdemelhették, akik közvetlen harci cselekményben nem vettek részt, de a katonai ellátás, a szervezés és az utánpótlás területén tevékenykedtek. Négy osztályban került adományozásra. A Polgári Hadi Érdemkereszt II., III. és IV. osztálya. (Wikimedia Commons/Duke83)',
  }, {
    src: '/trianon_images/ebersz_gyula/__________5.jpg',
    description: '(5)	Ébersz Gyula szolgálati táblája. (Magántulajdon)',
  }, {
    src: '/trianon_images/ebersz_gyula/__________7.jpg',
    description: '(7)	A debreceni pályaudvar, 1940 körül. (Fortepan/Kókány Jenő/107545)',
  }, {
    src: '/trianon_images/ebersz_gyula/__________9.jpg',
    description: '(9)	Osztrák–magyar katonák kétféle rohamsisakban az olasz hadszíntéren, 1917. (Fortepan/Komlós Péter/52332)',
  }, {
    src: '/trianon_images/ebersz_gyula/__________10.png',
    description: '(10)	A két fohász szövege:'
                    + '„Istenem őriz '
                    + 'minden utamban '
                    + 'oltalmaz segíts '
                    + 'munkámba” '
                    + '„Mindenható Kegyelmes '
                    + 'Istenem légy velem '
                    + 'őriz oltalmaz ez évben '
                    + 'és minden napjain ad meg '
                    + 'minden napi kenyerünket '
                    + 'és szabadits meg a gonosztúl '
                    + 'ammen” '
                    + 'Arcképes vasúti igazolvány Ébersz Gyula szemafor-felvigyázó részére, 1911. (Magántulajdon)',
  },

  ],
  lead: '',
},
{
  id: 3,
  name: 'Keresztes Ede',
  title: '',
  content: [
    '<b>Honnan</b>: Marosludas (románul: Luduș, régebb Ludoșul de Mureș, németül: Ludasch) város Romániában, Erdélyben, Maros megyében<br />'
            + '<b>Hová:</b> Budapest<br />'
            + '<b style="cursor: pointer;" onclick="onImage(1)">A menekült: Dr. Keresztes Ede, ügyvéd </b>',
    '<b>Életrajz:</b><br />', 'A léczfalvi Keresztesek ősi birtoka a Kovászna megyei Lécfalva (románul Leț), amely közigazgatásilag Nagyborosnyóhoz tartozik. A szomszédos Várhegy falut 1899-ben csatolták Lécfalvához. Sepsiszentgyörgytől 17 km-re keletre a Feketeügy jobb partján fekszik. A faluban 1600-ban székely országgyűlést tartottak. 1910-ben 985, túlnyomórészt magyar lakosa volt. A trianoni békeszerződésig Háromszék vármegye Sepsi járásához tartozott.<br />', 'Lécfalva nevével először az 1332–1334. években készített pápai tizedjegyzékekben találkozunk. A Keresztes család őse a Léc által vezetett székely vitézek egyike volt. A léczfalvi Keresztes név oklevélben először 1580-ban fordul elő. A lécfalvi Keresztesek a székely társadalom felső rétegéhez tartoztak, Keresztes Ferenc lófő székelyt, valamint János, Gergely és Sándor nevű fiait Magyar-Bródon 1621. november 8-án nemesíti Bethlen Gábor.  <b style="cursor: pointer;" onclick="onImage(2)">A családi címer</b>: „Léczfalvi Keresztes Ferenc lófőnek kék pajzsban koronából kinövő, nyílvesszőket tartó farkast adományoz, sisakdíszül pedig négy nyílvesszőt. Úgy látszik a Keresztesek már a Szapolyaiak alatt is szerepeltek, részt véve Erdély kialakulásának harcaiban s ennek emlékére kapták címerükbe a farkast, mely a Szapolyai dynastia címere volt.”  A család ekkortól a székelyek késő középkorban kialakuló főnemesi osztályához tartozott, primorok (jelentése: első) voltak.  <br />', 'A Keresztes család a református vallás székelyföldi elterjedése óta ennek a vallásnak a híve. A család emlékét őrzi Lécfalva község belső területén a „Keresztesek utcája”, melynek első írásos nyomát 1743-ból ismerjük. Itt áll <b style="cursor: pointer;" onclick="onImage(3)">a Keresztes kúria épülete.</b><br />', 'Léczfalvi Keresztes Antal primor (1825–1853), első felesége bikfalvi Mihály Borbála (elváltak 1850-ben), második feleségével czófalvi Csia Karolina primorral 1851-ben kötött házasságot. Tőle született gyermeke <b style="cursor: pointer;" onclick="onImage(4)">léczfalvi Keresztes Ede (1851–1921),</b><b style="cursor: pointer;" onclick="onImage(5)">(a)</b> aki nemcsak földbirtokos volt, de a kézdi református egyházmegye főgondnoka is. Első felesége Dálnoki Miklós Berta (1853–1882), akitől négy gyermeke született. Három csecsemő korában elhalálozott (Irma 1875; Emma 1876; Tivadar 1882–1883), <b style="cursor: pointer;" onclick="onImage(6)">negyedik gyermeke Ede, aki 1879-ben született és 1967-ben halt meg.</b> Második házasságát a kézdivásárhelyi Kovács Eleonórával (1856–1934) kötötte 1885-ben. <b style="cursor: pointer;" onclick="onImage(7)">Hét gyermekük született János, Géza, István, Emília, Károly, Irma és Lajos. </b><br />', 'Keresztes Ede a <b style="cursor: pointer;" onclick="onImage(8)">székelyudvarhelyi kollégiumban tanult</b>, ahol 1898-ban végzett, majd a debreceni Jogiakadémia elvégzése után a kolozsvári Ferencz József Tudományegyetemen szerzett jogi diplomát. 1906-ban <b style="cursor: pointer;" onclick="onImage(9)">Marosludason</b> lett ügyvéd. Dolgozott vármegyei ügyészként is.<br />', '1909-ben Marosvásárhelyen kötött házasságot erzsébetvárosi Patrubány Gabriellával és két gyermekük született: András György és György Pál. <b style="cursor: pointer;" onclick="onImage(10)">(a)</b><br />', '1920-ban elhagyják Erdélyt, Magyarországra költöznek. Először a II. kerületi <b style="cursor: pointer;" onclick="onImage(11)">Kapás utcában </b> megvásárolt lakásban éltek. A Kapás utcai házból az általuk építtetett <b style="cursor: pointer;" onclick="onImage(12)">Guyon Richárd utcai villába</b> költöztek. Alapító gondnoka lett a <b style="cursor: pointer;" onclick="onImage(13)">Pasaréti Református Gyülekezetnek</b><b style="cursor: pointer;" onclick="onImage(14)">/b><b style="cursor: pointer;" onclick="onImage(15)">(a)</b><b style="cursor: pointer;" onclick="onImage(16)">(b)</b> és támogatója a templomépítésnek. Mindvégig ápolta erdélyi és református gyökereit. Tagja volt a Johannita Lovagrendnek, baráti kapcsolatot ápolt több erdélyi és anyaországi értelmiségivel, egyházi méltósággal, többek között Ravasz László református püspökkel, de Márton Áron katolikus püspökkel is.  A háború után politikai okokból háttérbe szorítják, lemond főgondnoki tisztségéről is, de haláláig a gyülekezet megbecsült tagja volt és a tiszteletbeli főgondnok címet viselte. Végakaratának megfelelően Lécfalván temették el.<br />', '<b>A menekülés történetének forrása: Saját visszaemlékezése, amelyet 1944. március 26-án írógépen jegyzett le. A szöveget javítás nélkül, az eredeti formájában közöljük. </b><br />', '„<b style="cursor: pointer;" onclick="onImage(17)">Édesapám</b> a léczfalvi református egyházközségnek gondnoka, a kézdi járási egyházmegye főgondnoka, s lelkes 48.as székely volt és közszeretetben állott. A körjegyzőség községeit bölcsen és szeretettel vezette a haladás útján. Élete utolsó 8 évében sokat betegeskedett, 64 éves korában Illés tanár megoperálta, de állapota alig javult. Gyógyulását visszavetette és nagyon megviselte a román betörés. Nem vállalhatta a szekéren való menekülés fáradalmait, s otthon maradt. A megszállás elől pár napra a községtől 5 km.nyire fekvő Csobot erdőbe az Úrkútja nevű forráshoz menekült ki többed magával. Ahogy megtudták, hogy csendesebb a helyzet hazamentek. …<br />', 'A nehezen beszerzett katonai és polgári utazási engedélyekkel Marosludastól Uzonig katonavonatokon és gépkocsin, Uzontól Maksáig körülbelül 10 kilóméteren gyalog, s innen hazáig a nehezen szerzett szekéren tettük meg az utat Tarcali Ilonával, aki nálunk szolgált, s a szüleit kereste. Csak Maksán tudtuk meg, hogy ők is élnek. Az út mindenütt a háború nyomait viselte. Elhagyott házak, udvarra hajított bútorok, még zongorát is láttunk, s egyéb a kiürítés zűrzavarát mutató képek. Az utcán gazdátlan házi állatok, a mezőn behordatlan gabona keresztek, félbe maradt asztagok, s itt ott egy egy ember. A mi házunkat is feldúlták. A leveles ládát az udvarra hajították, s tartalmát szétszórták úgy, hogy alig maradt meg pár darab irat. A könyen elemelhető ingóságokat, s az édesapám kedves fuvoláját is ellopták. De mindez semmi volt ahoz képest, hogy édesapám él, nem esett bántódása, s nem hurcolták el, amiktől anyira rettegtünk. Pár napig boldogan pihentünk, s csendes időben még hallottuk az ágyuk távoli hangját. Ezután a katonavonatok alkalmi járataival, boldog megnyugvással utaztuk vissza Marosludasra. <br />', 'A háborús összeomlástól a román megszállásig izgalmas időket éltünk át Ludason. A fegyveresen visszaözönlő, fegyelmezetlen és lövöldöző katonák, a meglazult rend és elakadt közigazgatás miatt a lakósság legnagyobb része félelemben és izgalommal kereste a zűrzavarból kivezető utat. Így alakult meg a Nemzeti tanács, melynek elnökéül a magyar és román lakosság egyhangúlag engem választott meg. Ez vette át a megakadt közigazgatást és közbiztonsági szolgálatot. Nagy erőfeszítéssel, éjjel nappali készenléttel, a nemzetőrség megszervezésével, segélyezésekkel, sok rábeszéléssel nagyjából sikerült megfékeznünk a lövöldöző duhaj katonákat és az elégedetlenkedő elemeket. Én néha éjszakának idején is egyedül és fegyvertelenül jártam a községházára, a nemzetőrség tanyájára, valahányszor szükség volt erre, különösen a hangulat lecsillapítására. Pedig különösen kezdetben az éjeli lövőldözés sem tartozott a ritkaságok közé. <br />', 'A megszállás előjátéka, a románok kiválása a nemzeti tanácsból és nemzetőrségből, a magyar és román tanácsok, őrségek és lakosság között a viszony tűrhetővé tétele, a kirobbanások lefékezése, a megszálló román katonaság bevonulása, az intézmények és hatóságok átvétele, az elemi iskola – melynek gondnoka voltam – átadása, mind – mind izgalmas és súlyos feladatot rótt rám. Az esküt nem tett tisztviselők támogatása pénzzel és tanáccsal, a fegyverrejtegetések, a román hatóságok által felajánlott régi közigazgatási bizottsági tagságom elfogadásának a megtagadása, az ügyvédi eskü letételének a megtagadása, s végül ellenőrzés alá vételem az izgalmakat tetőpontra emelték. Hónapokon át álmatlanság és idegesség gyötört. A gyógyszerek nem használtak, de a visszatérés a gyerekkoromban kedvelt bélyeggyűjtéshez kissé megnyugtatták az idegeimet. A postáról zsákszámra vettem a kiselejtezett utalványokat és szállítóleveleket. Minden szabadidőmet a bélyegek leáztatásának, szárításának, rendezésének és százas csomókba kötözésének szenteltem. <br />', 'A felszabadulás reménysége hónapról hónapra halványodott, s be kellett látnom, hogy a megszállás hosszabb és az én helyzetem hónapról – hónapra tarthatatlanabb lesz. El kellett határoznom a megszállt terület elhagyását, és azt hogy gyermekeim neveltetése érdekében Budapestre költözünk. Azon a címen, hogy az anyósomat gyógykezelés céljából haladéktalanul Pestre kell kísérnem, nagy nehézségek leküzdésével sikerült kiutazási engedélyt kapnom. Keserves utazás és vámvizsgálat után éjszakára sikerült Berettyóujfaluba érnünk. Még a szállás nyomorúságos voltát, s a fáradtságot is elfeledtük, mert szabad magyar földre érkeztünk. Micsoda boldogság volt reggel a borbélyüzlet ablakából megpillantani a két év óta nem látott magyar csendőrt. Összeszorult a torkom, s kibuggyant a könnyem. Innem már szabad magyar földön, magyar vonaton boldog diadalmenetnek éreztem az utazást Budapestig. <br />', 'Budapesten is viharzott bennem a szabad élet öröme. Igyekeztem is kihasználni a drága időt. Megboldogult sógorom Staibl Andorral találtunk egy kis családi házat, s 1920. november 16.án meg is vettem a II. Kapás u. 37.sz. házat. Minden szükséges felvilágosítást megszereztem és előkészítettem Budapestre való beköltözésünket. <br />', 'Marosludsasra visszaérkezve azzal fogadott a feleségem, hogy az elutazásunkat követő napon kézbesítették a főszolgabíró rendeletét, hogy azonnal ürítse ki a házunkat és adja át a főszolgabíróság részére. Nagyon megijedt, de Keresztes Ödön földbirtokos barátom és dr. Oltean János ügyvéd jóindulatú segítségével sikerült megmenekülni attól, hogy a gyermekekkel együtt az utcára tegyék. De azt már az ő támogatásuk sem tudta megakadályozni, hogy a község melletti földjeinket az oláhok fel ne osszák és birtokba ne vegyék. <br />', 'Gyorsan kellett cselekednem, mert nyilvánvaló volt, hogy hovatovább nehezebb lesz a helyzetem és kedvezőtlenebb a házaim és telkeim eladása. Így is már csak erősen nyomott árak mellett tudtam az ingatlanokat és eladásra szánt ingóságainkat eladni. Közben lázasan folyt az elutazási készülődés és a csomagolás. <b style="cursor: pointer;" onclick="onImage(18)">(a)</b><b style="cursor: pointer;" onclick="onImage(19)">(b)</b><b style="cursor: pointer;" onclick="onImage(20)">(c)</b> Végre a kiutalt három teherkocsiba beraktuk az ingóságainkat, berendeztük a konyhát és hálóhelyeket, s március 1.én elindultunk az új, a nehéz, de felszabadulást jelentő magyar élet felé…. <br />', 'Két heti út után 1921.évi március 15-én hajnalban érkeztünk Lökösházára <b style="cursor: pointer;" onclick="onImage(21)">(a)</b>, s teherkocsi ajtajára kitűztük a rejtegetett, és elmenekített <b style="cursor: pointer;" onclick="onImage(22)">drága magyar zászlót</b>. Felejthetetlen érzés volt. Tavasz első napján március 21.én érkeztünk Budapestre a <b style="cursor: pointer;" onclick="onImage(23)">Déli pályaudvarra</b>. Az első éjjelt még a pályaudvaron a vasúti kocsiban, de a következőt már a Kapás utcai házunkból az eladó által kiürített, két, bútorokkal zsúfolt szoba egyikében, szükség ágyakban tölthettük.<br />', 'Különösen emlékezetes az 1940 októberi lécfalvi utunk a Pasaréti ref.egyházközség ajándékozta országzászló felavatására. <b style="cursor: pointer;" onclick="onImage(24)">(a)</b><b style="cursor: pointer;" onclick="onImage(25)">(b)</b><b style="cursor: pointer;" onclick="onImage(26)">(c)</b><b style="cursor: pointer;" onclick="onImage(27)">(d)</b> Az országzászlót gondnokságomra való kedves megemlékezéssel adta az egyházközség az én szülőfalumnak. Erre az október 21.i ünnepségre egészen váratlanul, s igen-igen nagy örömömre leutazott a mi igazán szeretett papunk dr. Joó Sándor is. Ennek a lécfalvi időzésnek a lélekemelő zászlóavatáson kívül még volt egy felejthetetlen eseménye a november 6.ra virradó éjjelen a nagy földrengés. Betű szerint megrázó esemény volt.”',
  ],
  slug: 'keresztes-ede',
  thumbnail: '/trianon_images/keresztes_ede/___________1_HU_MNL_OL_P_1930_VIII_0069_r_THUMBNAIL.png',
  images: [{
    src: '/trianon_images/keresztes_ede/1.png',
    description: '1.	Keresztes Ede portréja (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/2.png',
    description: '2.	A léczfalvi Keresztesek címere (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/3.jpg',
    description: '3.	A Keresztesek léczfalvi kúriája, 1935 (Magántulajdon)',
  },
  {
    src: '/trianon_images/keresztes_ede/4.png',
    description: '4.	id. Keresztes Ede 1892 körül (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/5.jpg',
    description: '5.	A Keresztes család 1892 körül Kovásznán (Magántulajdon)',
  },
  {
    src: '/trianon_images/keresztes_ede/6.png',
    description: '6.	ifj. Keresztes Ede a székelyudvarhelyi kollégium érettségiző diákja, 1898 (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/7.png',
    description: '7.	A Keresztes család 1907 körül (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/8.png',
    description: '8.	ifj. Keresztes Ede érettségi tablója, Székelyudvarhely, 1898 (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/9.jpg',
    description: '9.	Keresztes Ede marosludasi háza 1912-ben (Magántulajdon)',
  },
  {
    src: '/trianon_images/keresztes_ede/10.png',
    description: '10.	ifj Keresztes Ede és későbbi felesége Patrubány Gabriella, 1908-ban (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/11.jpg',
    description: '11.	A budapesti, 2. kerületi Kapás utca az 1930-as években. (Fővárosi Szabó Ervin Könyvtár - Budapest Gyűjtemény, bibFOT00000461)',
  },
  {
    src: '/trianon_images/keresztes_ede/12.png',
    description: '12.	A Guyon Richárd utcai családi ház, 1935 körül (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/13.png',
    description: '13.	A torockó téri református templom (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/14.jpg',
    description: '14.	A torockó téri református templom (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/15.png',
    description: '15.	A torockó téri református templom (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/16.png',
    description: '16.	A torockó téri református templom (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/17.png',
    description: '17.	id. Keresztes Ede és második felesége Marosludason 1914-ben (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/18a.jpg',
    description: '18.	a-c Román nyelvű repatriálási dokumentum, 1921 (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/18b.jpg',
    description: '18.	a-c Román nyelvű repatriálási dokumentum, 1921 (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/18c.jpg',
    description: '18.	a-c Román nyelvű repatriálási dokumentum, 1921 (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/19.jpg',
    description: '19.	Beköltözési engedély, 1921 (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/20.jpg',
    description: '20.	Az elmenekített magyar zászló címere (Magántulajdon)',
  },
  {
    src: '/trianon_images/keresztes_ede/21.jpg',
    description: '21.	A Déli pályaudvar látképe, 1935. (Fortepan, adományozó: Szent-Istvány Dezső képszám: 41579)',
  },
  {
    src: '/trianon_images/keresztes_ede/22.png',
    description: '22.	A léczfalvi Országzászló avatása, 1940 (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/23.png',
    description: '23.	A léczfalvi Országzászló avatása, 1940 (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/24.jpg',
    description: '24.	A léczfalvi Országzászló avatása, 1940 (Magyar Nemzeti Levéltár P1930)',
  },
  {
    src: '/trianon_images/keresztes_ede/25.jpg',
    description: '25.	Meghívó a léczfalvi országzászló avatásra, 1940. (Magántulajdon)',
  },
  ],
  lead: '',
},
{
  id: 4,
  name: 'Lux Viktor',
  title: '',
  content: [
    '<b>Honnan</b>: Felka (szlovákul Veľká) egykor önálló város, 1945 óta Poprád településrésze Szlovákiában<br />'
            + '<b>Hová:</b> Gödöllő, majd Pestszentlőrinc<br />'
            + '<b><b style="cursor: pointer;" onclick="onImage(1)">A menekült: Lux Viktor iskolaigazgató </b>',
    '<b>Életrajz:</b><br />', 'Lux Viktor Szepesváralján (ma Spišské Podhradie, Szlovákia) született 1866. március 26-án, német származású családban. Felesége, <b style="cursor: pointer;" onclick="onImage(2)">Ehlers Alice</b> 1879-ben látta meg a napvilágot. <b style="cursor: pointer;" onclick="onImage(3)">Két gyermekük született</b>: Katalin 1908-ban, Rózsa pedig 1909-ben. Mindkét lány már a trianoni béke által meghatározott országrészben, Szegeden szereztek tanári diplomát. <br />', 'Lux Viktor <b style="cursor: pointer;" onclick="onImage(4)">Eperjesen</b>, az <b style="cursor: pointer;" onclick="onImage(5)">evangélikus kollégiumban </b> végzett tanítóként, majd <b style="cursor: pointer;" onclick="onImage(6)">Felkán (a)</b><b style="cursor: pointer;" onclick="onImage(7)">(b)</b> (ma Poprád település része Szlovákiában) kapott tanítói állást, idővel pedig iskolaigazgató lett. A vesztes első világháború után, 1920 augusztusában a csehszlovák állam <b style="cursor: pointer;" onclick="onImage(8)">kiutasította</b> az országból a Lux-családot. Viktor testvére, Lux Elek – aki kékfestő volt – Németországba települt át, Viktornak azonban, mint magyar közalkalmazottnak (annak ellenére, hogy haláláig német identitású volt), Magyarországra kellett menekülnie. Fiatalabb lánya, Rózsa elmondása alapján szülei halálukig németül beszéltek velük már bőven a Magyarországra való menekülés után is.<br />', '<b>A menekülés története:</b><br />', 'A Lux-család teherkocsikban <b style="cursor: pointer;" onclick="onContentImage()">menekült</b> Magyarországra. 1920. szeptember 22-én lépték át a magyar határt Hidasnémetinél. Először Gödöllőig jutottak, ahol az állomáson voltak kénytelenek lakni vagonokban, meglehetősen rossz körülmények között, majd a pestszentlőrinci állomásra „költöztek” át. Két vagonuk volt, az egyikben a mindennapokhoz szükséges életteret rendezték be, a másikban pedig magukkal hozott ingóságaikat, értékesebb bútoraikat, háztartási eszközeiket tárolták. Ezek az átmenekített <b style="cursor: pointer;" onclick="onImage(9)">bútorok és berendezési tárgyak</b> a mai napig a leszármazottak tulajdonában vannak, melyek között kisebb és nagyobb tálalószekrényt, egy nagy asztalt, két karosszéket, egy intarziás dohányzóasztalt és két kisebb egyenes támlás széket találunk. Ezeket a vagonokban áttelepített darabokat 1907-ben gyártották, hátulsó részükön felirat jelzi, hogy Lux Viktor részére készültek. Felkáról kísérték el a családot egészen az új hazáig.<br />', 'Sorsuk végül szerencsésen alakult, 1924. november 6-án Viktor megkapta a <b style="cursor: pointer;" onclick="onImage(10)">magyar állampolgárságot</b>, mely feleségére és lányaikra is kiterjedt. 1933-ban <b style="cursor: pointer;" onclick="onImage(11)">Pestszentlőrincen</b> kaptak lakást az állami lakótelepen, melyet béreltek. <b style="cursor: pointer;" onclick="onImage(12)">Lux Viktor</b><b style="cursor: pointer;" onclick="onImage(13)">(a)</b><b style="cursor: pointer;" onclick="onImage(14)">(b)</b> a két világháború között a Vallás- és Közoktatásügyi Minisztériumban volt tanfelügyelő, tulajdonképpen nyugállományban. Mivel Csehszlovákiában németként volt számontartva, ezért a csehszlovák állam nem fosztotta meg földjétől és vagyonától, így egészen 1945-ig volt <b style="cursor: pointer;" onclick="onImage(15)">jövedelme</b> a Felkán maradt birtokok után járó bérleti díjakból. 1948. február 23-án hunyt el Budapesten, özvegye 1972-ben halt meg. Dédunokájuk, ifj. Buzás Miklós a Szabadtéri Néprajzi Múzeum főépítésze.<br />',
    '<b>Források</b><br />'
            + 'BUZÁS Miklós szíves közlése alapján<br />'
            + 'SZŰCS István Gergely: Sikerek, kompromisszumok és kudarcok a felvidéki menekültek integrációs folyamataiban. In: Fórum, 12. évf. (2010) 4. sz. 3–24.',
  ],
  slug: 'lux-viktor',
  thumbnail: '/trianon_images/lux_viktor/__________0_THUMBNAIL.jpg',
  images: [{
    src: '/trianon_images/lux_viktor/__________0.jpg',
    description: '(0)	Lux Viktor portréja, 1937. (Magántulajdon) ',
  },
  {
    src: '/trianon_images/lux_viktor/__________1.jpg',
    description: '(1)	Ehlers Alice portréja, 1937. (Magántulajdon)',
  },
  {
    src: '/trianon_images/lux_viktor/__________2.png',
    description: '(2)	Lux Viktor családjával: feleségével, gyermekeivel és unokáival, 1942. (Magántulajdon)',
  },
  {
    src: '/trianon_images/lux_viktor/__________3.jpg',
    description: '(3)	Eperjes, Jókai tér, balra a Postapalota, jobbra a Seminarium épülete, háttérben a Ferencesek temploma, 1912. (Fortepan/Magyar Földrajzi Múzeum/Erdélyi Mór cége/86611)',
  },
  {
    src: '/trianon_images/lux_viktor/__________4.png',
    description: '(4)	Az eperjesi evangélikus kollégium. (Zempléni Múzeum, 120010)',
  },
  {
    src: '/trianon_images/lux_viktor/__________5a.png',
    description: '(5)	a) Felka Tátra Szepes-Zips, 1902 körül. (Zempléni Múzeum, 0121453)',
  },
  {
    src: '/trianon_images/lux_viktor/__________5b.jpg',
    description: '(5)  b) Poprád Magas-Tátra a Szent Egyed templom tornyából nézve. Középen, távolabb Felka (ekkor önálló település, ma a város része), 1906. (Fortepan/Magyar Földrajzi Múzeum/Erdélyi Mór cége/86552)',
  },
  {
    src: '/trianon_images/lux_viktor/__________6a.png',
    description: '(6)	Cseh nyelvű kiutasítási végzés Lux Viktor részére, 1920. augusztus 4. (Magántulajdon)',
  },
  {
    src: '/trianon_images/lux_viktor/__________6b.jpg',
    description: '(6)	Cseh nyelvű kiutasítási végzés Lux Viktor részére, 1920. augusztus 4. (Magántulajdon)',
  },
  {
    src: '/trianon_images/lux_viktor/__________8.jpg',
    description: '(8)	A többek között Lux Viktor által átmenekített bútorral és háztartási eszközökkel berendezett Vagonlakók – Trianon árvái című időszaki kiállítás a Skanzenben, 2020. (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/lux_viktor/__________9a.png',
    description: '(9)	a) Lux Viktor állampolgársági bizonyítványa 1924-ből, melyet csak 1933-ban kapott kézhez. (Magántulajdon) ',
  },
  {
    src: '/trianon_images/lux_viktor/__________9b.png',
    description: '(9)  b) Értesítés az állampolgársági bizonyítvány postázásáról, 1933. október 5. (Magántulajdon)',
  },
  {
    src: '/trianon_images/lux_viktor/__________10a.png',
    description: '(10)	Kivonat Pestszentlőrinc község képviselőtestületének 1933. évi rendkívüli közgyűlésének jegyzőkönyvéből, benne a véghatározattal, mely Lux Viktor illetőségi helyéül Pestszentlőrincet jelölte ki. (Magántulajdon)',
  },
  {
    src: '/trianon_images/lux_viktor/__________10b.png',
    description: '(10)	Kivonat Pestszentlőrinc község képviselőtestületének 1933. évi rendkívüli közgyűlésének jegyzőkönyvéből, benne a véghatározattal, mely Lux Viktor illetőségi helyéül Pestszentlőrincet jelölte ki. (Magántulajdon)',
  },
  {
    src: '/trianon_images/lux_viktor/__________11.png',
    description: '(11)	Lux Viktorról készült olajfestmény. (Magántulajdon)',
  },
  {
    src: '/trianon_images/lux_viktor/__________12.png',
    description: '(12)	Lux Viktor Balatonszemesen feleségével és unokájával, id. Buzás Miklóssal, 1939. január 18. (Magántulajdon)',
  },
  {
    src: '/trianon_images/lux_viktor/__________13.png',
    description: '(13)	Lux Viktor feleségével, Ehlers Alice-val 1942-ben. (Magándulajdon)',
  },
  {
    src: '/trianon_images/lux_viktor/__________14.jpg',
    description: '(14)	Lux Viktor részére szóló, a Szlovák Gazdasági Banktól magyar nyelven (!) kapott postatakarékpénztár útján történő átutalási igazolás. (Magántulajdon)',
  },
  ],
  contentImages: [
    '/trianon_images/lux_viktor/konyv/1.png',
    '/trianon_images/lux_viktor/konyv/2.png',
    '/trianon_images/lux_viktor/konyv/3.png',
    '/trianon_images/lux_viktor/konyv/4.png',
    '/trianon_images/lux_viktor/konyv/5.png',
    '/trianon_images/lux_viktor/konyv/6.png',
    '/trianon_images/lux_viktor/konyv/7.png',
    '/trianon_images/lux_viktor/konyv/8.png',
    '/trianon_images/lux_viktor/konyv/9.png',
    '/trianon_images/lux_viktor/konyv/10.png',
    '/trianon_images/lux_viktor/konyv/11.png',
    '/trianon_images/lux_viktor/konyv/12.png',
    '/trianon_images/lux_viktor/konyv/13.png',
    '/trianon_images/lux_viktor/konyv/14.png',
    '/trianon_images/lux_viktor/konyv/15.png',
    '/trianon_images/lux_viktor/konyv/16.png',
    '/trianon_images/lux_viktor/konyv/17.png',
  ],
  contentText: 'Az 1920-ban alapított Országos Menekültügyi Hivatal által Lux Viktor részére kiadott fényképes igazolókönyv, 1920. (Magántulajdon)',
  lead: '',
},
{
  id: 5,
  name: 'Orning Antal',
  title: '',
  content: [
    '<b>Honnan:</b> Izdenc (horvátul: Zdenci) falu és község (općina) Horvátországban Verőce-Drávamente megyében<br />'
            + '<b>Hová:</b> Pécs<br />'
            + '<b>A menekült: Orning Antal állomásfőnök</b>',
    '<b>Életrajz:</b><br />', 'Orning Antal 1877-ben látta meg a napvilágot Nagykanizsán. Római katolikus családban nevelkedett, a középiskolai végzettség megszerzése után rövid ideig szülőhelyén volt adóhivatali díjnok, majd kötelező katonai szolgálati idejének letöltése után – melyet a Vasúti és Távírda-ezrednél teljesített – 1901-ben lépett a Magyar Királyi Államvasutak szolgálatába. Öt gyermeke született. 1914-ig, a nagy háború kitöréséig <b style="cursor: pointer;" onclick="onImage(1)">a horvát vonalakon</b> szolgált állomásfelvigyázóként, majd saját kérésére a Baranyasellye–Nekcse (horvátul Našice) vonal izdenci állomására (horvát nevén zdenci, maga az állomás Izdenc és Raholca/Orahovica között volt) helyezték át, ahol idővel állomásfőnök lett. Az Orning-család háztartásának fő bevételi forrását a <b style="cursor: pointer;" onclick="onImage(2)">vasutas tisztviselő</b> fizetése jelentette.',
    '<b>A menekülés története:</b><br />', '	1918 őszén már világosan látszott, hogy a Monarchia elveszti az első világháborút. Az összeomláskor a többi magyar vasúti alkalmazotthoz hasonlóan Orning Antalnak is menekülnie kellett, október 28-án a MÁV Pécsi Üzletvezetőség táviratban rendelte el a vonal kiürítését. Aznap 16 óra 50 perckor indult meg a vasutasokért az 5102. sz. menekítő vonat <b style="cursor: pointer;" onclick="onImage(3)">Szentlőrincről </b>, három személy- és harminc üres <b style="cursor: pointer;" onclick="onImage(4)">teherkocsival</b>. A szerelvény a mintegy 90 km-re lévő Nekcséig ment, ahonnan másnap éjjel 1 óra 40 perckor indult vissza, és délben ért vissza Szentlőrincre, magyar területre. Útközben Izdencen két fedett teherkocsit akasztottak le Orning Antal számára. Az egyik vagonba a fehérneműt, felsőruházatot és az ágyneműt tették és maga a család is ebben utazott, a másikba pedig a <b style="cursor: pointer;" onclick="onImage(5)">háziállatok </b> kerültek, köztük két szarvasmarha, tizenegy (!) disznó, tizennégy (!) liba és öt tyúk. A kaotikus állapotok és a sok állat miatt a család értékesebb ingóságait (az ágyakat, ruhás- és éjjeliszekrényt, a díványt, az ottománt, a varrógépet, különféle székeket, asztalokat és lámpákat, a függönyöket, az ébresztőórát, a könyveket stb.) nem tudta magával vinni. A hátramaradt bútorokat, a háztartási eszközöket és az élelmiszert a helyi lakosok és szökött, dezertált katonák hordták szét, a felesleges tárgyakat elégették. Orning a menekülés következtében őt ért anyagi veszteségek miatt kártérítésért folyamodott mind a MÁV-hoz, mind pedig a Szerb–Horvát–Szlovén Királysághoz, amit egy <b style="cursor: pointer;" onclick="onImage(6)">részletes kimutatással (a)<b style="cursor: pointer;" onclick="onImage(7)">(b)</b> is indokolt. A MÁV először kétezer korona előleget jutatott számára, majd pedig összesen 87 808 koronát előjegyzésbe vett, tehát ekkora összeget ismert el veszteségként. A kártérítési ügyiratból világossá válik, hogy a 18 évnyi szolgálat alatt egy ötgyermekes vasúti hivatalnok meglehetősen magas életszínvonalat tudott biztosítani magának, majd pedig hirtelen át kellett élnie, hogy mindazt egy csapásra elveszíti, amiért egész addigi életében dolgozott.<br />', 'Orning Antal 1919. február 7-én kapta meg áthelyezését <b style="cursor: pointer;" onclick="onImage(8)">Pellérd-Keszü (Pécs–Harkányfürdő vonal) állomásra</b>, ahol 1923-ig teljesített szolgálatot. Ekkor már házvásárlást tervezett, gyermekeit gimnáziumban és reáliskolában taníttatta. Annak ellenére, hogy a kártérítésként igényelt összeget még 1923 májusában sem kapta meg, vagyoni helyzete meglepően rövid idő alatt, valószínűleg nem legális módon stabilizálódott, sőt, javult. Ugyanebben az évben egy <b style="cursor: pointer;" onclick="onImage(9)">kiterjedt csalási ügybe</b><b style="cursor: pointer;" onclick="onImage(10)">(a)</b> keveredett és bár a legsúlyosabb vádpontokban felmentették, szolgálatba már nem helyezték vissza, 1924-ben nyugdíjazták. 66 korona 50 fillér összeget állapítottak meg nyugdíjként számára. <b style="cursor: pointer;" onclick="onImage(11)">1932. június 22-én hunyt el</b>, halálhíréről a Dunántúl című újság is beszámolt.',
    '<b>Források</b><br />'

            + 'MATUS László: A horvátországi vasútvonalak evakuálása 1918–1919-ben. (Orning Antal állomásfőnök kálváriája). 2016.<br />'
            + '<a href="https://www.mavcsoport.hu/sites/default/files/upload/page/honap_dokumentuma_mav-os_menekultek_1918-1919_0.pdf">https://www.mavcsoport.hu/sites/default/files/upload/page/honap_dokumentuma_mav-os_menekultek_1918-1919_0.pdf</a><br />'
            + 'SZŰCS István Gergely: Vasutas vagonlakók és a MÁV menekültpolitikája 1918–1924. In: Múltunk, 57. évf. (2012) 4. sz. 89–112.<br />'
            + 'NAGY Tamás: Trianon és a magyar vasút. In: DÖBÖR András – KISS Gábor Ferenc (szerk.): Magyarország és Európa 1919–1939. Belvedere Meridionale, Szeged, 2001. 149–166.<br />'
            + 'Dunántúl, 1932. június 23. XIII. évf. 140. sz.7.<br />'
            + 'Pécsi Lapok, 1923. november 27. II. évf. 269. sz. 1.<br />'
            + 'Új Nemzedék, 1923. november 29. V. évf. 270. sz. 2.<br />',
  ],
  slug: 'orning-antal',
  thumbnail: '/trianon_images/orning_antal/2_THUMBNAIL.jpg',
  images: [{
    src: '/trianon_images/orning_antal/2.jpg',
    description: '(2)	MÁV vasúti tisztviselő portréja, 1910-es évek. (Magántulajdon)',
  },
  {
    src: '/trianon_images/orning_antal/1.jpg',
    description: '(1)	A Magyar Királyi Államvasutak kezelésében lévő helyi érdekű vasutak térképe, 1910-s évek eleje. (Magyar Műszaki és Közlekedési Múzeum, MMKM TKGY 2948)',
  },
  {
    src: '/trianon_images/orning_antal/3.jpg',
    description: '(3)	A szentlőrinci vasútállomás, 1910 körül. (Zempléni Múzeum, 0086223)',
  },
  {
    src: '/trianon_images/orning_antal/4.jpg',
    description: '(4)	Vasutas menekült és családja egy tehervagon előtt, 1919 ősz. Harsányi Gyula felvétele. (Magyar Nemzeti Múzeum, Történeti Fényképtár, 2494/1957 fk)',
  },
  {
    src: '/trianon_images/orning_antal/5.jpeg',
    description: '(5)	Járó Imre: Szükséglakások a ferencvárosi rendezőn. (Ágoston Vilmos: A főváros vagonlakói. In: Budapest, 30. évf. (2007) 11. sz. 14.)',
  },
  {
    src: '/trianon_images/orning_antal/6a.jpg',
    description: '(6)	Orning Antal által készített leltár az elszenvedett anyagi veszteségekről, 1920. (3674/1920. sz. ügyirat, MÁV SZK Zrt. MTÜ Archívum, MÁV Pécsi Igazgatóság iktatott iratai, AG.11415. doboz)',
  },
  {
    src: '/trianon_images/orning_antal/6b.jpg',
    description: '(6)	Orning Antal által készített leltár az elszenvedett anyagi veszteségekről, 1920. (3674/1920. sz. ügyirat, MÁV SZK Zrt. MTÜ Archívum, MÁV Pécsi Igazgatóság iktatott iratai, AG.11415. doboz)',
  },
  {
    src: '/trianon_images/orning_antal/7.jpg',
    description: '(7)	Az egykori Pellérd-Keszü állomás épülete, mely Pécsről Harkány felé haladva a pálya jobbkéz felőli oldalán volt. (vasutallomasok.hu)',
  },
  {
    src: '/trianon_images/orning_antal/8.jpg',
    description: '(8)	A Pécsi Lapok, 1923. november 27-i száma, mely beszámol a családi ügyről: Szélhámoskodás hamis fuvarlevelekkel. Az utóbbi napokban igen sok feljelentés érkezett a főkapitányságra ismeretlen tettesek ellen, akik hamis vasúti fuvarlevelek segítségével érzékenyen megkárosították több fővárosi terménykereskedőt. Körülbelül tizenkét ilyen feljelentés érkezett a főkapitányságra, ahol Munkácsy defektivfelügyelő csoportja kezdte meg a nyomozást. A detektívek megállapították, hogy a terménykereskedőknél megjelent két vidéki gazdának öltözött ember, akik vagoótételekben ajánlottak megvételre különböző terményeket, a napi árnál valamivel mindig olcsóbb volt ajánlatuk és igy a kereskedők készséggel vállalkoztak az áru megvételére. A két csaló mindjárt vasúti fuvarlevélmásolatokat mutatott fel, azokkal igazolták, hogy az áru már valamelyik nagyobb vidéki állomáson fel van adva. A fuvarlevélmásolatok teljesen szabályszerűen voltak minden esetben kiállítva a Máv. körbélyegzőjével és a feladóállomás bélyegzőjével ellátva, úgy, hogy a kereskedők egész nyugodtan foglalózták le az árut. Néhány nap elteltével, amikor az áru késlekedése miatt érdeklődtek a Máv-nál, azt a felvilágosítást kapták, hogy a bemutatott fuvarlevelek hamisítványok. A detektivek a nyomozás során megállapították, hogy a két szélhámos minden egyes alkalommal más-más néven mutatkozott be s ez megnehezítette kézrekeritésüket. A detektivek felszólították a főváros valamennyi terménykereskedőit, hogy azon esetben, ha a két szélhámos jelentkezik náluk, azonnal értesítsék a rendőrséget. Egy előkelő fővárosi terménykereskedőhöz beállított két vidéki gazdálkodónak látszó ember, akik nagyobb tétel babot ajánlottak megvételre és fuvarleveleket mutattak fel. A kereskedő leültette ezeket a „gazdálkodókat”, azonban nyomban jelentést tett a főkapitányságon, ahonnan Szigeti István és Lenkei Márton detektívek azonnal automobilon siettek ki a kereskedőhöz. Kiérkezve oda, a két gazdálkodóval tárgyalásba bocsátkoztak az általuk ajánlott áru megvételére. A gazdálkodók átadták a detektiveknek a fuvarleveleket, amelyekről kiderült, hogy szintén hamisak. A detektivek a két szélhámost előállították a főkapitányságon, ahol igazoltatásuk alkalmával kitünt, hogy az egyik Schwarz Mór ló- kereskedő, a másik pedig Pap József hentesmester, mind a ketten pécsi lakósok. A két szélhámos kihallgatása során bevallotta, hogy a hamis fuvarlevelekkel körülbelül mér egy év óta manipulálnak és összesen mintegy százmillió koronával károsították meg a főváros terménykereskedőit. A rendőrség vallomásuk alapján előzetes letartóztatásba helyezte a két szélhámost. A budapesti kihallgatáson Schwarz Mór azt vallotta, hogy a fuvarleveleket Sgainer Henrik Máv. elüljáró, Pécs Báró Bánffy Dezső utcai lakos hamisította részére. Sgainer Henrik elöljárót a pécsi államrendőrség előállította s előzetes letartóztatásba helyezte. Sgainer ellenben tagadja a terhére rótt cselekményt. Azt állítja, hogy 19-én felkereste őtet Schwarz, kit még Slavóniából ismer és elmondotta neki, hogy olcsón adott el búzát s szeretné az üzletet stornirozni [sztornózni]. Nem bánja, ha az ügy 1½ millióba kerül is. Reá akarta őtet venni, hogy a fuvarleveleit a magyar államvasut körbélyegzőjével és Pécsnek mint feladó állomásnak a bélyegzőjével lássa el. Ő ezt visszautasította. Huszonegyedikén Schwarz Mór ismét felkereste és értesítette, hogy a dolog sikerült. A fuvarleveleket Orning Antal Pellérd-Keszü állomás elöljárója bélyegezte le, ki ezért tőle 300.000 koronát kapott. Sgainer vallomása alapján előállították Orning Antalt. Az államrendőrségen Orning beismerte, hogy 21-én 1 óra körül ott volt nála Schwarz s ekkor meg is egyeztek. A terhére rótt cselekményt négy ízben elkövette, de Schwarztól pénzt még nem kapott érte. Orning vallomásáról telefon által értesítették a budapesti államrendőrséget, amely ennek következtében tegnap délelőtt ismét kihallgatta Schwarzot. A vizsgálat pécsi eredményének hatása alatt Schwarz tökéletesen beismerte a bűncselekményt. Fenntartja előbbi vallomásának Sgainerra vonatkozó részét. Szerinte Orning tőle 500.000 koronát kapott. A vizsgálat pécsi részének befejezése után a budapesti államrendőrség kérése szerint Orningot Budapestre fogják kísérni. […]',
  },
  {
    src: '/trianon_images/orning_antal/9.jpg',
    description: '(9)	Az Új Nemzedék, 1923. november 29-i száma, mely beszámol a csalási ügyről: <br />'
                    + '150 milliós csalás miatt letartóztattak két pécsi ügynököt.<br />'
                    + '— Az Uj Nemzedék tudósítójától. — <br />'
                    + 'A budapesti álamrendőrség többrendbeli csalás és okiratamisitásárt letartóztatta Schwartz Mór és Papp Imre pécsi ügynököket Schwartz a kihallgatás folyamán elmondotta, hogy több vagon gabonát adott el. Azonban a gabona ára időközben felszökött s az üzletet stornirozni [sztornózni] akarta. Erre a célra hamisított fuvarlevelekre volt szüksége. A fuvarlevelek megszerzése végett elment Sgainer Henrik, pécsi állomáselőljáróhoz, majd Orning Antalhoz, a pellérdkeszüi állomás vezetőjéhez, hogy hamis fuvarlevelet adjanak neki, illetve az általa adott üres szállítólevelekre hamis bélyegzőt üssenek rá Sgainernek ezért 300.000, Orningnak pedig 500.000 koronát adott. Az államrendőrség Schwartz e vallomásáról rögtön értesítette a pécsi kapitányságot, amely Sgainer Henriket és Orning Antalt a csendőrség utján előállította. Sgainer Henrik tagadta a terhére rótt bűncselekményt, míg Orning Antal a hamisítást töredelmesen bevallotta, de határozottan kijelentette, hogy ezért pénzt nem fogadott el. A pécsi rendőrség e két vallomást telefonon közölte a budapesti rendőrséggel, ahol ujra kihallgatták Schwartzot, Schwartz ezután is azt vallotta, hogy úgy Orning Antaltól, mint Sgainer Henriktől hamis bélyegzést kapott. Orningnek ezért félmilliót, Sgainernek pedig 300.000 koronát fizetett. Az államrendőrség e vallomást nyomban közölte a pécsi kapitánysággal, azzal, hogy mind a két vasutast tartóztassa le. Ez meg is történt s mindkettőjüket a tegnap esti vonattal Budapestre kisérték. Schwartz Mór Pécsett igen gazdag ember hírében áll. Ő vásárolta meg annak idején a muzeum épületét 32 millióért, amelyet azután rövidesen 132 millióért adott el a budapesti Gazdák Bankjának. A pécsi Munkácsy Mihály-utcában vásárolt 13 millióért egy házat, amelyet néhány napra rá 60 millióért adott el. Schwartz Mór tehát az okozott károkat előreláthatólag megtérítheti. Hozzátartozói kaució ellenében máris kérték szabadlábrahelyezését.',
  },
  {
    src: '/trianon_images/orning_antal/10.jpg',
    description: '(10)	A Dunántúl 1932. június 23-i száma, benne Orning Antal halálhírével.',
  },
  ],
  lead: '',
},
{
  id: 6,
  name: 'Riechel Jenő',
  title: '',
  content: [
    '<b>Honnan:</b> Veliki Bastaji falu Horvátországban, Pozsega megyében<br />'
            + '<b>Hová:</b> Bars, Nagykanizsa<br />'
            + '<b>A menekült: Riechel Jenő gyermek, később mozdonyvezető</b>',
    '<b>Életrajz:</b><br />', 'Riechel Jenő 1912. február 4-én született Veliki Bastaji-ban (a mai Horvátország területén fekszik, egykor a Magyar Királyságon belül Horvát–Szlavónország részeként, Pozsega vármegye Daruvári járásának része volt), ahol az édesapja pályamesterként dolgozott. <br />', 'Harmadik generációs vasutas, <b style="cursor: pointer;" onclick="onImage(1)">nagyapja Riechel György</b> és <b style="cursor: pointer;" onclick="onImage(2)">édesapja, aki szintén György (1875-1928)</b> a Magyar Királyi Államvasutaknál szolgált. <b style="cursor: pointer;" onclick="onImage(3)">Édesanyja Fischer Krisztina (1889-1952) </b><b style="cursor: pointer;" onclick="onImage(4)">(b)</b><br />', 'A gyermek Riechel Jenőnek az volt az álma, hogy mozdonyvezető lehessen. Rang volt a vasútnál dolgozni. Az egyik első mozdonyvezetői útján III. Viktor Emanuel olasz király vonatát felvezető, úgynevezett futármozdonyt vezette 1937-ban. <b style="cursor: pointer;" onclick="onImage(5)">Kedvenc mozdonya a 424-es volt.</b><br />', '1967. április 1-jén <b style="cursor: pointer;" onclick="onImage(6)">vonult nyugdíjba a vasúttól.</b> <br />', '2013 májusában 102 évesen halt meg, Nagykanizsán helyezték örök nyugalomra.',
    '<b style="cursor: pointer;" onclick="onImage(7)">A menekülés történetének forrása: Saját visszaemlékezése, amelyet 2011-ben készített interjúban mesélt el.</b><br />'
            + '1920-ban menekültek Magyarországra tehervagonokban, először Barcsra, majd Nagykanizsára.  Nagykanizsára a Muraközből, Horvátországból érkeztek menekültek, <div class="quote"><h2>A magyar vasutasokat elküldik a horvátok \n'
            + 'Nagykanizsa, február 27. (A Világ saját tudósitójától.) A jugoszláv kormány Zágrábban horvát vasutas-tanfolyamat állított fel s a tanfolyamot elvégzettekkel leváltja a magyar vasutasokat, akiket nagyrészben Nagykanizsán helyeznek el. A közeli jövőben ismét 50-60 vasutascsalád érkezik Nagykanizsára, akiket a barak-kórházban szállásolnak el. (Világ, 1919. március 1. 10. évfolyam, 52. szám)</h2> </div>de jöttek a Felvidékről és Erdélyből is. A város nem tudott azonnal lakást biztosítani a menekülteknek, ezért a vasút segítségével a pályaudvar arra alkalmas részén, a raktárvágányon tehervagonokat állítottak ki, és azokban helyezték el a menekülteket, <div class="quote"><h2>A trianoni menekültek – nagyszámban muraköziek – letelepítése évekig nagy gondot jelentett. A helyi lap szerint 1920 augusztusában tizenhét család élt a teherpályaudvar tizenhét vagonjában. Mire megjöttek a téli nagy hidegek sikerült őket normálisabb helyzetbe juttatni, de érkeztek a mindenüket elvesztők újabb és újabb csapatai. 1921 novemberében huszonegy vagonlakó család húzta meg magát a kanizsai síneken. (Tarnóczky Attila:Hol, mi? Kanizsai házak és lakói)</h2></div>mintegy száz családot, gyakran 4-5 gyermekkel. A vagonlakók sokat szenvedtek a hidegtől és az alapvető higiénia hiányától is. Sokszor még a vízvétel is probléma volt. A vagonban ágyak voltak, kályhával fűtöttek. Több mint félévig éltek itt Riechel Jenőék is. Ezután költöztették be őket az úgynevezett <b style="cursor: pointer;" onclick="onImage(8)">Barakk-kórházba</b> <b style="cursor: pointer;" onclick="onImage(9)">(c)</b> <b style="cursor: pointer;" onclick="onImage(10)">(d)</b> <b style="cursor: pointer;" onclick="onImage(11)">(e)</b> <b style="cursor: pointer;" onclick="onImage(12)">(f)</b>, az egykori katonai kórház területén kialakított szükséglakásokba. Ekkor már sokan találtak munkát a vasúton, a Franck Kávégyárban, a honvédkórházban, a város egyéb üzemeiben, iskoláiban, hivatalaiban. A szükséglakások lakói igyekeztek otthont varázsolni átmentett bútoraikkal és környezetük szépítésével. ',
  ],
  slug: 'riechel-jeno',
  thumbnail: '/trianon_images/riechel_jeno/2_THUMBNAIL.jpg',
  images: [{
    src: '/trianon_images/riechel_jeno/2.jpg',
    description: '(2) A Barcs – Pakrác vasútvonalon fekvő Szircs vasútállomása. A Magyar Királyságon belül Horvát–Szlavónország részeként, Pozsega vármegye Daruvári járásának része volt Szircs (más néven Szirács, horvátul: Sirač) falu és község. Ma Horvátország része.',
  },
  {
    src: '/trianon_images/riechel_jeno/3.jpg',
    description: '(3) Riechel György, az édesapa vasutas egyenruhában. A fotó Wanaus József (Budapest, Sütő utca 2.) fényképészeti üzletében készült.',
  },
  {
    src: '/trianon_images/riechel_jeno/4a.jpg',
    description: '(4a) A szülők esküvői képe',
  },
  {
    src: '/trianon_images/riechel_jeno/4b.jpg',
    description: '(4b) A jobb oldalon Riechel Jenő édesanyja, Fischer Krisztina, aki harmadik gyermekével Tiborral várandós.',
  },
  {
    src: '/trianon_images/riechel_jeno/5.jpg',
    description: '(5) A 424-es mozdony. Nagykanizsa, 1936',
  },
  {
    src: '/trianon_images/riechel_jeno/6.jpg',
    description: '(6) Riechel Jenő nyugdíjas korában',
  },
  {
    src: '/trianon_images/riechel_jeno/7_jeno_bacsi.mp4',
    description: '(7) Interjú Riechel Györggyel',
    thumbnail: '/trianon_images/riechel_jeno/6.jpg',
  },
  {
    src: '/trianon_images/riechel_jeno/10b.png',
    description: '(10b) A barakk kórház. Reprodukció: Thúry György Múzeum Adattári fotótár. Az eredeti felvételek Dr. Szabó Csaba és Gayer Ildikó (Nagykanizsa) tulajdona',
  },
  {
    src: '/trianon_images/riechel_jeno/10c.png',
    description: '(10c) A barakk kórház. Reprodukció: Thúry György Múzeum Adattári fotótár. Az eredeti felvételek Dr. Szabó Csaba és Gayer Ildikó (Nagykanizsa) tulajdona',
  },
  {
    src: '/trianon_images/riechel_jeno/10d.png',
    description: '(10d) A barakk kórház. Képeslap. Thúry György Múzeum Adattári fotótár.',
  },
  {
    src: '/trianon_images/riechel_jeno/10e.jpg',
    description: '(10e) A barakk kórház. Képeslap. Thúry György Múzeum Adattári fotótár.',
  },
  {
    src: '/trianon_images/riechel_jeno/10f.png',
    description: '(10f) A barakk kórház helyszínrajza. Thúry György Múzeum Adattári fotótár.',
  },
  ],
  lead: '',
},
{
  id: 7,
  name: 'Tolcsvay Nagy Géza',
  title: '',
  content: [
    '<b>Honnan:</b> Hátszeg (románul: Hațeg, németül: Hatzeg, Hötzing vagy Wallenthal) város Romániában, Erdélyben, Hunyad megyében.<br />'
            + '<b>Hová:</b> Budapest<br />'
            + '<b>A menekült: Tolcsvay Nagy Géza és Pécsi Berta tanárok, gyermekeik Géza és Béla</b>',
    '<b>Életrajz</b><br />', '<b style="cursor: pointer;" onclick="onImage(1)">Tolcsvay Nagy Géza</b> 1880. június 4-én született a Hunyad megyei <b style="cursor: pointer;" onclick="onImage(2)">Hátszegen</b>, amely ekkor román többségű település volt, majd 1900-ra megsokszorozódott a magyar nemzetiségűek aránya, és 1910-re nagyjából kiegyenlítődtek a nemzetiségi arányok.<br />', 'Itt lett tanító, és kötött házasságot Pécsi Bertával. Két gyermekük született Géza és Béla. ',
    '<b style="cursor: pointer;" onclick="onImage(3)">A menekülés történetének forrása: Unokájának, Tolcsvay Lászlónak a visszaemlékezése</b><br />', '1920-ban nem tették le a román hűségesküt, nem voltak hajlandóak románul tanítani a magyar diákokat. Egy éjszaka kellett elhagyniuk szülőföldjüket egy szekérre felpakolható ingósággal. A család a ruhaneműek mellett néhány bútort, és <b style="cursor: pointer;" onclick="onImage(4)">a család zongoráját (a)</b><b style="cursor: pointer;" onclick="onImage(5)">(b)</b><b style="cursor: pointer;" onclick="onImage(6)">(c)</b><b style="cursor: pointer;" onclick="onImage(7)">(e)</b><b style="cursor: pointer;" onclick="onImage(8)">(f)</b> hozta magával. Szekérrel utaztak Debrecenig, ahol vagonokba pakolták őket. Egy ideig ott maradtak, és utána kerültek Budapestre, ahol öt évig éltek vagonban. Az éjszakákat nagyon rémisztően élték meg, a hideget. A gyermekek ott játszottak egymással, innen jártak iskolába. Mély nyomott hagyott a gyermekekben.<br />', 'A vagon-élet nagyon megviselte Pécsi Berta egészségét, és a 20-as évek elején a balatonfüredi szanatóriumban halt meg szívbetegségben. Tolcsvay Nagy László a két gyermekével továbbra is Budapesten, a<b style="cursor: pointer;" onclick="onImage(9)"> Déli pályaudvaron felállított vagonokban élt.</b> A két gyermeket a <b style="cursor: pointer;" onclick="onImage(10)">Gyermekmentő Liga</b> egy-évre Hollandiába és Svájcba vitte, de egymástól elszakítva töltötték ezt az időt.<br />', 'Később Szentesen telepedtek le, Tolcsvay Nagy Géza ott lett alapító iskolaigazgató a <b style="cursor: pointer;" onclick="onImage(11)">szentesi állami polgári leányiskolában.</b><b style="cursor: pointer;" onclick="onImage(12)">(a)</b><b style="cursor: pointer;" onclick="onImage(13)">(b)</b> Közben újra megnősült, és született egy harmadik gyermeke is.<br />', 'Később keveset mesélt a menekülés időszakáról. De az <b style="cursor: pointer;" onclick="onImage(14)">Erdélyből elhozott könyvek (a)</b><b style="cursor: pointer;" onclick="onImage(15)">(b)</b> meghatározóak maradtak. A gyermek felnőttként sértettséget érzett, az elveszített gyermekkor, az elveszített édesanya miatt, amelyet gyakran azonosított az ország elvesztésével.<br />', 'Csak az 1970-es években tudtak visszamenni, és megnézni az egykori családi házat.',
  ],
  slug: 'tolcsvay-nagy-geza',
  thumbnail: '/trianon_images/tolcsvay_nagy_geza/1_THUMBNAIL.jpg',
  images: [{
    src: '/trianon_images/tolcsvay_nagy_geza/1.jpg',
    description: '1.	Tolcsvay Nagy Géza portréja (Magántulajdon)',
  },
  {
    src: '/trianon_images/tolcsvay_nagy_geza/2.jpg',
    description: '2.	A hátszegi vasútállomás. Képeslap (Szerencsi Múzeum)',
  },
  {
    src: '/trianon_images/tolcsvay_nagy_geza/4a.jpg',
    description: '4. A Reményi Mihály féle zongorát 1912 telén vásárolta Tolcsvay Nagy Géza feleségének első gyermekük születésekor. A zongorát Hátszegről menekülve hozták magukkal Budapestre. Egy családnál helyezték el, amíg vagonban éltek. A család négy generációja tanult rajta. Először Tolcsvay Nagy Géza és Pécsi Berta gyermeke Béla tanult rajta, aki később kántorkodott a szentesi templomban, ahol –mivel jazz zenekara is volt- jazz-kántornak nevezték. Később Budapesten elvégezte a Zeneakadémiát, majd kávéházi zongorista lett. Utána fiai tanultak rajta a Tolcsvay Trió alapítói, Béla és László. Fotó: 4a-c. A fotót Tolcsvay László készítette.'
                    + 'Reményi Mihály hangszerkészítő (1867-1939) 1890-ben Budapesten nyitotta meg műhelyét és üzletét. Az általa készített hegedűk minősége nemzetközi szinten is elismertséget hozott számára. A fő profiljának számító vonós hangszerek mellett, sok más hangszer típussal is foglalkozott és kereskedett, többek között zongorával is. Ügyfeleik közé tartoztak a kor legnevesebb előadói, zenekarai, a világ minden tájáról – azon hangszerészek közé tartoztak, akik megkapták a királyi beszállítói címet. Műhelyük és üzletükBudapesten a Király utca 58. alatt működött. 1939-ben bekövetkezett halála után az üzletet fiai vitték tovább. A család 1959-ben emigrált Kanadába, ahol a mai napig jelentős hangszerüzlet házat működtetnek, Remenyi House of Music néven.',
  },
  {
    src: '/trianon_images/tolcsvay_nagy_geza/4b.jpg',
    description: '4. A Reményi Mihály féle zongorát 1912 telén vásárolta Tolcsvay Nagy Géza feleségének első gyermekük születésekor. A zongorát Hátszegről menekülve hozták magukkal Budapestre. Egy családnál helyezték el, amíg vagonban éltek. A család négy generációja tanult rajta. Először Tolcsvay Nagy Géza és Pécsi Berta gyermeke Béla tanult rajta, aki később kántorkodott a szentesi templomban, ahol –mivel jazz zenekara is volt- jazz-kántornak nevezték. Később Budapesten elvégezte a Zeneakadémiát, majd kávéházi zongorista lett. Utána fiai tanultak rajta a Tolcsvay Trió alapítói, Béla és László. Fotó: 4a-c. A fotót Tolcsvay László készítette.'
                    + 'Reményi Mihály hangszerkészítő (1867-1939) 1890-ben Budapesten nyitotta meg műhelyét és üzletét. Az általa készített hegedűk minősége nemzetközi szinten is elismertséget hozott számára. A fő profiljának számító vonós hangszerek mellett, sok más hangszer típussal is foglalkozott és kereskedett, többek között zongorával is. Ügyfeleik közé tartoztak a kor legnevesebb előadói, zenekarai, a világ minden tájáról – azon hangszerészek közé tartoztak, akik megkapták a királyi beszállítói címet. Műhelyük és üzletükBudapesten a Király utca 58. alatt működött. 1939-ben bekövetkezett halála után az üzletet fiai vitték tovább. A család 1959-ben emigrált Kanadába, ahol a mai napig jelentős hangszerüzlet házat működtetnek, Remenyi House of Music néven.',
  },
  {
    src: '/trianon_images/tolcsvay_nagy_geza/4c.jpg',
    description: '4. A Reményi Mihály féle zongorát 1912 telén vásárolta Tolcsvay Nagy Géza feleségének első gyermekük születésekor. A zongorát Hátszegről menekülve hozták magukkal Budapestre. Egy családnál helyezték el, amíg vagonban éltek. A család négy generációja tanult rajta. Először Tolcsvay Nagy Géza és Pécsi Berta gyermeke Béla tanult rajta, aki később kántorkodott a szentesi templomban, ahol –mivel jazz zenekara is volt- jazz-kántornak nevezték. Később Budapesten elvégezte a Zeneakadémiát, majd kávéházi zongorista lett. Utána fiai tanultak rajta a Tolcsvay Trió alapítói, Béla és László. Fotó: 4a-c. A fotót Tolcsvay László készítette.'
                    + 'Reményi Mihály hangszerkészítő (1867-1939) 1890-ben Budapesten nyitotta meg műhelyét és üzletét. Az általa készített hegedűk minősége nemzetközi szinten is elismertséget hozott számára. A fő profiljának számító vonós hangszerek mellett, sok más hangszer típussal is foglalkozott és kereskedett, többek között zongorával is. Ügyfeleik közé tartoztak a kor legnevesebb előadói, zenekarai, a világ minden tájáról – azon hangszerészek közé tartoztak, akik megkapták a királyi beszállítói címet. Műhelyük és üzletükBudapesten a Király utca 58. alatt működött. 1939-ben bekövetkezett halála után az üzletet fiai vitték tovább. A család 1959-ben emigrált Kanadába, ahol a mai napig jelentős hangszerüzlet házat működtetnek, Remenyi House of Music néven.',
  },
  {
    src: '/trianon_images/tolcsvay_nagy_geza/4d.jpg',
    description: '4 d. Reményi Mihály portréja. (Magyar Kereskedelmi és Vendéglátóipari Múzeum)',
  },
  {
    src: '/trianon_images/tolcsvay_nagy_geza/4e.jpg',
    description: '4 e. Reményi Mihály hangszerkészítő számolócédulája, 1910-es évek. (Magyar Kereskedelmi és Vendéglátóipari Múzeum, KD_1973.121.1.8)',
  },
  {
    src: '/trianon_images/tolcsvay_nagy_geza/4f.jpg',
    description: '4 f. Reményi Mihály Hangszer Ipar Telepe látható a Király utcában az 1890-es években. (Magyar Kereskedelmi és Vendéglátóipari Múzeum, KF_LTSZN_10_15)',
  },
  {
    src: '/trianon_images/tolcsvay_nagy_geza/5.jpg',
    description: '5.	Vagonlakó menekültek a budapesti pályaudvaron, 1920. Müllner János felvétele (Magyar Nemzeti Múzeum Történeti Fényképtár)',
  },
  {
    src: '/trianon_images/tolcsvay_nagy_geza/6_MFI_0114_a_szazadik_gyermekvonat_visszaerkezese_Belgiumbol.mp4',
    description: '6.	A századik gyermekvonat visszaérkezése Belgiumból. Filmhíradó részlet, 1926. április. (Nemzeti Filmintézet)',
    thumbnail: '/trianon_images/tolcsvay_nagy_geza/1.jpg',
  },
  {
    src: '/trianon_images/tolcsvay_nagy_geza/7.jpg',
    description: '7.	Iskolaigazgatói kinevezés a szentesi Állami Polgári Leányiskolába. Megjelent a Budapesti Közlöny 1925. április 23-i számában.',
  },
  {
    src: '/trianon_images/tolcsvay_nagy_geza/8.jpg',
    description: '8.	Tolcsvay Nagy Géza iskolaigazgató a szentesi Polgári Leányiskola tanári karában.',
  },
  {
    src: '/trianon_images/tolcsvay_nagy_geza/9.jpg',
    description: '9.	Tolcsvay Nagy Géza a szentesi Polgári Leányiskola egyik osztályával. ',
  },
  {
    src: '/trianon_images/tolcsvay_nagy_geza/10a.jpg',
    description: '10.	a-b. György Aladár: A Föld és népei. Franklin Társulat Kiadása. Budapest, 1906. (Fotó: Tolcsvay László)',
  },
  {
    src: '/trianon_images/tolcsvay_nagy_geza/10b.jpg',
    description: '10.	a-b. György Aladár: A Föld és népei. Franklin Társulat Kiadása. Budapest, 1906. (Fotó: Tolcsvay László)',
  },
  ],
  lead: '',
},
{
  id: 8,
  name: 'Weinstock Fülöp',
  title: '',
  content: [
    '<b>Honnan:</b> Nagyvárad (románul Oradea, németül Großwardein, szlovákul Veľký Varadín, jiddisül גרויסווארדיין; Groszvardajn) Város  Romániában, a Partiumban, Bihar megye székhelye<br />'
            + '<b>Hová:</b> Budapest<br />'
            + '<b style="cursor: pointer;" onclick="onImage(1)">A menekült: Weinstock Fülöp órás, ékszerész</b>',
    '<b>Életrajz:</b><br />',
    'Weinstock Fülöp fiának, Weinstock Ernő fotográfus képeslapkiadónak a hagyatéka a Szabadtéri Néprajzi Múzeum gyűjteményébe került. Az ebben fellelhető iratok, fotók alapján tudjuk rekonstruálni Weinstock Fülöp életrajzát.<br />',
    '<b style="cursor: pointer;" onclick="onImage(2)">Weinstock Fülöp</b> tokaji zsidó családban született 1865. szeptember 25-én, ahol édesapja Weinstock Jónás (1832-1906) órásként dolgozott, édesanyja Benjamin Róza (1838-1922). 1873-ban Tokajból Nagyváradra költözött a család. Fülöp 1886-ban órássegédként kezd el dolgozni apja üzletében. <b style="cursor: pointer;" onclick="onImage(3)">(b)</b> Az 1869-es cenzus szerint a szülők öt gyermekükkel (Háni (sz:1863), Ignácz (1864), Fülöp (1865), Jakab (1867), Móricz (1869)) és a református vallású cselédükkel Tóth Susannával (Zsuzsanna) laktak együtt a tokaji Nagy utca 306-ban. 1871-ben megszületett hatodik gyermekük Ilona is.<br />',
    'Fülöp 1892. december 4-én kötött házasságot a nagyváradi <b style="cursor: pointer;" onclick="onImage(4)">Weisz Eszter Etelkával (1873-1898). </b> Házasságukból három gyermek született Ernő (1893-1985), Margit (1896-1897) és Edit (?).<br />',
    'Az asszimilálódott zsidó házaspár a város magyar polgárságának a tagja volt. Weinstock Fülöp <b style="cursor: pointer;" onclick="onImage(5)">(a)</b> <b style="cursor: pointer;" onclick="onImage(6)">(b)</b> Weisz Gyulának volt üzlettársa Nagyváradon, és a nagyváradi, <b style="cursor: pointer;" onclick="onImage(7)">Fő utca 11. szám (az úgynevezett Bartsch-házban) alatti üzletben</b><b style="cursor: pointer;" onclick="onImage(8)">(b)</b> órát, optikai és fényképészeti cikkeket árusítottak.<br />',
    '<b style="cursor: pointer;" onclick="onImage(9)">Weinstock Fülöpné 1898-ban 25 évesen skarlátban meghalt. </b><b style="cursor: pointer;" onclick="onImage(10)">(b)</b>  <br />',
    'A háború előtti éveket Fülöp feltehetően Orosházán töltötte, ahol másodjára is megnősült, második felesége Somolyai Etel, aki 1874-ben Szilágysomlyón született. Ebből a házasságából több gyermeke született: Olga (1902-1903), Anna (1904) és Magda (1906). A házasság nem sikerült, erről egy a hagyatékban talált, a fiának írt levél tudósít. Második feleségről nincsenek információk. 1915-ben Budapesten vasúti pályaórás segéd, azaz a vasút óráinak javításával foglalkozik. <b style="cursor: pointer;" onclick="onImage(11)">(a)</b><b style="cursor: pointer;" onclick="onImage(12)">(b)</b><br />',
    '<b style="cursor: pointer;" onclick="onImage(13)">Legidősebb fia Ernő</b> 1910-ben Budapestre költözött, majd 1914 és 1918 között a Monarchia hadseregében, a légierőnél volt fényképész. <br />',
    '<b style="cursor: pointer;" onclick="onImage(14)">Az 1910-22 közötti évekről keveset tudunk. A háború elején 1905-ben a népfölkelési fegyveres szolgálatra alkalmatlannak tartották az akkor 50 éves, viszonylag alacsony férfit.</b><br />',
    'A háború után – miután Budapestre költözik- sok időt tölt családjával, <b style="cursor: pointer;" onclick="onImage(15)">fiával</b> és <b style="cursor: pointer;" onclick="onImage(16)">leányunokájával.</b><br />',
    '<b style="cursor: pointer;" onclick="onImage(17)">1924-ben útlevelet kérelmez</b>, hogy kivándoroljon Amerikába. Hogy pontosan mikor költözik ki, nem tudjuk.<br />',
    '1928-ban már biztosan New Yorkban él. Hiszen részt vesz az úgynevezett Kossuth zarándoklaton, melyet báró Perényi Zsigmond, volt belügyminiszter vezetett. A zarándokút csúcspontja a new yorki Kossuth szobor felavatása volt 25.000 résztvevő jelenlétében. 1928. március 15-én, az avatás napján a magyarok által hozott földet amerikai földdel keverték össze, majd az urnát a szobor talapzatában helyezték el. <b style="cursor: pointer;" onclick="onImage(18)">Minderről, és Weinstock Fülöp részvételéről Az Est című lap 1928. március 15-i száma tudósít. (Az Est, 19. évfolyam, 62. szám, 1928. American special edition)</b><br />',
    'Néhány levele megmaradt, amit a Budapesten élő fiának küldött, ezekben főleg a fiát figyelmezteti a Budapesten ráváró veszélyekről, a náci rémtettekről. Fia és a családja nem tudott kijutni Amerikába.<br />',
    '80 éves korában 1945. október 24-én halt meg New Yorkban.',
    '<div class="quote"><h2>Lovag utca 20. : A bérpalotát Révész Sámuel és Kollár József építészek tervei alapján 1905-1906-ban építtette Muray Róbertné (Fenil Angéla) A műteremmel épült bérpalota érdekessége, hogy több helyen stilizált menórával díszített, a függőfolyosó kovácsoltvas korlátja mellett a külső homlokzaton, az első emeleti ablakok előtti alacsony kovácsoltvas rács közepén is megjelenik, teljes valóságában.</h2></div>',
    '<b>A menekülés története:</b><br />',
    '1920-ra a nagyváradi üzlet csőd közelbe került. A trianoni határok meghúzása után Weinstock Fülöp pedig <b style="cursor: pointer;" onclick="onImage(19)">kérvényezte Budapestre költözését és magyar állampolgárságának megtartását két kiskorú lányával.</b> <br />',
    '<b style="cursor: pointer;" onclick="onImage(20)">A Lovag utca 20. alatti házban vásárolt lakást.</b> <b style="cursor: pointer;" onclick="onImage(21)">(b)</b> <b style="cursor: pointer;" onclick="onImage(22)">(c)</b> <b style="cursor: pointer;" onclick="onImage(23)">(d)</b> <b style="cursor: pointer;" onclick="onImage(24)">Ezekben az években fiával együtt dolgozik különböző budapesti üzletekben</b>',
  ],
  slug: 'weinstock-fulop',
  thumbnail: '/trianon_images/weinstock_fulop/0_THUMBNAIL.jpg',
  images: [{
    src: '/trianon_images/weinstock_fulop/0.jpg',
    description: '0.	Weinstock Fülöp portréja 1905 körül (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/weinstock_fulop/1a.png',
    description: '1.	a. Weinstock Fülöp portréja, 1937. szeptember 25. A fotót az Egyesült Államokból küldte Ernő fiának. (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/weinstock_fulop/1b.jpg',
    description: '1.b. Weinstock Jónás órás üzlete Nagyváradon. 1895 körül. A fotót feltehetőleg Weinstock Ernő készítette. (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/weinstock_fulop/2.jpg',
    description: '2. Német nyelvű eljegyzési kártya, Weinstock Fülöp és Weisz Etelka, 1892. (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/weinstock_fulop/3a.jpg',
    description: '3. a-b. Weinstock Fülöp azonosító jegye (igazolványa), 1897. (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/weinstock_fulop/3b.jpg',
    description: '3. a-b. Weinstock Fülöp azonosító jegye (igazolványa), 1897. (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/weinstock_fulop/4a.jpg',
    description: '4. a. Weinstock Fülöp üzlete Nagyváradon, 1908 körül. A fotót feltehetőleg fia, Weinstock Ernő készítette (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/weinstock_fulop/4b.jpg',
    description: '4. b. Az úgynevezett Bartsch-ház: A nagyváradi Fő utca egyik legszebb palotájaként tartják számon a historizáló palotát, amelyet Bartsch Sándor 1888-ban építtetett Hanzlián János budapesti építésszel. (Forrás: https://www.facebook.com/varadikepek/photos/a.578706605490696.140155.536023809758976/1865839073444103 )',
  },
  {
    src: '/trianon_images/weinstock_fulop/5.jpg',
    description: '5.	Weinstock Fülöpné síremléke Nagyváradon. (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/weinstock_fulop/6.jpg',
    description: '6.	Újságcikk Weinstock Fülöpné haláláról,1898. (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/weinstock_fulop/7a.jpg',
    description: '7.	a-b. Weinstock Fülöp pályaórás igazolványa, 1915. (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/weinstock_fulop/7b.jpg',
    description: '7.	a-b. Weinstock Fülöp pályaórás igazolványa, 1915. (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/weinstock_fulop/8.jpg',
    description: '8.	Weinstock Ernő, repülő százados portréja, 1915. (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/weinstock_fulop/9.jpg',
    description: '9.	Weinstock Fülöp népfölkelési igazoló lapja, 1915 (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/weinstock_fulop/10.jpg',
    description: '10.	Weinstock Fülöp, fia Ernő, Ernő felesége Raffay Anna és unokája Ágota 1930 körül. (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/weinstock_fulop/11.jpg',
    description: '11.	Weinstock Fülöp és unokája Ágota 1930 körül. (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/weinstock_fulop/12.jpg',
    description: '12.	Weinstock Fülöp amerikai útlevél kérelme, 1924. (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/weinstock_fulop/13.jpg',
    description: '13.	A new yorki Kossuth szobor avatóünnepsége, 1928. március 15. Fotó: Jelfy Gyula (Fővárosi Szabó Ervin Könyvtár - Budapest Gyűjtemény)',
  },
  {
    src: '/trianon_images/weinstock_fulop/14.jpg',
    description: '14.	Weinstock Fülöp és két lánya Anna és Magdolna állampolgárság megtartására vonatkozó hatósági papírja, 1923. (Szabadtéri Néprajzi Múzeum)',
  },
  {
    src: '/trianon_images/weinstock_fulop/15a.jpg',
    description: '15.	a-d A lovag utca 20. szám alatti ház, 2020. Fotó: Sári Zsolt (Szabadtéri Néprajzi Múzeum) Lovag utca 20. : A bérpalotát Révész Sámuel és Kollár József építészek tervei alapján 1905-1906-ban építtette Muray Róbertné (Fenil Angéla) A műteremmel épült bérpalota érdekessége, hogy több helyen stilizált menórával díszített, a függőfolyosó kovácsoltvas korlátja mellett a külső homlokzaton, az első emeleti ablakok előtti alacsony kovácsoltvas rács közepén is megjelenik, teljes valóságában.',
  },
  {
    src: '/trianon_images/weinstock_fulop/15b.jpg',
    description: '15.	a-d A lovag utca 20. szám alatti ház, 2020. Fotó: Sári Zsolt (Szabadtéri Néprajzi Múzeum) Lovag utca 20. : A bérpalotát Révész Sámuel és Kollár József építészek tervei alapján 1905-1906-ban építtette Muray Róbertné (Fenil Angéla) A műteremmel épült bérpalota érdekessége, hogy több helyen stilizált menórával díszített, a függőfolyosó kovácsoltvas korlátja mellett a külső homlokzaton, az első emeleti ablakok előtti alacsony kovácsoltvas rács közepén is megjelenik, teljes valóságában.',
  },
  {
    src: '/trianon_images/weinstock_fulop/15c.jpg',
    description: '15.	a-d A lovag utca 20. szám alatti ház, 2020. Fotó: Sári Zsolt (Szabadtéri Néprajzi Múzeum) Lovag utca 20. : A bérpalotát Révész Sámuel és Kollár József építészek tervei alapján 1905-1906-ban építtette Muray Róbertné (Fenil Angéla) A műteremmel épült bérpalota érdekessége, hogy több helyen stilizált menórával díszített, a függőfolyosó kovácsoltvas korlátja mellett a külső homlokzaton, az első emeleti ablakok előtti alacsony kovácsoltvas rács közepén is megjelenik, teljes valóságában.',
  },
  {
    src: '/trianon_images/weinstock_fulop/15d.jpg',
    description: '15.	a-d A lovag utca 20. szám alatti ház, 2020. Fotó: Sári Zsolt (Szabadtéri Néprajzi Múzeum) Lovag utca 20. : A bérpalotát Révész Sámuel és Kollár József építészek tervei alapján 1905-1906-ban építtette Muray Róbertné (Fenil Angéla) A műteremmel épült bérpalota érdekessége, hogy több helyen stilizált menórával díszített, a függőfolyosó kovácsoltvas korlátja mellett a külső homlokzaton, az első emeleti ablakok előtti alacsony kovácsoltvas rács közepén is megjelenik, teljes valóságában.',
  },
  {
    src: '/trianon_images/weinstock_fulop/16.jpg',
    description: '16.	Weinstock Fülöp névjegykártyája (Szabadtéri Néprajzi Múzeum)',
  },
  ],
  lead: '',
},
{
  id: 9,
  name: 'Impresszum',
  title: '',
  content: [
    '<b>Impresszum</b>',
    'Vagonlakók – Trianon árvái<br />'
            + 'Szabadtéri Néprajzi Múzeum<br />'
            + 'A kiállítás kurátorai: dr. Sári Zsolt – Kloska Tamás<br />'
            + 'Látványterv és design: Play Dead Kft.<br />'
            + 'Grafika: Play Dead Kft.<br />'
            + 'Angol fordítás: Andrikó Katalin<br />'
            + 'A kiállításhoz fotókat, képeslapokat, dokumentumokat és filmíradót kölcsönző intézmények és személyek: Fortepan, Fővárosi Szabó Ervin Könyvtár, Magyar Műszaki és Közlekedési Múzeum, Magyar Nemzeti Filmarchívum, Magyar Nemzeti Levéltár, Thury György Múzeum, Zempléni Múzeum, Buzás Miklós, Koticsné dr. Magyari Márta, Riechl Éva, Tolcsvay László<br />'
            + 'Külön köszönet: Buzás Miklós, Koticsné dr. Magyari Márta<br />'
            + 'Támogató: Emberi Erőforrások Minisztériuma<br />'
            + 'A Szabadtéri Néprajzi Múzeum fenntartója: Emberi Erőforrások Minisztériuma<br />',
    '<b>Imprint</b>',
    'Wagon Dwellers – Orphans of Trianon<br />'
            + 'Hungarian Open Air Museum<br />'
            + 'Curators: Zsolt Sari, PhD – Tamás Kloska<br />'
            + 'Design: Play Dead Kft.<br />'
            + 'Graphic Design: Play Dead Kft.<br />'
            + 'Translation: Katalin Andrikó<br />'
            + 'Institutions and Individuals Lending Photos for the Exhibition: The Hungarian Museum of Science, Technology and Transport, Museum of Zemplen, Márta Magyari Koticsné PhD, Miklós Buzás<br />'
            + 'Acknowledgements: Miklós Buzás, Márta Magyari Koticsné PhD<br />'
            + 'Sponsored by: Ministry of Human Capacities<br />'
            + 'The Museum is Maintained by the Ministry of Human Capacities<br />',
  ],
  slug: 'impresszum',
  images: [],
  lead: '',
},
];

export default data;
