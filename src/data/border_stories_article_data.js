const articles = [
  {
    title: 'Önnek is van olyan HATÁRtörténete, amelyet szívesen felajánlana a virtuális közösségi kiállítás számára?',
    lead: 'A Szabadtéri Néprajzi Múzeum a Határtalan Skanzen tematikus év keretén belül a Nemzeti összetartozás éve kapcsán az összmagyarság kulturális összetartozását és az első világháborút lezáró békediktátum hatásait mutatja be a látogatóknak. A tematika fő ívét a „határ” fogalom adja, gondolva ezzel nemcsak a fizikai, hanem a testi, az emberi és az emberéleteket érintő határokra is. A tematika mentén határtörténetek gyűjtésébe kezdtünk, melyeket az alábbi aloldalon találhatja meg az érdeklődő. ',
    content: [
      'Önnek is van olyan HATÁRtörténete, amelyet szívesen felajánlana a virtuális közösségi kiállítás számára?',
      'Vannak olyan tárgyai, fotói, emlékei, amelyek az 1920 és 2020 közötti időszakból származnak?',
      'Ha igen, ossza meg velünk! A történeteket, emlékeket elsősorban digitalizálva várjuk, melyhez segítség is kérhető kollégáinktól: hatartortenetek@skanzen.hu emailcímen.',
      'A beérkezett anyagokat a múzeum kizárólag kutatási, kiállítási, illetve a gyűjtést segítő kommunikációs célokra használja fel. Tárgyfelajánlás esetén a tárgy a múzeum gyűjteményébe kerül. A felhasználás során minden esetben az adatvédelmi szabályoknak megfelelően járunk el. A személyes adatok védelmét lásd a Tájékoztatóban. ',
      '<i>A megjelenő témakörök:</i></br>'
    + 'A kiállításban megjelenő témakörök alapján azonosíthatók be a történetek. A választott témákkal kapcsolatban a személyes történetek mellett a sajtóban a témában megjelent újságcikkek is helyet kapnak. Az öt fő címke, amelyekkel a szövegek el lesznek látva:\n',
      '<ul>'
    + '<li>csempészet</li>'
    + '<li>ideiglenes határátlépés</li>'
    + '<li>végleges határátlépés</li>'
    + '<li>útlevekkel kapcsolatos problémák</li>'
    + '<li>határbetegség jelensége</li>'
    + '</li>'
    + '</ul>',
      'A leíró szöveg alatt mellékelt ábrán látható módon (1. ábra) csempés rendszerben jelennek meg a történetek, amelyekhez egy-egy jellemző kép és egy rövid cím tartozik, melynek meghatározása a muzeológus és a kommunikációs munkatárs közös feladata. A gyűjtés során előfordulhat, hogy a történethez nem tudnak képet társítani, de az elmesélésben egy-egy kiemelkedő/ jellegzetes tárgyról a Skanzen gyűjteményeiből fényképek beszerezhetők (pl. narancssárga Lada kombi, lila mikrobusz stb.) A fent említett hashtageken kívül egyéb címkékkel is ellátható a szöveg, annak beazonosítása érdekében: pl. híres személyek esetén a személy neve is itt jelenik meg.',
    ],
    quote: '',
    image: require('@/assets/images/zarjegy_nelkuli.png'),
    coverImage: require('@/assets/images/zarjegy_nelkuli.png'),
    slug: 'felhivas',
    tags: [],
    maps: [
      'Frankfurt',
      'Budapest',
      'Debrecen',
    ],
    notShown: true,
  },
  {
    title: 'Útlevél két szemszínnel',
    lead: 'Útlevél két szemszínnel',
    content: ['Tudni kell rólam, hogy a szemeim különböző színűek, az egyik barna a másik zöldes-szürkés-kékes. 1988 óta van útlevelem,'
            + 'és akkoriban az útlevélben is megjelenítették a szem színét. Természetesen a kérőben meg is kellett jelölni. Egyet. '
            + 'Ennek ellenére mi beírtuk a zöldet és a barnát is. Izgatottan vártuk, hogy megjöjjön az útlevél, mi is lesz benne. Beleírták.'
            + ' Mindkettőt. Aztán eljött a nyár, amikor egyik ismerősünk elvitt minket családostul Bécsbe, ahova akkor már lehetett menni vásárolgatni...'
            + 'Amikor mentünk kifelé a határon, útlevélellenőrzés történt. Megnézték az ismerősét, megköszönték, visszaadták. '
            + 'Megnézték édesapámét, megköszönték, visszaadták. Majd édesanyámét, testvéremét, végül maradt az enyém.'
            + ' Megnézték, majd megnézték még egyszer, meg még egyszer, csodálkozón vetették össze a valósággal, vigyorogtak egyet, és visszaadták, jó utat kívántak és elbocsátottak.'
            + 'Szerintem még nem találkoztak különböző színű szeművel. Vagy olyannal, aki beírta.',
    '(S. P. közlése, a beküldő helyesírását követve).',
    'A borítóképen: Női profil (a kép csupán illusztráció)',
    'A kép forrása: Fortepan',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/szigorú_ell.jpg'),
    coverImage: require('@/assets/images/border_coverImages/A_CSEMPESZET_SZIGORUBB_COVER_KEP.jpg'),
    slug: 'utlevel-kek-szemszinnel',
    tags: [
      '#útlevél',
      '#ideiglenes határátlépés',
    ],
    maps: [],
  },
  {
    title: 'A képviselőház ülése',
    lead: 'Halottak a határon',
    content: ['Nemzeti Ujság',
      '1927. május (9. évfolyam, 98-122. szám)',
      '1927-05-06 / 102. szám',
      '— Sok visszásság van a csempészet körül. Ausztriában a tőlünk exportált cukrot fele áron adják, mint nálunk.'
            + 'Aki a határmenti községekből a háztartása részére áthoz egy kis cukrot, azokat sorra agyonlövik a vámőrök.'
            + 'Mióta képviselő vagyok nyolc ilyen eset történt a kerületemben. Nemrégiben egy tizennégyéves leányt lőttek agyon a vámőrök öt kilo cukorért és néhány tojásért.'
            + '  Néhány héttel ezelőtt egy hadiözvegy egyetlen fiát lőtték agyon. '
            + 'Milliókat költünk gyermekmenhelyekre és lelencházakra, ugyanakkor a vámőrök puskái könnyelműen kioltják az emberek életét.'
            + 'Az ilyen esetek természetesen súlyos következményekkel járnak. Alig lehet megnyugtatni az embereiket. A konszolidációnak ez a gyakorlat semmi esetre sem válik hasznára.'
            + '  Sokszor éppen a vámőrök ugrasztják be a csempészetbe az embereket, mert az elkobzott áruból jutalékot kapnak',
      'Búd János pénzügyminiszter: Ne tessék általánosítani. Tessék megnevezni az illetőket. ',
      'Lingauer Albin: Ez nem általánosítás. Bíróság előtt tett vallomásokból derült ki. Az aktákat készséggel rendelkezésére bocsátom, a miniszter urnák. A költségvetést elfogadom.',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/képviselő ülés.jpg'),
    coverImage: require('@/assets/images/border_coverImages/A_KEPVISELOHAZ_ULESE_COVER_KEP.jpg'),
    slug: 'a-kepviselohaz-ulese',
    tags: [
      '#csempészet',
      '#sajtóanyag',
    ],
    maps: [],
  },
  {
    title: 'A mi határtörténetünk',
    lead: 'A Gulyás család határtörténete Trianon után.',
    content: [
      'Édesapám, Gulyás Bálint, 1910. április10.-én született Kisperegen, Arad megyében, Aradtól harminc kilométerre, a mostani határ közelében. A következő évben született István, majd József nevű öccse, mindhárman Kisperegen.',
      'Édesapjuk, az idősebb Gulyás Bálint, --az én nagyapám-- ezredtrombitásként szolgált az akkori magyar hadseregben, így sokat volt távol a családjától. ',
      'A románok először 1916-ban törtek be Erdélybe, addigi szövetségeseiket hátba támadva és elárulva, --miután megállapodást kötöttek az antant hatalmakkal annak fejében, hogy ha belépnek az I. világháborúba a központi hatalmak ellen, akkor megkapják Erélyt és a Partiumot. De ekkor még a központi hatalmak közös erővel egészen Bukarestig verték vissza az akkori román hadsereget. Románia ezt követően 1918 májusában - az 1916-os antanttal kötött megállapodást is megszegve-- különbékét kötött a központi hatalmakkal.  1918 őszén-az I. világháború végén ismét akkori szövetségesüket elárulva- a románok másodszor is betörtek Erdélybe-miután a Károlyi-kormány Magyarországon szétzüllesztette a magyar hadsereget, és Magyarország védtelenül maradt. Az 1920-as Trianoni diktátum végérvényesen Romániának ítélte az akkori Magyarország jelentős területeit, köztük Arad megyét is.',
      'Családunk kettészakadt: a szülők a kis-Magyarország területén, három fiúgyermekük pedig Kisperegen, a nagymama felügyelete alatt. Nagymamám, a fiúk édesanyja valószínűleg azért tartózkodott a mai Magyarország területén, hogy helyet keressen a letelepedéshez férje és fiai számára.',
      'Komoly szervezést igényelt akkoriban, hogy a gyermekek vajon hogy kerüljenek át a határon szüleikhez?<br />'
            + 'A dédanyám és családja kisbirtokosok voltak, földterületekkel rendelkeztek a falu határában. Dédanyám sűrűn járt ki a határba, ilyenkor mindig magával vitte a három unokáját is. A román határőrök lassan-lassan hozzászoktak, hogy a határ menti földeken meg-meg jelent egy nagymama, három fiúgyermekkel. Később már ügyet sem vetettek rájuk. <br />'
            + 'Egy ilyen alkalommal –természetesen a szülőkkel egyeztetett napon— azt mondta nagymamájuk a fiúknak: most fussatok! És a három fiú baj nélkül átért a határ túloldalára, ahol édesanyjuk várt rájuk.',
      'A Gulyás család Szegeden telepedett le, a Veresács utcában.',
      'Édesapjuk, az idősebb Gulyás Bálint, volt ezredtrombitás 1942-ben halt meg. Kerékpározás közben meghűlt, tüdőgyulladás okozta halálát. <br />'
            + 'Édesanyjuk, Baranyi Julianna 1982-ben, kilencvenkét éves korában adta vissza lelkét Teremtőjének úgy, hogy öt unokája és hat dédunokája még ismerhette csöndes, áldozatos szeretetét.',
      'Mára már mindhárom fia is követte őt. <br />'
            + '(D.G.Z. közlése alapján, a beküldő helyesírását követve)',
      '<i>A borítóképen: Gulyás Bálint és felesége</i>',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/Gulyás nagyszüleim 2.jpg'),
    coverImage: require('@/assets/images/border_coverImages/A_MI_HATARTORTENETUNK_COVER_KEP.jpg'),
    slug: 'a-mi-hatartortenetunk',
    tags: [
      '#végleges határátlépés',
    ],
    maps: [],
  },
  {
    title: 'A múltnak kútja',
    lead: 'Széll Tamás: Tallózás Bucsu múltjában',
    content: [
      'Vasi Szemle - Dunántúli Szemle Vasi Szemle <br />'
            + '2002 (56. évfolyam, 1-6. szám) <br />'
            + '2002 / 6. szám <br />',
      'A trianoni határ helyenként alig több mint fél kilométerre húzódik a falutól.'
            + '<b> Új jövedelemszerzési lehetőségként a - főleg tűzkőre, szacharinra, cukorra stb. korlátozódó - csempészet adódott, de a náci időkben sokan dolgozni is átjártak a jobb kereseti lehetőséget biztosító német birodalomba. A békediktátum után sok búcsúi, rohonci és csajtai ún. kettős birtokos volt, akik külön engedéllyel folyamatosan tovább művelhették tulajdonukat, de a második világháborút követő pártállam ezeket felszámolta, s a határt vasfüggönnyel hermetikusan lezárta. </b>'
            + '(A trianoni békediktátum után határmenti községgé vált Búcsút a kommunista diktatúra szinte kiközösítette az országból, hiszen a műszaki zár a falu szélső háza mellett húzódott, s mindenki gyanús volt, aki Búcsúba látogatott.) Amikor a közlekedés javulása folytán másutt a távoli vidékek is közelebb kerültek egymáshoz, itt természetellenesen, nemcsak a szomszéd ausztriai helységektől, de az ország belső részeitől is elzárták az ország peremébe ékelt bucsuiakat. A határt szigorúan őrizték, aknákat telepítettek, mely az országból menekülő számos állampolgár, sőt néhány szolgálatot teljesítő határőr csonkolással járó balesetét, vagy akár életét is követelte. Az 1956-os forradalom után már felszedték az aknákat, majd kettős, elektromos jelzőkkel ellátott műszaki zárat telepítettek. A falut a határ felé elhagyni csak mezőgazdasági célból, külön engedéllyel meghatározott kapukon, és csak nappal, megszabott időben lehetett, a falu lakosait pedig a „határőr község” címre hivatkozva besúgásra, a politikai üldözött menekültek feladására, elfogásuk elősegítésére próbálta ösztönözni a hatalom. Mindez fájdalmas évtizedeket okozott ugyan, másfelől azonban hozzájárult ahhoz, hogy Búcsú megújulva is sok mindent megőrzött régi bájából.',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/vasi szemle.jpg'),
    coverImage: require('@/assets/images/border_coverImages/A_MULTNAK_KUTJA_COVER_KEP.jpg'),
    slug: 'a-multnak-kutja',
    tags: [
      '#csempészet',
      '#sajtóanyag',
    ],
    maps: [],
  },
  {
    title: 'A pirosat vagy a kéket?',
    lead: '„Ez az útlevél a világ összes országába érvényes…”',
    content: [
      'Ekkor már – ti. 1972 után (szerk.) – két útlevéllel rendelkeztünk, egy pirossal és egy kékkel. A pirossal utazhattunk a szocialistának nevezett országokba elvben évente ötször, és ezt a pirosat otthon is tarthattuk, míg a kékkel háromévenként mehettünk nyugatra. Minden egyes út előtt kérelmet és sokféle papírt kellett benyújtani a hatóságokhoz. Rendszerint interjúkra is behívódtunk. Akkor jártam először a Ritz-Carltonban. Az épület korábban az Adria Biztosítótársaságé volt, míg ma Ritz-Carlton lett. A két időszak között pedig a BM Főkapitányságaként funkcionált, ahová idönként beidéződtünk egy-egy beszélgetésre. Máig emlékszem arra a göndör vöröshajú szürke öltönyös fickóra, akit ilyenkor meg kellett győznöm arról, hogy én biz’ ha törik, ha szakad, hazatérek a rothadó nyugatról. ',
      'Később már nem voltam meginvitálva szívélyes társalgásokra, mert változott a világ. Eleinte még a kék útlevelet hazatérés után be kellett szolgáltatni a BM-nak, de az idő múlásával már arra sem volt szükség. Volt azonban ebben a kék útlevélben egy provokáló mondat: „Ez az útlevél a világ összes országába érvényes.” Ez mindig zavart, hisz általában csak egy-két ország meglátogatására kaptunk engedélyt – vagy nem. Így a világ minden országa egy nagyképű és idegesítő duma volt.<br />'
            + '(N. Z. közlése, a beküldő helyesírását követve)',
      'A borítóképen: 1972-ben bevezetett piros útlevél borítója<br />'
            + 'A kép forrása: dr. Bencsik Péter magángyűjteménye',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/útlevél.jpg'),
    coverImage: require('@/assets/images/border_coverImages/A_PIROSAT_VAGY_A_KEKET_COVER_KEP.jpg'),
    slug: 'a-pirosat-vagy-a-keket',
    tags: [
      '#útlevél',
      '#ideiglenes határátlépés',
    ],
    maps: [],
  },
  {
    title: 'A Wartburg fenekén',
    lead: 'Hova rejtették a csempészárut a Román Szocialista Köztársaságból érkezők?',
    content: [
      'Nagyváradon, az ismerősöknél volt letéve ez a tálasfogas, mert korábban nem mertük átmenni vele határon. Aztán egyszer mégis erőt vettünk magunkon, és eldöntöttük, hogy áthozzuk. Az ismerőseink a stadion környékén laktak. Nem tudom milyen nap volt, de nem volt aznap meccs, így elég kihalt rész volt. A ház előtt megállt a férjem a Wartburggal és valahogy arra akarta felszerelni. Alábújt a kocsi alá, én pedig odanyújtottam neki a tálast. Vittünk drótokat meg azbeszt szövetet, hogy nehogy hozzá érjen a kipufogóhoz. A férjem ott szerelgette, eleredt ugyan az eső, de azért úgy csinálgatta. Egyszer megállt egy autó és kérdezte magyarul:<br />'
            + '<i>-	Segíthetek?</i><br />'
            + 'Mondtuk, hogy nem, köszönjük szépen, de nem. Alig telt el 10 perc, megint megállt egy autó: <br />'
            + '<i>-	Segíthetek?</i><br />'
            + 'A férjem már kicsit durvábban mondta, hogy nem, nem, nem kell segítség. De mikor már az ötödik állt meg, akkor azt mondja a férjem: <br />'
            + '<i>-	Menjen a francba! Hagyjon békén!</i><br />'
            + 'Nagyon felhúzta magát. De aztán alászerelte a tálast. Elindultunk, estére értünk a határhoz. Ott beálltunk a tető alá a vámnál. Ahogy beálltunk, a mögöttünk lévő kocsi, –ami kicsit távolabb is állt tőlünk – felkapcsolta a lámpáját. Kiszálltam és megkértem, hogy <br />'
            + '<i>-	Uram, minek a lámpa? Hát itt ki van világítva minden…</i><br />'
            + 'De csak nem akarta eloltani. Mi meg néztük, hogy „te jó isten, csak egy kicsit a kocsi fenekére néz a vámos…” Mert volt olyan, amikor a motorháztetőt is kinyitották. Ahogy ment az autó, a drót hozzá ért valamihez és csikorgott. Megálltunk és elkezdtem nagyon köhögni, mint aki köhögőgörcsöt kap, hogy nehogy a határőr meghallja a csikorgást. Így aztán el tudtunk menni. De még előtte az egyik határőrnek kioldódott a cipőfűzője az autónktól 5 méterrel távolabb. Lehajolt, hogy bekösse mi pedig mondtuk magunkban, hogy „te jó isten, ha ez itt a mi kocsink mellett van…” De áthoztuk.<br />'
            + '(R.Á. interjúja alapján készült szerkesztett változat)',
      'A borítóképen: a történetben szereplő tálas-fogas<br />'
            + 'A kép forrása: Kmellár Viktória felvétele (2020)',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/tálas.jpeg'),
    coverImage: require('@/assets/images/border_coverImages/A_WARTBURG_FENEKEN_COVER_KEP.jpg'),
    slug: 'a-wartburg-feneken',
    tags: [
      '#csempészet',
      '#ideiglenes határátlépés',
      '#határfrász',
    ],
    maps: [],
  },
  {
    title: 'Amikor megváltoztak a határok',
    lead: 'Hogyan nézett ki az ún. kishatárátkelő útlevél?',
    content: [
      'A dédnagypapám 1902-ben született az akkor még Osztràk-Magyar Monarchia (azonbelül Magyar Királyság) területén.',
      'Véget ért az első világháború, megköttettek a békeszerződések is. Dédnagypapám és családja: szülei, testvérei Csehszlovákiába kerültek, kedvese (dédnagymamám) viszont a Magyar Kiràlyság területén élt. Jó, hát itt kb. 13 km távolságról beszélhetünk a két település között és valahol félúton húzták meg az államhatárt. Ennek következtében mivel kb. földterületeit vágta ketté az államhatár így dédnagyapám nehéz helyzetbe került. Ennek nyomán kapta az alábbi útlevelet, ami biztosította a két ország közötti utazás lehetőségét. Persze, ez a helyzet hosszú ideig nem működhetett, így el kellett döntenie, h melyik ország lesz az otthona. Ezt az okiratot 1925-ben adták ki Prágában, “csehszlovák” és francia nyelven, de néhol magyar bejegyezés is van benne. és biztosította az átjárást Csehszlovákia és a Magyar Királyság között.<br />'
            + '(K. A. közlése, a beküldő helyesírását követve)',
      'A borítóképen: Lucskay István útleve<br />'
            + 'A kép forrása: a beküldő által közölt felvétel',
    ],
    quote: '',
    image: require('@/assets/images/border_coverImages/AMIKOR_MEGVALTOZTAK_A_HATAROK_COVER_KEP.jpg'),
    coverImage: require('@/assets/images/border_coverImages/AMIKOR_MEGVALTOZTAK_A_HATAROK_COVER_KEP.jpg'),
    images: [
      {
        src: require('@/assets/images/border_stories/útlevél1.jpg'),
        thumbnailWidth: '220px',
      },
      {
        src: require('@/assets/images/border_stories/útlevél2.jpg'),
        thumbnailWidth: '220px',
      },
      {
        src: require('@/assets/images/border_stories/útlevél3.jpg'),
        thumbnailWidth: '220px',
      },
      {
        src: require('@/assets/images/border_stories/útlevél4.jpg'),
        thumbnailWidth: '220px',
      },
      {
        src: require('@/assets/images/border_stories/útlevél5.jpg'),
        thumbnailWidth: '220px',
      },
    ],
    slug: 'amikor-megvaltoztak-a-hatarok',
    tags: [
      '#útlevél',
      '#ideiglenes határátlépés',
    ],
    maps: [],
  },
  {
    title: 'Antibébi cserekereskedelem',
    lead: 'Hogyan jutott el az antibébi tabletta Romániába a ’70-es évek közepén?',
    content: [
      'Utazás az első feleséggel sötétkék 500-as Fiattal Romániába. Az út talán 1975-ben lehetett. Akkoriban az 500-as Fiat időnként eltűnt egy-egy szakadékban, amely mindössze az úton előre nem jelzett nagyjából félméteres lyukakat jelentette.',
      'Az országutakon az első szlogen, amit könnyedén megtanultunk az a következő felirat volt: Trăiască Partidul Comunist Român! (Van olyan ember, aki nem ismerné ennek a jelentését? De ne kelljen szégyenkezni, leírom: Éljen a Román Kommunista Párt!) […]',
      'Egy forgalomelterelésnél, ahol a főutat elterelték egy falusi földútra, meglehetősen le kellett lassítani, mert az előttünk legutoljára esett zivatar után erre járt traktor roppant mély árkokkal szántotta tele az „országutat”. A nagyjából fél km/h sebességgel egyik buckáról a másikra ugráló aprócska Fiatot a gyalogosok simán lehagyták, […]',
      'Az autó egyszer csak leállt és nem volt hajlandó továbbmenni. […]',
      'Egyszer megállt mellettünk egy autó. Véletlenül egy Dacia volt! A vezető felmérte a helyzetet, és felajánlotta, hogy küld hozzánk segítséget, majd elhúzott a legközelebbi város irányába.<br />'
            + 'Úgy egy óra múlva megjelent egy másik autó immár a város felől, és lássatok csudát, az is Dacia volt! Kiszállt belőle egy magyarul beszélő szerelőruhás ember, beleugrott a motorba, majd úgy 20-25 perc múlva kért, hogy indítsak. És a motor indult! Természetesen a nagy boldogságban az ember bármennyit hajlandó lett volna átadni az embernek, sőt annál akár jóval KEVESEBBET IS, de ő nem fogadott el pénzt. Elmondta, hogy ha valóban szeretnénk meghálálni a segítséget, akkor ha legközelebb arra járunk, hozzunk neki antibébi tablettákat, azaz fogamzásgátlót, hisz ő még ereje teljében lévő fiatalember. […] Természetesen megígértük a segítséget.',
      'Pár hét múlva, hogy elősegítsük az ember nemzetiségi illetve nemzési vágyainak veszélytelen kielégítését, nekiindultunk a határnak újra. Ezúttal számos doboznyi fogamzásgátlót rejtve el a hatalmasnak nem mondható 500-as Fiatban.',
      'Feleségem nem látta hová rejtettem a dobozkákat, csak annyit mondtam neki, hogy hátul vannak, biztonságban, a hátsó ülés alatt.',
      'A határra érve simán átjutottunk a magyar oldalon, de a román ellenőrző ponton feltűnt egy román ellenőrző-nő és ellenőrizhetnékje támadt. Nem túl barátságosan, de megkért, hogy mutassam meg a csomagtartót. Ez az autó elején egy sporttáskának sem elegendő méretű terület volt. Megmutattam. Ezek után benézett a járműbe és kérte, mutassam meg, mi van az ülés mögött. Feleségem kissé szótlanul és egyre szótlanabbul készült a letartóztatásunkra. A hátsó ülés háttámláját leengedtem és megmutattam a román ellenőrző nőnek. Ő köszönte, és megkért, emeljem fel magát az ülést is. Ekkor már nem csak a feleségem volt bizonyos abban, hogy az éjszakát egy meglepetésként szolgáló helyen töltjük majd, hanem magam is. De mit tehettem volna, emeltem. Úgy 10 centire. A szakember ellenőrző-nő ekkor kitépte a kezemből az ülés lapját, és magasabbra emelve pillantott bele. – Amikor bajban vagyok, már megszoktam, hogy teljesen flegmának látszom, és ezúttal is ez történt. Nem látszott rajtam aggódás, izgalom amikor a nő behajolt és leskelődött, mi rejtőzhetett az ülés alatt. HA CSAK 5 CENTIVEL EMELTE VOLNA FÖLJEBB, meglátta volna az ülés rugói és a műbőr borítás közé benyomkodott dobozkákat, de nem tette. Nem túl barátságosan, de megköszönte a bemutatót és intett, hogy mehetünk tovább…. És mi mentünk. Nem túlságosan begyorsulva, hisz azért ne legyünk túl gyanúsak, inkább még jobban lelassulva, majd elhagytuk a határétkelőt és nekivágtunk az autószerelőnek átadni a tablettákat.<br />'
            + '(N. Z. közlése, a beküldő helyesírását követve)',
      'A borítóképen: Fiat 500-as modell<br />'
            + 'A kép forrása: Fortepan',

    ],
    quote: '',
    image: require('@/assets/images/border_stories/Fiat_500_Fortepan.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Antibebi_cserekereskedelem_COVER_KEP.jpg'),
    slug: 'anitbebibol-cserekereskedelem',
    tags: [
      '#csempészet',
      ' #ideiglenes határátlépés',
    ],
    maps: [],
  },
  {
    title: '— Az „ezerédes"',
    lead: 'Így nevezi a nincstelen falusi a szacharint, amelyet cukor helyett használ, ha van mihez használni.',
    content: [
      'Népszava<br />'
            + '1933. szeptember (61. évfolyam, 199–223. sz.)<br />'
            + '1933-09-28 / 221. szám',
      '— Az „ezerédes". így nevezi a nincstelen falusi a szacharint, amelyet cukor helyett használ, ha van mihez használni. Mert cukorhoz évszámra nem jut, nem engedi hozzá a kartell, amely inkább beszántatja az eladhatatlan mennyiséget, csakhogy ne kelljen csökkenteni az árakat. A nincstelen városi és falusi proletár pedig a tápláló cukor helyett szacharinra kényszerül. A cukorkapitalizmus felelős tehát, hogy a szacharincsempészet olyan nagyarányúra nőtt az utóbbi években. Szerdán ismét száz kilogramm csempészet szacharint találtak a vámtisztviselők a Nyugati pályaudvaron. A küldemény címzettjét, Grünberger Zsigmond szállítót előállították. Kihallgatása során elmondta, hogy a küldemény átvételével egy Spitzer nevű ember bízta meg, akinek sem keresztnevét, sem lakáscímét nem tudja. Spitzer gyakran kapott külföldről árut. amelyet különféle vidéki városokba irányított. A nyomozás megállapította, hogy Spitzert tulajdonképpen Grosznak hívják. Két szacharincsempészt sikerült előállítani. A nyomozás folyik.',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/ézredes.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Az_ezeredes_COVER_KEP.jpg'),
    slug: 'az-ezeredes',
    tags: [
      '#csempészet',
      '#sajtóanyag',
    ],
    maps: [],
  },
  {
    title: 'Célállomás Isztambul',
    lead: 'Kerülőúttal Törökországba a ’70-es években…',
    content: [
      '1974-ben, amikor először nyílt lehetősége egy távolabbi utazásra, egy huszonegy éves srác kitalálta, hogy elmegy és megnézi Törökországot. Próbálta rávenni a barátait is, hogy tartsanak vele, de számukra elsősorban Párizs volt vonzó, így a srác eldöntötte, hogy egyedül megy. A (jóságos) BM-től meg is kapta az engedélyt az utazásra, de az apróbetűs részben (ami valójában nem volt apróbetűs, sőt, ki is volt emelve a levélben) ott szerepelt az a kitétel, amely rendesen elszomorított: „Az utazás kizárólag repülővel, azon belől MALÉV géppel történhet!” Honnan lett volna nekem akkor repülőre pénzem?! A szocialista és a kapitalista országok járatai között hatalmas eltérés volt a jegyárakban a rubel és a dollár elszámolású jegyek közötti különbség folytán. Nem akartam feladni, és az akkor a Dorottya utcában leledző MALÉV irodában adtam elő a gondomat egy nagyon kedves hölgynek. Ő nyilván megszánhatott, mert felfedte a megoldást: Vegyek egy jegyet Budapest-Bukarest-Budapest útvonalra, és egy másikat a Bukarest-Isztambul-Bukarest közötti járatra. Meg voltam mentve! Ezt már ki tudtam fizetni. A bukaresti járatot rubelben számolták el, így az ára csak töredéke volt a dollár elszámolású isztambuli jegynek. Az apró nehézség mindössze annyi volt, hogy el kellett repülnöm Bukarestbe, ahol megvártam azt a gépet, amely 4 órával később szintén Budapestről érkezett és tovább repült Isztambulba, de akkor már velem együtt.<br />'
            + 'És lőn!<br />'
            + 'Visszafelé már egyszerűbb volt a történet. Be kellett mennem a THY (Török Légitársaság) isztambuli irodájába, előadni, hogy hirtelen megváltozott a programom, ezért kénytelen vagyok egyazon járattal hazarepülni, azaz nem kellett külön bukaresti géppel előre menni egy megállót, hanem rögtön fölszállhattam a budapesti gépre. Kis csodálkozásukat és gyanújukat némi humorral, ám megszeppent arccal oszlattam el, de elfogadták.<br />'
            + '(N. Z. közlése, a beküldő helyesírását követve)',
      'A borítóképen: MALÉV repülőgép<br />'
            + 'A kép forrása: Fortepan',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/Malév_1974_Fortepan.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Celallomas_Isztambul_COVER_KEP.jpg'),
    slug: 'celallomas-isztambul',
    tags: [
      '#ideiglenes határátlépés',
    ],
    maps: [],
  },
  {
    title: 'Csempészet ',
    lead: 'A rédicsi vámhivatal csempészet miatt indított eljárást…',
    content: [
      'Zalai Hírlap<br />'
            + '2002. február (58. évfolyam, 27-50. szám)<br />'
            + '2002-02-19 / 42. szám',
      'Rédics (a. g.) — A rédicsi vámhivatal csempészet miatt indított eljárást egy belépő külföldi személy ellen, mivel tehergépkocsija vezetőfülkéjében a pénzügyőr egy be nem jelentett Canon videokamerát talált. Az 150 ezer forintot érő készüléket lefoglalták.',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/csemp_rédics.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Csempeszet_COVER_KEP.jpg'),
    slug: 'csempeszet',
    tags: [
      '#csempészet',
      '#sajtóanyag',
    ],
    maps: [],
  },
  {
    title: 'Csempészsegédek',
    lead: 'Vasúti határátlépés Lengyelország felől a rendszerváltás előtt.',
    content: [
      'Négy tizenötéves fiatalember utazik vonattal Lengyelországba. Egyikük magam valék. Szlovákián áthaladva közeledünk a lengyel határhoz. Csak mi négyen voltunk a fülkében, és nem is igen szerettük volna, ha bárki befurakodjék közénk. Egyszer egy vidám, joviális fickó tűnt fel a folyosón, és benézett hozzánk. Mivel lehetne biztosítani a magunk csöndes (?) intimitását? OSIBISA! Abban az időben árultak néhány rocklemez indiai kiadását a magyarországi hanglemezboltokban. Közöttük egy Osibisa elnevezésű afrikai fiúkból álló együttesét. Nagyon szerettük. Afrikai ritmusok, dobok, zörgés, nagyon szeretnivaló friss zene  volt számunkra, tizenéves kölykök számára. Amikor megláttuk a joviális fickót megint benézni a kabinunkba, azonnal rákezdtünk a kezünkben lévő bármilyen hangadásra képes tárgyakat ritmikusan ütlegelni, és a zenéből – ami már a könyökünkön jött ki – az énekelemeit is harsogni. A fickó kintről bámult befelé, mosolygott, majd elhúzta a tolóajtót és bejött. Még egy darabig „zenéltünk”, de mivel a várt eredmény elmaradt, eluntuk és abbahagytuk.',
      'Amikor csönd lett, a folyamatosan mosolygó úr beszélni kezdett. Kérdezősködni próbált – akkor már valamennyire ment a lengyel, értettük és meg tudtuk értetni magunkat is. Megkért bennünket, hogy vigyük át a határon néhány csomagját azzal, hogy a mi tulajdonunk, mivel neki a megengedett limitnek a sokszorosa van a kabinjába. Valóban, a kabin majdnem tele volt. A „zenészek” persze miért ne segítenének egy szerencsétlen csempésznek? Mindenki átvett 2-2 csomagot. Egy-egy kisebbet és egy-egy nagyobbat. A mai napig nem tudjuk, mi volt bennük… A lengyel vámos jött, ránk nézett, majd továbbment. Ezek nem olyan csempész típusok, gondolhatta, de a barátunkra rászállt. Mivel azonban ő teljesen átkutatva is kizárólag annyi csempészárut vitt magával, amennyi nem lépte túl a limitet, kénytelen volt útjára engedni. A határ lengyel oldalán a vámosok leszálltak, mi pedig tovább utaztunk észak felé barátunkkal együtt. Ő a következő állomáson szállt le, és nem kis nehézséggel tudtuk kiadni az akkor már legális csomagjait az ablakon. Ez a határátlépés azonban nem számított a legkeményebb esetnek a történelemben.<br />'
            + '(N. Z. közlése, a beküldő helyesírását követve)',
      'A borítóképen: az OSIBISA Woyaya albuma<br />'
            + 'A kép forrása:<a href="https://www.discogs.com/Osibisa-Woyaya/master/87900"> https://www.discogs.com/Osibisa-Woyaya/master/87900</a>',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/album_borító.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Csempeszsegedek_COVER_KEP.jpg'),
    slug: 'csempeszsegedek',
    tags: [
      '#csempészet',
      '#ideiglenes határátlépés',
    ],
    maps: [],
  },
  {
    title: 'Cukorcsempészet miatt eljárás indult a kőszegi „Hangya“ ellen.',
    lead: 'Szombathelyről jelenti az Esti Kurir tudósítója, hagy Kőszegen nagyobb arányú csempészetet fedeztek föl.',
    content: [
      'Esti Kurir<br />'
            + '1925. március (3. évfolyam, 49-73. szám)<br />'
            + '1925-03-21 / 66. szám',
      'A csempészetben való részesség vádja miatt eljárást is indítottak a kőszegi „Hangya Szövetkezet” igazgatója, Loparits József ellen, aki egyben városi tanító is. A megállapodások szerint 20 métermázsa olyan cukor került a Hangya üzletébe, amelyet egy horvátzsidányi lakos és egy kőszegi ember csempésztek be Ausztriából. Amikor a nyomozás megindult, az érdekeltek kísérletet tettek, hogy úgy tüntessék föl a cukrot, mintha Kőszegen vásárolták volna, de egy kőszegi kereskedő sem tette meg azt a „kis szívességet”, hogy fiktív számlákat adott volna a csempészeknek. Kőszegen az eset nagy feltűnést kelt, mert Loparits, a Hangya igazgatója, egyúttal városi tanító is és a ker. szoc. munkásegylet egyik vezetője, akiről senkisem gondolta volna, hogy csempészműveletekbe is keveredhetik.',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/cukorcsemp.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Cukorcsempeszet_miatt_COVER_KEP.jpg'),
    slug: 'cukorcsempeszet-miatt-eljaras-indult-a-koszegi-"hangya"-ellen',
    tags: [
      '#csempészet',
      '#sajtóanyag',
    ],
    maps: [],
  },
  {
    title: 'Elfelejtett készpénz ',
    lead: 'Kulisszatitkok egy néprajzos kutatásból…',
    content: [
      'Néprajzos egyetemista koromban nagyon sokat jártunk gyűjteni a barátnőmmel Kárpátaljára. Az évek alatt kialakult egy jól bevált módszerünk, hogyan megyünk át az ukrán-magyar határon. Az esetek nagy többségében vonattal elmentünk Mátészalkáig, onnan pedig volt egy közvetlen buszjárattal a tiszabecsi határig. Itt már csak át kellett gyalogolnunk, hogy a határ túloldalán lestoppoljunk egy autót, ami elvisz minket az úticélunkig. Ez nagyon jól működött, általában gyorsan és baj nélkül átjutottunk a határon.',
      'Egyik nyáron azonban úgy döntöttünk, hogy a sok gyűjtés és kutatás után szeretnénk megnézni Kárpátalja kulturális és természeti kincseit, és a szokásos nyári ott tartózkodásunk alatt kirándulni fogunk. Ehhez azonban kocsira is szükségünk volt, ezért kivételesen autóval indultunk útnak, így a határnak is. A határon nem volt nagy sor, így gyorsan haladtunk. A meglepetés aztán az ukránoknál ért minket, amikor az egyik határőr kiszúrta a magyar rendszámú kocsinkat, odajött hozzánk, és elkezdett kutakodni. Majd a kezünkbe nyomott egy papírt, hogy töltsük ki. Persze nem teljesen értettük, mit akar, hova mit kellene pontosan írni, de megpróbáltuk. Miután visszaadtuk, derült ki, naná, hogy nem jól töltöttük ki, és a nálunk lévő pénzt nem írtuk be. Több se kellett a határőrnek, elkezdett kutakodni a táskámban, majd diadalittasan elszedte a pénztárcámat, benne a „letagadott” pénzzel. Rögtön félreállított minket, ami utána még órákat álltunk a határon. Rögtön értesítettük a barátnőnket, aki azonnal a segítségünkre sietett, így legalább volt tolmácsunk. Nagy nehezen elengedett bennünket, de közölte, hogy a bíróságra kell mennem, ahol majd eldöntik, mi legyen velem és a pénzzel. Úgyhogy egy hét múlva meg kellett jelennem Nagyszőlősön a bíróságon. Szerencsére a bíró nem csinált nagy ügyet belőle, sőt, azt mondta, a pénzt is visszakaphatom néhány nap múlva. A baj csak az volt, akkor már nem voltunk Kárpátalján.',
      'Hazaérve sokat tanakodtunk, visszamenjünk-e pénzért, de végül úgy döntöttünk, hogy igen. Ekkor kezdődtek az igazi kalandok. Vasárnap reggel, rutinosan, elindultunk a barátnőmmel, meg sem nézve a menetrendet, mondván, mi azt úgyis tudjuk. Mátészalkán szembesültünk a ténnyel, hogy a busz, amivel mindig a határhoz megyünk, iskolaidőn kívül nem jár. Ja, és amúgy nem megy semmi Tiszabecsre. Persze, jó néprajzoshoz illően, nem estünk nagyon kétségbe, stoppoltunk. Szerencsénk volt, el is vittek minket a határig.',
      'Másnap már kora reggel megjelentünk a nagyszőlősi rendőrségen, hogy visszakapjuk a pénzt. Ott sikerült egy ismerős rendőrrel beszélnünk, aki nagyon kedves és segítőkész volt, de közölték, hogy most nincs készpénz a pénztárban, el kell menniük a bankba, és majd csak utána kapjuk vissza. Hogy ez mikor lesz, ők se tudták. Természetesen az egész napot ott töltöttük, azt hittük, már sose lesz vége. De, győzelem, végre délután 4-kor visszakaptuk a pénzt! Rendben, ez jó hír, de mit csináljunk, itt vagyunk Nagyszőlősön, és valahogy haza kellene jutni Budapestre. Elindultunk, lesz, ami lesz. Valahogy elvergődtünk a határig, onnan stoppal Mátészalkára. Már útközben kiderült, hogy Mátészalkán még fel tudunk szállni egy vonatra, ami elvisz minket Debrecenbe, de onnan aznap már nem megy vonat Budapestre. Mit csinál egy jó feleség, riasztja a férjét, hogy legyen kedves lejönni érte és a barátnőjéért Debrecenbe kocsival, hogy haza tudjanak menni. Így is lett. Mire mi este 10-re Debrecenbe értünk, a férjem is megérkezett kocsival. Éjszaka közepén értünk haza azzal a biztos tudattal, ezt az utazást soha nem fogjuk elfelejteni.<br />'
            + '(P. V. közlése, a beküldő helyesírását követve)',
      'A borítóképen: a beküldő által közölt felvétel<br />'
            + 'A kép forrása: a beküldő által közölt felvétel',

    ],
    quote: '',
    image: require('@/assets/images/border_stories/hidon.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Elfelejtett_keszpenz_COVER_KEP.jpg'),
    images: [
      {
        src: require('@/assets/images/border_stories/IMG_4258.jpg'),
        thumbnailWidth: '220px',
      },
      {
        src: require('@/assets/images/border_stories/IMG_4259.jpg'),
        thumbnailWidth: '220px',
      },
      {
        src: require('@/assets/images/border_stories/IMG_4261.jpg'),
        thumbnailWidth: '220px',
      },
    ],
    slug: 'elfelejtett-kezpenz',
    tags: [
      '#ideiglenes határátlépés',
    ],
    maps: [],
  },
  {
    title: 'Elszaporodtak a csempészek',
    lead: '1920-ban megnövekedett a csempészek száma.',
    content: [
      'Miskolczi Estilap - Reggeli Hirlap<br />'
            + '1922. június (31. évfolyam, 123-145. szám)<br />'
            + '1922-06-22 / 139. szám',
      'Az utóbbi időben az eddig meglehetősen kisebb keretek között lefolyó csempészet hatalmas arányokban megnövekedett. A csempészek különösen a putnoki vonalat használják, mert itt könnyűszerrel átsurranhatnak a határmenti erdőségeken. A csempészek, mig eddig csaknem kizárólag a dohányra vetették magukat, most leginkább bort és szeszesitalt csempésznek Csehszlovákiába, ahol az ötven-hatvan koronáért beszerzett borokat 30-40 szokolért, tehát öt-hatszáz koronáért adják túl. Ha tekintetbe vesszük, hogy egy-egy alkalommal tiz liter bort megerőltetés nélkül átvihetnek, akkor megállapíthatjuk, hogy egy útnak tiszta jövedelme 4000-5000 korona, mig kockázata ezzel szemben öt-hatszáz korona. A nyereség és kockázat eme aránytalansága ösztönzi és a nehéz megélhetési viszonyok késztetik a legtöbb csempészt vállalkozására.',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/elszaporodtak.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Elszaporodtak_a_csempeszek_COVER_KEP.jpg'),
    slug: 'elszaporodtak-a-csempeszek',
    tags: [
      '#csempészet',
      '#sajtóanyag',
    ],
    maps: [],
  },
  {
    title: 'Falatozás a határon',
    lead: 'Mit esznek a határőrök?',
    content: [
      '1990 táján a lila mikrobusszal indultunk Törökországba. Igyekeztünk a legrövidebb utat választani, de mindenképpen Aradon át, mivel ott a helyiek gyűjtöttek össze nekünk echte román benzint, ami nekünk még akkor is féláron volt, ha beleszámítjuk az ő hasznukat. Az autóban így 5 db 20 literes kanna zötyögött velünk. Az út egy szerb-bolgár átkelőn vezetett, ahol a határőrök együttesen tűntek el ebédelni. Azt nem tudom, ki főzött, a bolgárok vagy a szerbek, de látszólag megállapodtak abban, hogy ne zavarják meg a forgalmat KÉTSZER, egyszerre fognak ebédelni, és így a legkisebb – valamivel több, mint egy órás – zavart okoznak a forgalomban. A vámos épület előtt, ahonnan láttuk, hogy mi történt odabent, elővettük a gázfőzőt, és mi is főztünk magunknak ebédet. Már a falatozásnál tartottunk, amikor megjelentek a vámosok és a határőrök, és kunyerálni kezdtek a kajánkból, ami ezek szerint jobban nézhetett ki, mint az ő menzájuk tartalma. Kaptak kóstolót, aztán tovább is mehettünk – mindenféle ellenőrzés nélkül….<br />'
            + '(N. Z. közlése, a beküldő helyesírását követve)',
      'A borítóképen: étkezés az asztalszéknél, MNÉA-F 14180 leltári számon szereplő felvétel<br />'
            + 'A kép forrása: Szabadtéri Néprajzi Múzeum',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/MNÉA F-14180.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Falatozas_a_hataron_COVER_KEP.jpg'),
    slug: 'falatozas-a-hataron',
    tags: [
      '#ideiglenes határátlépés',
    ],
    maps: [],
  },
  {
    title: 'Hamisított engedély társasutazásra',
    lead: 'Hogyan kirándulhat a gyerek a Papa aláírása nélkül?',
    content: [
      'Amikor a ’60-as években a Mama elvitt egy autóbuszos társasutazásra a Tátrába, mindenféle engedélyekre volt szükség. A rendőrségre jártunk pecsétekért, papírokat beadni. Mivel a Papa nem jött velünk, szükség volt az ő jóváhagyására is az utazásomhoz. Ő akkoriban vidéken dolgozott, így le kellett VOLNA mondani az útról. A Rómer Flóris utcai rendőrségről kifelé a kapus rendőr megkérdezte, hogy miért vagyunk ilyen szomorúak. A Mama elmesélte neki. A rendőr mosolygott, és rámutatott a szemben lévő tejboltra. <br />'
            + '-	Látják azt a boltot? Oda kell átmenni, és ott találnak tanukat, az apuka aláírását pedig nem hiszem, hogy az anyuka nem tudná odafirkantani.<br />'
            + 'Átmentünk, majd vissza. Az officér odafönt mosolygott, hogy az apuka milyen gyorsan hazaért Békés megyéből, és alá is tudta írni. Megkaptuk a BETÉTLAPOT.<br />'
            + 'Útlevélre nem volt szükség – úgy, mint ma! Vagy majdnem… Egy betétlap került a Mama személyi igazolványába (a köznyelvben az utcán akkortájt túl gyakran is igazoltató rendőrök kiejtésében: szeméjazonotossági!).<br />'
            + 'Az indulás napján a csapat elindult egy meglehetősen aprócska és lepukkant busszal. A határon a busz nem ment át, hanem az utasok leszálltak és átgyalogoltak a fegyveresekkel erősen védett csehszlovák-magyar határon. A híd közepén találkoztunk a szembejövő csehszlovákokkal, akik az általunk addig használt Ikarusba ültek át, amely jóval kisebb volt, mint a számunkra átadott Škoda busz. Hogyan férhettek el az Ikarusban szegények három napig…<br />'
            + 'Nekünk arany életünk volt a miénkhez mérten hatalmas buszban, túl is éltük a három napot, amikor visszafelé is megtörtént a csere, igen fáradt arcok jöttek szembe a hídon.<br />'
            + '(N. Z. közlése, a beküldő helyesírását követve)',
      'A borítóképen: Ikarus busz<br />'
            + 'A kép forrása: Fortepan',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/Ikarus_1970_Fortepan.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Hamisitott_engedely_COVER_KEP.jpg'),
    slug: 'hamisitott-engedely-tarsutazasra',
    tags: [
      '#útlevél',
      '#ideiglenes hatátlépés',
    ],
    maps: [],
  },
  {
    title: 'Hátamon a tálam',
    lead: 'Népművészeti tál egy cserge áráért Romániából…',
    content: [
      'A Ceausescu érában az emberek nagyon rámentek a cserge  meg a bunda behozatalára. Hétszáz lej vámot kellett rá fizetni, ami olyan 1300 forint körül volt. De nem fogadták el a lejet, hanem úgy kellett váltani. De mindenki svarcba vitte. Még ha váltottunk is ki, akkor visszaváltottuk mondván, hogy nem költöttük el. Egyszer mikor mentünk Romániába, hazafelé jól megpakoltuk a kocsit régiségekkel. Volt egy hatalmas nagy tál is nálunk. Nem magas, de nagy.<br />'
            + '<i>-	De hogy hozzuk át?</i><br />'
            + 'Volt egy nagyfekete pelerinem és erre mondta a férjem, hogy:<br />'
            + '<i>-	Vedd fel! Rákötözöm a hátadra.</i><br />'
            + 'Aztán tényleg rákötözte a hátamra, én meg ültem elöl az anyósülésen, ilyen púposan. Vittünk magunkkal egy csergét is. Ilyet többször csináltuk, mert a vámosok mindig nagyon örültek, hogy vámolhattak csergét. Ha volt nálunk. azt be is vallottuk. Akkor az 700 lej vámot ki is fizettük. Nagy dolog… A lényeg a tál volt, hogy azt át tudtuk hozni. Elmondtuk, hogy van nálunk cserge, a férjem pedig szállt volna ki, hogy kifizesse a vámot. Erre mondta a határőr, hogy<br />'
            + '<i>-	„No, no, a doamnă!”</i> - vagyis, hogy én menjek.<br />'
            + 'Engem meg úgy kivert a víz, hogy majd’ elájultam. Volt egy épület, ahová ment be egy nő, akit a határőr terelgetett előre úgy, hogy udvariasságból megfogta a vállát. Én lemaradtam, mert attól féltem, hogy ahogy megyek be, nehogy az én hátamat is megérintse. Mert akkor biztos elkezd ott döngeni a hátamban. Valahogy sikerült úgy bemennem, engem nehogy terelgessen vagy nehogy udvarisakodjon velem. Aztán kifizettem a csergét. Végig úgy néztem ki, mint a Quasimodo. De aztán áthoztuk a tálat.<br />'
            + '(R.Á. interjúja alapján készült szerkesztett változat)',
      'A borítóképen: a történetben szereplő tál<br />'
            + 'A kép forrása: Kmellár Viktória felvétele (2020)',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/tál.jpeg'),
    coverImage: require('@/assets/images/border_coverImages/Hatamon_a_talam_COVER_KEP.jpg'),
    slug: 'hatamon-a-talam',
    tags: [
      '#csempészet',
      '#ideiglenes határátlépés',
      '#határfrász',
    ],
    maps: [],
  },
  {
    title: 'Határ',
    lead: 'Családi történetek által ihletett alkotás',
    content: [
      'Amikor apám született mélyről jövő, lassú forrásban volt Európa, mint a vasárnapi húsleves, és éppen úgy habot vetett.<br />'
            + 'Nagyanyám végig vérzett a terhesség alatt, mintha méhében is háború dúlna. De apám egészségesen született, szép súllyal, anyja testének háború előtti tartalékait felemésztve. Tömör kezecskéi és lábacskái a csecsemők kiismerhetetlen, ősi ritmusa szerint mozgolódtak, s habár nagyanyám lányt szeretett volna, a globális rend az ő méhéből is fiút rendelt életre, pótolván a harctéren vérbe fagyott férfiakat.',
      'Apám feneke hamarosan kicserepesedett a ritka pelenkacserétől. Nagyanyám kézimunkához és szépíráshoz szokott ujjai először próbálgatták a kapafogást és úri hájakkal övezett dereka lassan szokott hozzá a mosóteknő fölé görnyedéshez.<br />'
            + 'A szlovák határ felé tartva - a magyarság, politika rendelte, nagy áramlásával ellentétes irányban - eszébe sem jutott nagyapám. Ő csak mint tompa fájdalom élt benne, főleg reggeleit keserítette, azt a borzongásnyi időt, amíg az álomból átkelt az eszmélet világába. De a túlélés ezer tennivalója mindig hamar felülírta a bánatot és a kétségbeesés tettvággyá nemesedett. Azt számolgatta, mennyi ideje is szoptatott utoljára. Bal melle már feszült a tejtől, de a csecsemő még aludt a kopott stafírung fehér párnahuzatába bugyolálva.<br />'
            + 'Közeledtek a híd szlovák hídfőjéhez, az átkelőhelyhez. Nagyanyám úgy sejtette, hogy három órája is lehetett, amikor mellre tette apámat, még a vasútállomáson.<br />'
            + '<i>- Úti okmányokat</i> – a határőr fényesre zsírosodott gallérjában türelmetlenül forgatta nyakát <i>- …a gyerekét is!</i><br />'
            + '<i>- Neki még nincs.</i> – Nagyanyám minden szava páraként öltött testet a hidegben, mert ide, az épület mellé nem ért már el a Duna felett dühöngő szél.<br />'
            + '<i>- Akkor nem engedhetem át magukat, forduljanak vissza!</i><br />'
            + '<i>- A nővéremékhez megyünk, itt nincs senkim, kérem, engedje át a fiamat is! </i><br>'
            + '<i>- Maga mehet, de a gyerek nem.</i><br>'
            + 'Nagyanyám szó nélkül letette apámat és kevés maradék csomagját cipelve elindult át, a határon. Minden lépésnyi távolodást a csontjaiban érzett, karja még lüktetett a gyerek terhétől, szíve dobogásától rezonáltak átfagyott tagjai – de ment.<br />'
            + 'Apám sírása a félelem és az éhség hangjain egyre határozottabban szólt. Valaki fölé hajolt, hogy csitítsa, de az idegen tekintet csak fokozta kétségbeesését, elszántságát. A határőrök úgy tettek, mintha zavartalanul folyna tovább az ellenőrzés, de szolgálati idejük, s így türelmük is, fogyatkozott.<br />'
            + 'Nagyanyám hóna alatt gyűlt a savanyú veríték, s egyszerre megérezte, hogy nagyapám nem a háború miatt sodródott el, hanem egy másik asszony melegéért. Magyarország és Szlovákia határán, háború és béke mezsgyéjén rászakadt a teljes magányosság. Zsebkendőjét az arca elé emelte, szája hang nélküli sikolyra nyílt. Csomagjai élettelenül feküdtek körülötte, mert gazdájuk megszűnt mozgásukat, értelmüket táplálni – önmagát is csak egy földfelszínre nehezedő, meghatározhatatlan körvonalú, atomokra bomlani készülő testnek érzékelte – mindenét elvesztette.<br />'
            + 'A fájdalom intenzitása percekre süketté tette. De amint alábbhagyott a szorítása, az első hang, ami tudatáig szűrődött, gyermekének szűnni nem akaró ordítása volt.<br />'
            + '<i>- Hé, maga, nem hallja!?!! Vigye már innen ezt a gyereket! – kiáltott utána a zsíros galléros – Ne is lássam magukat!</i><br>'
            + 'Nagyanyám azzal a mozdulattal kapta karjába apámat, amellyel később a sóskával tömött zsákokat.  Kisfiú-apám arcát máris a tejtől nedvedző mellek felé fúrta, izgatottan kapkodva fejecskéjét. Néhány perccel később gyöngyöző homlokkal szopott, nagyanyámat pedig elöntötte valami ismeretlen eredetű bizakodás, hit abban, hogy épít majd egy házat, ha kell, szögenként összespórolva a rávalót és szül még húgocskát is ennek a gyereknek, egy rendes, hűséges férjtől. Majdnem minden úgy is lett, ahogyan nagyanyám akkor, ott, negyvennégyben megsejtette.<br />'
            + '(S. M. közlése, a beküldő helyesírását követve)',
      'A borítóképen: síró csecsemő (a kép csupán illusztráció)<br />'
            + 'A kép forrása: Fortepan',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/fortepan_12264.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Hatar_COVER_KEP.jpg'),
    slug: 'hatar',
    tags: [
      '#ideiglenes határátlépés',
      '#útlevél',
    ],
    maps: [],
  },
  {
    title: 'Huszonnégy csempészt szállítottak be a miskolczi ügyészségre ',
    lead: 'A hernádpetrii vámőrség három tagját is letartóztatták',
    content: [
      'Miskolczi Estilap - Reggeli Hirlap<br />'
            + '1922. október (31. évfolyam, 223-248. szám)<br />'
            + '1922-10-28 / 246. szám<br />',
      'Hiába minden hatósági szigor, hiába a bíróságok kemény ítéletei, a kapzsiság és fékevesztett nyerészkedési vágy nem okul, a csempészvakmerőség tovább folytatja üzelmeit, mert minden szigoron és minden büntetésen túl ott csábit a trianoni határokon a busás haszon, a korona és szokol nagy valutáris árkülönbözete.<br />'
            + 'Pénteken délelőtt ismét egy nagy csempésztársaságot kisértek végig Miskolcz főutcáján a miskolczi kir. ügyészség fogházába a csendőrök, a kiknek éberségén múlik, hogy utolsó rongyainkat sem viszik ki a busás haszonért cseh megszállott területre. <br />'
            + 'Ennek a legújabb monstrecsempészési ügynek külön érdekessége, hogy ez év elejétől folytatta a csempészbanda bűnös üzelmeit és büntetlenül folytathatta, mert a vámőrség néhány embere, akiknek pedig az ellenőrzés lett volna a törvényes kötelességük, összejátszott a csempészekkel. A csendőrség ébersége azonban, melynek ezúttal a véletlen is segítségére volt, végül is leleplezte a manipulációkat és ennek eredményeként húsz hernádpetrii és négy hernádvécsei lakost, akik sertést, lovat, tehenet, szeszt, dohányt és bort csempésztek át, az ügyészségi fog házba, szállítottak, de letartóztatták és ugyancsak Miskoíczra szállították a hernádpetrii vámőrség három emberét, mig a vámőrség egyik tiszthelyettese, aki irányította a bűnös üzelmeket, átszökött a cseh megszállt területre. <br />'
            + 'A nagyszabású csempészetről tudósítónk kővetkezőket jelenti:<br />'
            + 'Október 16-án a gönczi vásárban feltűnt Török János csendőrtiszthelyettesnek, az ottani őrsvezetőnek egy fiatal parasztlegény, aki sertéseket vásárolt és nagyon gyanúsan viselkedett. Igazolásra szólította fel és az illető Gulyás Lajos 23 éves hernádpetrii lakosnak|,mondta magát. A csendőrtiszthelyettes nem elégedett meg a felvilágosítással, vallatóra fogta a fiút, hogy kinek és micélból vásárolta a sertéseket s minthogy a fiú gyanús feleleteket adott, felkisérte az őrsre, ahol csakhamar kivallotta, hogy csempészés céljából vette a sertéseket egy társaság megbízásából, melynek ő is tagja. <br />'
            + 'Minthogy azon a környéken már ez év eleje óta nagyban folyik a csempészet, azonban a csendőrség minden megfigyelése dacára sem tudta leleplezni, Török tiszthelyettesnek az volt a nézete, hogy ez az eset talán a dolog nyitjára vezeti. <br />'
            + 'Úgy is történt. A csendőrség nagy apparátussal megindította a nyomozást, amelyben résztvett Török János gönczi tiszthelyettes vezetése alatt Váczi Nagy János tiszthelyettes, Tölgyessy János törzsőrmester, Roskó István próbacsendőr a gönczi őrsről, Temmer Imre tiszthelyettes egy csendőrtársával a hidasnémetii őrsről, Frink Gyula tiszthelyettes egy csendőrtársával a hernádvécsei őrsről és Bodnár Antal tiszthelyettes egy csendőrtársával a szemerei őrsről. <br />'
            + 'A nyomozás, mely október 16 ikától október 26 ikáig tartott, teljes eredménnyel járt, amennyiben a csendőröknek sikerült az egész csempésztársaságot, mely huszonnégy tagból állott, leleplezni és elfogni. A nyomozás megállapította, hogy ez a társaság ez év elejétől folytatja bűnös űzelmeit és rengeteg holmit szállított át Csehszlovákiába. Legutóbb 355 liter bort, 30 liter szeszt, 32 drb sertést, 20 drb lovat, 10 drb tehenet csempésztek ki. <br />'
            + 'A csendőrök nyomozása megállapította azt is, hogy a csempészek hónapokon keresztül annak következtében dolgozhattak akadálytalanul, mert a hernádpetrii vámőrség négy embere, köztük egy vámőrszakasz- vezető és egy vámőrtiszthelyettes összejátszott a csempészekkel. Tapasztalataikról jelentést tettek a csendőrök felsőbb hatóságuknak, mire a vámőrség három emberét letartóztatták. Vezetőjük: a vámőrtiszthelyettes azonban neszét vette a dolognak és a letartóztatás előtt sikerült átszöknie cseh megszállt területre. <br />'
            + 'A három letartóztatott vámőrt pénteken szállították be a miskolczi fogházba, ugyancsak pénteken délelőtt kísérték be a miskolczi kir. ügyészségre a csendőrök a csempészlársaság 24 tagját. <br />'
            + 'A letartóztatott esem: észek ezek: Kozmán Gyula, Lázár Lajos, Kovács Imre, Tóth József, Hozmán Dénes, Gulyás József, Paronnay József, Gulyás Lajos, Hintós János, Gulyás István, Kocsis József, Bartkó Mihály, Jávorszky János, id. Lázár Lajos, Heizer János, Vanyó János, Rozinán István, Németh Rezcő, Polecskó János földmivesek, Friedmann Ármin kereskedő, valamennyien hernádpetrii lakosok, továbbá ifj. Orosz János lókereskedő, Glück Ignác szeszfőző, Guttmann Ignác kereskedő, Tóth József földmives, hernádvécsei lakosok. <br />'
            + 'Az ügy tárgyalását előkészítő eljárás valószínűleg hosszabb időt fog igénybe venni, de a kir. ügyészség lehetőleg gyorsítani fogja a nyomozás lefolytatását, hogy a bűnös emberek minél előbb elvegyék büntetésüket.',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/huszonnégy csempész.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Huszonnegy_csempeszt_COVER_KEP.jpg'),
    slug: 'huszonnegy-csempeszt-szallitottak-be-a-miskolczi-ugyeszsegre',
    tags: [
      '#csempészet',
      '#sajtóanyag',
    ],
    maps: [],
  },
  {
    title: 'Ismét meggyilkollak egy cseh-szlovák detektívet ',
    lead: 'Mi történt Mezőkaszonyban? A határon innen történt a gyilkosság. A vakmerő darutollasok. Miért nincs egy szó sem az esetről?',
    content: [
      'Munkás Ujság - Ungvár Munkás Ujság<br />'
            + '1920 (2. évfolyam, 18-23. szám)<br />'
            + '1920-11-19 / 19. szám',
      'A Horovitz-esethez hasonló szenzáció tartja izgalomban Mezőkaszony lakosságát. Sokszor rámutattunk már arra, hogy ebben a kis határmenti városkában tömegesen vannak olyanok, akik nyíltan folytatják az irredenta munkát és a csempészetet. Adatokát vittünk az illetékes hatóságok elé s annak dacára nem történt semmi sem. Hányszor és hányszor lepleztük le Halász volt jegyző működését, hányszor j kiabáltuk, hogy a magyar pribékek átjárnak a határon — nem történt intézkedés. Ahelyett, hogy fellépne ellenük, a panaszkodó elvtársakat bünteti meg. Ami most történt, egy galádul megölt ember élete szintén a Pavluch lelkén szárad.<br />'
            + 'Szombaton, 13-án d. u. Weisz Ignác csehszlovák detekiv teljesített szolgálatot a mezőkaszonyi határban. Már régebben megfigyelte, hogy<br /> <b>a faluból különböző ingóságokat szállítanak a kaszonyi határban fekvő pincékbe.</b><br />'
            + 'Ezúttal is észrevette, hogy a Papp Sándor szőllöjében levő pincébe petróleumot, cukrot, szövetet stb. hordanak. Mikor látta, hogy az áruhordást befejezték odament a pincébe s a legnagyobb meglepetésére<br />'
            + '<b>egy magyar őrjáratot talált ott egy hadnagy vezetésével, aki nyilván az értéktárgyakat akarta elszállitatni.</b><br />'
            + 'A hadnagy ur a detektivhez fordult és felszólította, hogy igazolja magát. A detektív arra való hivatkozással, hogy ő itthon van, megtagadta az igazolást s látva a túlerőt, elakart távozni. <br />'
            + 'A Horthy-csemete erre parancsot adott az egyik katonának, hogy lőjje le a detektivet. A katona utána lőtt és egy robaanó golyóval földreteritette a szerencsétlen embert, aki a helyszinen azonnal meghalt. <br />'
            + 'A tragikus esetről — amint tudjuk — mindezideig egy szó jelentés sem érkezett. <br />'
            + 'Kell ide még kommentár? Ugy-e nem. Kiváncsiak vagyunk a vizsgálatra. Kiváncsiak vagyunk mi lesz Pappal, a Pavluch jóbarátjával, akinek árucsempésző tevékenysége statáriális rendelkezések alá esik. Ordítani szeretnénk egyre csak azt, hogy mi lesz? mi lesz? mi lesz?',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/Szozat_1923_07__pages39-39-page-001.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Ismet_meggyilkoltak_COVER_KEP.jpg'),
    slug: 'ismet-meggyilkoltak-egy-cseh-szlovak-detektivet',
    tags: [
      '#csempészet',
      '#sajtóanyag',
    ],
    maps: [],
  },
  {
    title: 'Kolbász hátizsákban',
    lead: 'Mit ehetett egy magyar 1981-ben Amerikában?',
    content: [
      '1981-re megérett bennem a vágy, hogy feleségemmel baráti, nem-hivatalos látogatást tegyünk az Egyesült Államokba. Utólag bevallom, hogy az út egy hátsó szándékot is tartalmazott, miszerint Bloomington, Indiana, egyetemén folytatnám inkább a budapesti ELTE-n elkezdett tanulmányaimat, ami akkoriban még 15 éves büntetést jelentett volna számomra és valószínűleg közeli rokonaim számára is, hisz nem hivatalosan utaztunk, hanem limitált kint tartózkodási időre. Ezek a szankciók azonban az út előtt nem igen zavartak, hisz én tanulni akartam. Megjegyzem, Bloomington Altajisztikai és néhány más tanszéke tele van Budapestről odatámolygott profokkal, akikről nemrégiben hallottam azt a történetet, hogy „ő a feleségével azért ment el innen, hogy ne kelljen gulyást enni, és ne kelljen magyar nótát hallgatni hozzá. Most pedig azért él ott, hogy minél többször ehessen gulyáslevest és hallgathasson hozzá magyarnótát.” Akkoriban még a Dalai Láma öccse is ott volt tibeti lektor, ami egy tibeti szakos diáknak azért fontos dolognak tűnt. Akkor.<br />'
            + '[…] Gépünkről élvezettel bámultam a jégtáblákat odalenn az óceánon, volt egy fél óránk Izlandon is, ahol kitanulmányozhattam a reptéri bolt könyvrészlegét, majd végig követhettem Grönland partjait, míg végre New York fölé keveredtünk. 55 percig nem kaptunk leszállási engedélyt, ott körözgettünk az apró kék fürdőmedencés telkek fölött, míg végre sikerült az Új Világban földet érni.<br />'
            + 'Ekkor még javában 9.11 előtt vagyunk, de rendelkezések már akkor is voltak. Öreg barátomnak New Havenben, hisz annyira beleszeretett a békés megyei meglehetősen csípős vastag háziszalámiba, hát miért ne azt vigyek neki…<br />'
            + 'Két bőröndünk volt és egy nagy hátizsák. Hatalmas méretű fekete, vagy azóta tudom, hogy African American hölgy totyogott oda hozzánk és vizslatni kezdte a csomagjainkat. És ekkor még mindig nem tudtam, hogy tilos bármiféle élelmet, de különösen húsárut, ezen belül házi készítésű füstölt húsárut bevinni az Államokba.<br />'
            + 'Az említett terjedelmes hölgy kinyittatta mindkét bőröndöt, szinte centiről centire átnézett, átpakolt mindent, és közben dünnyögött magában… Látszott rajta, hogy most kéne valami az előléptetéséhez, valamit kéne találnia nálunk.<br />'
            + 'Aznap este öreg barátom már igaz, helyi kenyérrel és helyi zöldpaprikával, de békés megyei igen csípős házi füstölt vastag szalámit csemegézett otthonában, ugyanis az Afrikai Amerikai lédi tojt a hátizsákomra, nem érdekelte, így mind a három szál csoda célhoz ért… Ha most arra gondolok, mi lett volna, ha megtalálja… nem, arra nem is gondolok. Inkább arra, hogy milyen baromi finom dolog volt az… most is lóg még egy szál az albérleti konyhámban… nyamm!<br />'
            + '(N. Z. közlése, a beküldő helyesírását követve)',
      'A borítóképen: bográcsgulyás, MNÉA F-69645 leltári számon szereplő felvétel<br />'
            + 'A kép forrása: Szabadtéri Néprajzi Múzeum',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/MNÉA F-69645.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Kolbasz_a_hatizsakban_COVER_KEP.jpg'),
    slug: 'kolbasz-hatiszakban',
    tags: [
      '#ideiglenes határátlépés',
    ],
    maps: [],
  },
  {
    title: 'Közép-Európa',
    lead: 'Botlik József: Az őrvidéki (burgenlandi) magyarság sorsa (Az első két év: 1922-1923)',
    content: [
      'Vasi Szemle<br />'
            + '2008 (62. évfolyam, 1-6. szám)<br />'
            + '2008 / 6. szám<br />',
      'Közben a magyar hatóságok az 1920-as évek elején, a lehetőségekhez képest igyekeztek segíteni a többségében nélkülöző őrvidéki magyar és német lakosságon. Például az 1923. január 7-én folytatott magyar-osztrák tárgyalásokon a határsértésekkel kapcsolatban a magyar fél kénytelen volt előadni, hogy azok az ún. semleges övezet fennállásának, illetve a még most is űzött csempészetnek a következményei. A semleges sáv három nap múlva, 1923. január 10-én megszűnt, de a csempészet még egy ideig virágzott. Jegyzőkönyvekkel bebizonyítható, hogy az osztrák fél „állandóan semmis okokból eredő felszólalásokkal árasztatott el, [ezért] utasítattak az alantas [ti. a helyi] hatóságok, hogy ők is lehetőleg minden osztrák részről elkövetett kifogásolható esetet tegyenek jelentés tárgyává. [...] Különben az eddigi tapasztalat kimutatta majdnem minden felszólalásánál, hogy a vizsgálat eredménye homlokegyenest ellenkezett az osztrák részről előadott tényállással, ami indokát nagyrészt abban leli, hogy a határ menti lakosság a csempészetben jövedelmező foglalkozást látva, összefog a magyar határőrökkel szemben, és minden csempész jogos üldözését a békés polgár zaklatásának tünteti fel. [...] Emellett természetes, hogy [...] az élelmiszerek Ausztriába való csempészése nem ellenkezik az osztrák érdekkel, az osztrák határőrök nem járnak el túl szigorúan”.',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/vasi 2008.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Kozep_Europa_COVER_KEP.jpg'),
    slug: 'kozep-europa',
    tags: [
      '#csempészet',
      '#sajtóanyag',
    ],
    maps: [],
  },
  {
    title: 'KÖZGAZDASÁG',
    lead: 'Egy kis statisztika a nyugat magyarországi csempészek öthavi „munkájáról"',
    content: [
      'Szózat<br />'
            + '1923. július (5. évfolyam, 146-171. szám)<br />'
            + '1923-07-04 / 148. szám<br />',
      'A szombathelyi vámőrkerület területén, a Dunától a Muráig terjedő határsávban, az idén is nagy volt az élénkség. Derék vámőreinkkel igen sok kemény diót törettek fel a „holddal bujkálók“ (moonshiner), ahogy az amerikai tájszólás nevezi találóan a csempészt Vámjövedéki szempontból az országnak egy határvidékén sincs olyan szorgalmas élet mint a nyugati határvonal mentén, ami érthető is, ha a szomszédos utódállamok gazdasági helyzetét összehasonlítjuk az osztrák köztársaságéval. <br />'
            + 'A szombathelyi vámőrkerület minden határvidékének meg van a maga különleges csempészszárnya. így pld. az Ólmod-—Und-i szakaszban az állatcsempészetet űzték előszeretettel, Nagycenk— Harka vidékén a malac és tojás-csempészet öltött nagy arányokat, Sopron és Szentgotthárd környékén mindennemű élelmicikknek kisebb mennyiségben való kicsempészését kísérelték meg; a szombathelyi szakaszban só- és dohánycsempészet dívott, Moson-szolnok és Hegyeshalom vidékén a cukor és Sacharin kelt lábra; a Dunavonal és a magyar-délszláv határszakasz aránylag a legcsöndesebb. <br />'
            + 'A kötelességét híven teljesítő vámőr minden csempészfogás után törvényszerüleg megállapított méltó anyagi jutalomban részesül, hogy a megvesztegetés ördöge meg ne kisérthesse! A vámőrkerületi parancsnoksághoz a szakaszoktól beérkező „tény- leirásokat“ az illetékes vármegyei pénzügyigazgatóságok dolgozzák föl és a törvény megszabta jutalékokat szétosztás végett kiküldik a szakaszparancsnokságoknak. <br />'
            + 'Az esetben, ha a jutalék összege meghaladna a 40.000 koronát, a kiutaláshoz a magyar királyi pénzügyminiszter ur hozzájárulása szükséges. <br />'
            + 'Egyéb esetekben a pénzügyigazgatóságok önállóan járnak el és elismerésre méltó gyors munkát, fejtenek ki, ami fölötte szükséges is, különben a jutalékrendszer — különösképpen figyelembevéve koronánk gyógyíthatatlannak látszó hátgerincsorvadását — teljesen célját tévesztené! <br />'
            + 'Külön ki kell emelni a soproni pénzügyigazgatóságnak eredményes működését, amely a már említett időszakokban 2,162.948 koronát folyósított csupán jutalékok szétosztása végett. <br />'
            + 'Az 1923. évi január hó elsejétől május hó végéig terjedő időszakban jogerősen elkobzott csempészáruk hivatalos kimutatását az alábbiakban ismertetjük:',

    ],
    quote: '',
    image: require('@/assets/images/border_stories/kozgazdasag.jpg'),
    images: [
      {
        src: require('@/assets/images/border_stories/1kep.jpg'),
        thumbnailWidth: '220px',
      },
    ],
    coverImage: require('@/assets/images/border_coverImages/KOZGAZDASAG_COVER_KEP.jpg'),
    slug: 'kozgazdasag',
    tags: [
      '#csempészet',
      '#sajtóanyag',
    ],
    maps: [],
  },
  {
    title: 'Lámpa részletre ',
    lead: 'Használati tárgyak csempészete Románia területéről Ceausescu idején.',
    content: [
      'Az asztalunk felett lógó lámpát a Ceausescu érában vettük Kolozsváron egy idős házaspártól, akik éppen Németországba mentek egy temetésre, majd ott is maradtak, mert a férjletelepedési jogot kapott. De ekkor a lánya meg a fia még nem kapták meg az útlevelet. Ennek a családnak volt ez a gyönyörű lámpája és megkérdeztük, hogy mit csinálnak vele, ha elmennek. Mondta, hogy<br />'
            + '<i>-	Hát eladom.</i><br />'
            + 'Akkor mondtuk neki, hogy már meg is van rá a vevő. Na igen, de van ez a hatalmas búra, és hozzá egy annál is nagyobb ellensúly. De azt hogy hozzuk át a határon? Szétszedtük, és csak a felét mertük elhozni, hogy ha mégis meglátják a határon, hogy csak egy fél lámpa. A búráját egy ismerősnél hagytuk, Nagyváradon. De aznap éppen egy olyan határőr dolgozott, akivel már korábban összebarátkoztunk. Kiderült róla akkor, hogy szívbeteg, úgyhogy mindig vittünk neki Panangint. Így áthoztuk a fél lámpát, de most mit csináljunk? Hát a másik felét is át kéne hozni, főleg addig amíg ez az őr van szolgálatban, mert tudtuk, hogy ő nem fog minket megnézni. Úgyhogy mentünk egy 5-600 métert a határtól, ahol volt egy erdő. A férjem oda vitte be a lámpatestet szépen becsomagolva. Kiszámolta, hogy hányadik fa alá teszi, húzott rá avart is, mert éppen őszi idő volt. Oda letette, kiürítettük a kocsit és mentünk vissza. Mondta a férjem a határőrnek, hogy a feleségem otthagyta a lakáskulcsot az ismerősünknél és nem tudunk hazamenni, úgyhogy vissza kell menni. A problémát az okozta, hogy nem akartunk újra sorban állni. Úgyhogy mi felvettük a pléhpofát, hogy mi már másodszorra jövünk és tudta a határőr, úgyhogy nem szólt érte. Áthoztuk utána a búrát és a többi részt is, nem nézte az öreg. Az erdőnél pedig kiszámoltuk, hogy körülbelül a többi része hol van. A férjem legalább fél órán át kereste, mert egyet-kettőt fordult, aztán már egészen máshol volt. Végül megtalálta a másik felét is. Így került át ez a gyönyörűséges lámpa.<br />'
            + '(R.Á. interjúja alapján készült szerkesztett változat)',
      'A borítóképen: a történetben szereplő lámpa<br />'
            + 'A kép forrása: Kmellár Viktória felvétele (2020)',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/lámpa.jpeg'),
    coverImage: require('@/assets/images/border_coverImages/Lampa_reszletre_COVER_KEP.jpg'),
    slug: 'lampa-reszletre',
    tags: [
      '#csempészet',
      '#ideiglenes határátlépés',
    ],
    maps: [],
  },
  {
    title: 'Lebuktak',
    lead: 'Nemzetközileg védett madártetemeket akart átcsempészni a románmagyar határon…',
    content: [
      'Zalai Hírlap<br />'
            + '2002. szeptember (58. évfolyam, 204-228. szám)<br />'
            + '2002-09-17 / 217. szám',
      'Budapest (mti) - Nemzetközileg védett madártetemeket akart átcsempészni a románmagyar határon két olasz állampolgár a hétvégén - tájékoztatott Sipos Jenő, a Vám- és Pénzügyőrség Országos Parancsnokságának szóvivője közleményben. A tételes vámvizsgálat során kiderült, hogy egy hűtőtáskában megkopasztva és lefagyasztva tizenhat sárszalonkát és egy fütyülőrécét rejtettek el.',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/lebuktak.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Lebuktak_COVER_KEP.jpg'),
    slug: 'lebuktak',
    tags: [
      '#csempészet',
      '#sajtóanyag',
    ],
    maps: [],
  },
  {
    title: 'Lócsempészet a demarkáción',
    lead: 'Hajdúmegyébe csempészek hurcolták be a takonykórt — A Magyarság tudósítói ától. — ',
    content: [
      'Magyarság<br />'
            + '1920. december (1. évfolyam, 1-14. szám)<br />'
            + '1920-12-28 / 11. szám',
      'A trianoni bölcsesség tudvalevőleg olyan uj határokat állapított meg, hogy egyes határmenti községekből a szomszéd községbe csak idegen területen át lehet eljutni. Ennek következtében úgy egyes magyar, mint idegen községek lakói, különösen kupecek, akiknek egyik községben is, másik községben is van ingatlanuk, állandó határátlépési engedély boldog tulajdonosai. Ezek a boldog tulajdonosok így az úgynevezett betüremlési helyeken egyszerűen átkocsiznak a demarkáción és ha másként nem megy, lovat fogva szekerük elé, kettőt szépen átcsempésznek idegen területre. Mivel pedig az állategészségügyi törvény szerint szekérbe fogott állatok járlatlevelét a határátlépésnél nem lehet kérni, a csempészet egész vigan folyik. <br />'
            + 'A román határon így csempészik be, a cseh határon pedig így csempészik ki a lovakat. Mind a két csempészet baj. A kifelé csempészet azért, mert amúgy is gyenge lóállományunkat csökkenti, a befelé csempészet meg azért, mert az oláh megszállás alatt levő területről minduntalan ragályt hurcolnak be. Hajdumegyében például a múlt héten tizenkilenc lovat irtottak ki egy községben, mert takonykórosak voltak. A ragályt természetesen oláh területről hurcolták be. <br />'
            + 'A hatóságoknak már most súlyos gondokat okoznak ezek a kis határmenti Csempész-Eldorádók, amelyekkel szemben tehetetlenek, mert egészen bizonyos, hogy ha a határ ilyen marad, nyílt országutja lesz kelet felöl a Romániában már fellépett keleti marhavésznek is. Annál komolyabb ez az aggodalom, mert aki az oláhokat ismeri, egészen bizonyosra veheti, hogy szomszédaink szándékosan meg fognak ajándékozni ezeken a betüremlési pontokon fertőzött szarvasmarhákkal is. <br />'
            + 'Egyelőre csupán azt lehetne megszigorítani, hogy a magyar hatóságok a csempészett lovakra járlatleveleket ne állítsanak ki. Mert ez ma az oláh demarkáció mentén elég könnyen megy. Egy bizonyos szemhunyás ugyanis szükséges volt, mert az oláhok a tőlünk lopott lovakat csempészték vissza és ha a szigorú jog álláspontjára helyezkedtünk volna, nem vásárolhattuk volna vissza azokat a mezőhegyesi csődöröket, amelyeket román tisztek árusítottak a határ mentén. <br />'
            + '<b>Csonka Magyarország nem ország, </b><br />'
            + '<b>Egész Magyarország mennyország!</b><br />',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/lócsemp.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Locsempeszet_COVER_KEP.jpg'),
    slug: 'locsempeszet-a-demarkacion',
    tags: [
      '#csempészet',
      '#sajtóanyag',
    ],
    maps: [],
  },
  {
    title: 'Mindegy, csak Lada legyen!',
    lead: 'Honnan érkezett a Donald rágó Magyarországra?',
    content: [
      'Egyik ismerősünk elvitt minket családostul Bécsbe, ahova akkor már lehetett menni vásárolgatni... Egy narancssárga Lada kombival utaztunk. Jól bevásároltunk, sok mindent hoztunk magunknak, az ismerősnek is. A lényeg, hogy a határ innenső oldalán valamiért az ismerős (akinek akkor egy bazárüzlete volt, ahhoz is hozott be Donald-rágót és hasonlókat) és édesapám kiszállt ügyet intézni a vámosokkal, vagy nem is emlékszem már rá pontosan miért. De a lényeg, hogy láttuk, hogy jönnek visszafelé, és beszállnak mögöttünk három-négy autóval egy másik narancssárga Lada kombiba. Aztán kiszállnak és beszállnak a "mienkbe". Elmesélték, hogy ugyanúgy hárman ültek hátul, ugyanolyan autó, de még a rendszáma is egy számjegyben tért el. Beszálltak, és azt látták csak, hogy a belső tér nem egészen stimmel és a kulcs sem stimmel az indítóba. Ma is nevetünk a sztorin.<br />'
            + '(S. P. közlése, a beküldő helyesírását követve)',
      'A borítóképen: Lada kombi<br />'
            + 'A kép forrása: Fortepan',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/lada_kombi_1970_Fortepan.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Mindegy_csak_COVER_KEP.jpg'),
    slug: 'mindegy-csak-lada-legyen',
    tags: [
      '#ideiglenes határátkelés',
    ],
    maps: [],
  },
  {
    title: 'Miskolc a csempészváros ',
    lead: 'Évente háromezer csempészt Ítél el a bíróság',
    content: [
      'Felsőmagyarországi Reggeli Hirlap<br />'
            + '1937. január (46. évfolyam, 1-25. szám)<br />'
            + '1937-01-24 / 19. szám',
      'Tüssszkő, tüssszkő, szacharin— tüssszkő— A miskolci hetivásár ritmustalan lármáján keresztül sziszeg a hang, a miskolci csempészárus halk, vásári kikiáltása. A csempész olcsón vesztegeti áruját. A miskolci vásárok olcsójánosa: a csempész. <br />'
            + 'A csempész romantikus, de számoló kereskedő. Izgalmasabb, viszont versenyképesebb a kalkulációja, mint a vásározó kiskereskedőé: egy kilogramm súlyú tűzkövet például átlagban 50—60 pengőért vásárol meg Csehországban. Miskolcon azután darabonként értékesíti áruját egy-egy darabért 4 fillért kér el, ami kereken négyszerié olcsóbb, mint a dohánytőzsdék legális, 16 filléres darabára. Ilyenmódon mintegy 176 pengőt árul ki egy kilogrammnyi tűzkőből. 126 pengő rajta a tiszta nyeresége. A cigarettapapír darabjáért pedig két fillért fizetnék, ezzel szemben 6 fillérért adják tovább. A cigarettapapír trafik ára 12 fillér, a csempész tehát a cigerattapapir szakmájában is verhetetlen konkurrenciát teremt az államnak. <br />'
            + 'A csempész kalkulációjából hiányzik az adó és hiányzik a helypénz, amit a törvényekkel számoló (kereskedőnek, a dohánytőzsdéknek és a vásározónak meg kell fizetnie, Igaz, hogy ezzel szemben állandóan kalkulálnia kell a saját személyes szabadságával. <br />'
            + 'A csempészet kétségkívül jó üzlet és így nem csoda, ha Felsőmagyarországon, a trianoni cseh határ közelében rengetegen, ezren és ezren vállalják a fogház kockázatát, csak azért, hogy törvényellenes kereskedés utján törvényellenes, de nagy haszonhoz jussanak. Miskolc Budapest után az ország legnagyobb csempészvárosa, a felsőmagyarországi, csempészkereskedelem gócpontja. A miskolci törvényszék évenként 3 ezer embert von felelősségre csempészet miatt. Borsod és Abauj megyében egész vidékek, egész járások népességének nagy része űzi a csempészetet, Azt is nyilvántartja a pénzügyőri felügyelőség, hogy melyek azok a csehországi falvak, amelyekből legsűrűbben hoznak át csempészárut Magyarországra: Rimaszécs, Lénárdfalva, Tornaalja, Sajógömör, Hosszúszó, Szilice, Torna, Komaróc, Kenyhec, Abaújnádasd. <br />'
            + 'A csempészkereskedelem lebonyolítására széleskörű szervezetet építettek ki a felsőmagyarországi csempészek. A csempészetet úgynevezett főcsempészek irányítják, akik rendszerint négy-öt, a nagyobbak 10—20 alcsempésszel végeztetik a szállítás munkáját. Az árut titkos határmenti raktárakban gyűjtik össze és ezekből osztják szét azoknak a bizományosoknak, akik azután főképen Miskolcon, de Mezőkövesden, Diósgyőrött, Ózdon és más ipartelepeken, vagy az ország fővárosában igyekeznek ezeket értékesíteni. A csempészek legnagyobbrészt gyalogszerrel, vagy lóval vontatott közlekedési eszközökkel hordják át az árut Csehországból, vannak azonban tőkeerős csempészvállalatok, amelyek teherautókat is mozgósítanak.  A miskolci pénzügyőrségen abban látják a szokatlanul elterjedt csempészet legfőbb okát, hogy a szerencsétlenül megválasztott cseh-magyar trianoni határ, Borsodban és Abaujban leginkább erdőségen fut keresztül, ami különösen kedvező viszonyokat teremt a törvénytelen kereskedés számára',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/Miskolc.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Miskolc_a_csempesz_COVER_KEP.jpg'),
    slug: 'miskolc-a-csempeszvaros',
    tags: [
      '#csempészet',
      '#sajtóanyag',
    ],
    maps: [],
  },
  {
    title: 'Monográfia az 1956-os forradalom és szabadságharc besenyszögi emlékeiről',
    lead: 'Egy disszidálás története',
    content: [
      'Molnár János Lajos Ferenc életének teljes történetét a Monográfia az 1956-os forradalom és szabadságharc besenyszögi emlékeiről c. kötetben olvasható, Danyi Nikolett írásában. ',
      'A disszidálás története Danyi Nikolett felolvasásában hallható.',
      'A borítóképen: Molnár János Lajos Ferenc Torontóban<br />'
            + 'A kép forrása: Danyi Nikolett felvétele (2017)',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/IMG_20170405_173643.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Monografia_COVER_KEP.jpg'),
    slug: 'monografia-az-1956-os-forradalom-es-szabadsagharc-besenyszogi-emlekeirol',
    tags: [
      '#végleges határátkelés',
    ],
    maps: [],
  },
  {
    title: 'Nagyarányu cukorcsempészés az elszakított területekről Magyarországra',
    lead: 'A fűszerkereskedők memoranduma a pénzügyminiszterhez',
    content: [
      'Magyarország<br />'
            + '1929. november (36. évfolyam, 249-273. szám)<br />'
            + '1929-11-17 / 262. szám',
      '(A Magyarország tudósítójától.) A fűszerkereskedők köréből különösen vidéken igen sok panasz hangzik el az utóbbi időben, hogy a cukorforgalmuk lényegesen csökken, sőt sok helyen a minimálisra redukálódott. A kereskedők nyomozni kezdtek, s megállapították, hogy az utóbbi hónapokban rengeteg mennyiségű cukrot csempésztek át a cseh és osztrák határon. Az így átcsempészett, cukrot a legális kereskedelmi ár feléért árusítják és oly nagy mennyiség kerül belőle forgalomba, hogy nemcsak a határmenti városok és községek szükségletének nagy részét látja el, de az ország távolabbi részébe is sok kerül belőle. <br />'
            + 'A cseh és osztrák határ mentéig valóságos társaságok alakulnak, amelyek a legkitűnőbb szervezettel csempészik át a cukrot a trianoni határon. A csempészek ügynökök seregével dolgoznak, akik az árut az ország belsejébe szállítják és forgalomba hozzák. Érdekes, hogy miért főképpen cukrot csempésznek az országba,1? Ennek az a magyarázata, hogy a cukrot magas kincstári illeték terheli, úgyhogy míg például a fővárosban egy kiló kristálycukor kiskereskedelmi fogyasztói ára 1.14 pengő, a kockacukoré 1.22 pengő, vidéken pedig néhány fillérrel drágább, addig a csempészet cukrot a zugforgalomban kilónként 20—40 százalékkal olcsóbban árusítják. A kiskereskedőnek a cukor kilójánál csupán néhány fillér haszna van. addig a csempészek 40—50 filléres haszonnal értékesíthetik a cukrot. Érthető tehát, hogy a kiskereskedők forgalma nagymértékben Csökkent és a határmenti vidékeken sok fűszerkereskedő be is szüntette a cukor tartását, mert nem tudta eladni az árut._ Különösen a mezei munkák befejezése után lendült fel a határ mentén a csempészet. A csendőrség, pénzügyőrség. Vámőrség fokozott éberséggel ügyel a határon és_ minden héten sikerül néhány csempészt ártalmatlanná tenni. Néhány nappal ezelőtt Brennberg felett, a trianoni osztrák- magyar határon fogtak el csempészeket, akikről kiderült, hogy cukrot akartak behozni az országba. A brennbergi csendőrőrs egyik járőre éjszakai kőrútján szembetalálkozott egy háromtagú társasággal, amely Sopron felé igyekezett. <br />'
            + 'A csendőrök megadásra szólították fel a csempészeket, akik azonban az éjszaka leple alatt meg akartak szökni, majd mikor látták, hogy ez nem sikerül, a csendőrökre támadtak. A megtámadott csendőrök erre fegyverükhöz nyúltak, mire a csempésztársaság két tagja megadta magát, de az egyiknek sikerült elmenekülnie. A csendőrök a menekülő után lőttek, do a golyó célt tévesztett és a csempész a bozótok között eltűnt. <br />'
            + 'Ha a határőrségnek időnként sikerül is néhány csempészt ártalmatlanná tenni, a szervezett és mindenre kész bandák garázdálkodását nehéz megszüntetni. A fűszerkereskedők, akiket a cukorcsempészek működése következtében rendkívüli károk érnek, ezért tegnap beadvánnyal fordultak a pénzügyminiszterhez, akitől fokozottabb védelmet kértek, A Fűszerkereskedők Országos Egyesületének bejelentése a következőképpen hangzik:<br />'
            + 'A határmenti városok és községek (Miskolc, Salgótartján, Sátoraljaújhely, stb) fűszerkereskedői, szatócsai már hosszabb idő óta azt állapítják meg, hogy cukorforgalmuk igen nagy mértékben csökkent. Ennek okait keresve arra a megdöbbentő felfedezésre jutották, hogy a cukorforgalom csökkenése arra az igen nagyarányú cukorcsempészésre  vezethető vissza, amely az elszakított területekről irányul Magyarországra. A csempészés ma már nemcsak a határmenti városokra szorítkozik, hanem az államkincstár súlyos megkárosításával kiterjed az egész országra is. <br />'
            + 'Országos egyesületünk az ország legkülönbözőbb részeiből érkező megkeresésekre elhatározta, hogy Nagy méltóságod figyelmét erre a mindinkább elburjánzott visszaélésekre felhívja. A határmenti városok kereskedői amúgyis súlyosan érzik a trianoni békeszerződés hátrányait. Országunk rettentő megcsonkítása folytán elvesztették az azelőtt Magyarországhoz tartozott községekből vevőiket és így fokozottabban kell ügyelnünk arra, hogy a legalább megmaradt helyeit fogyasztó- közönsége vásároljon náluk és ne kelljen illegitim versenytársakkal megküzdeniök.<br />'
            + 'A memorandumban végül kérik a pénzügyminiszter intézkedését a csempészés meggátlására. <br />'
            + 'Mit mondanak a pénzügyminisztériumban <br />'
            + 'Érdeklődtünk a pénzügyminisztérium illetékes osztályában, milyen intézkedéseket tesznek a csempészei ártalmatlanná, tételére. Érdeklődésünkre a következő felvilágosítást kaptuk: <br />'
            + '— A minisztérium nagy, figyelemmel kíséri a határokat és a magunk részéről mindent elkövetünk a csempészés meg- gátlására, de a mai határok mellett a csempészést teljesen megakadályozni nem lehet. A fűszerkereskedők bejelentését külön a központi vámigazgatósághoz tesszük át, hogy az közegeinek megfelelő utasítást adjon.<br />',

    ],
    quote: '',
    image: require('@/assets/images/border_stories/cukorcsemp_h.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Nagyaranyu_cukorcsempeszes_COVER_KEP.jpg'),
    slug: 'nagyaranyu-cukorcsempeszes-az-elszakitott-teruletekrol-magyarorszagra',
    tags: [
      '#csempészet',
      '#sajtóanyag',
    ],
    maps: [],
  },
  {
    title: 'Nejlon inget Cseszkóból',
    lead: 'Nájlon és sziloningek a korszellem divatának megfelelően Csehszlovákiából.',
    content: [
      'Abban az időben a „Cseszkó” (v.ö. Csehszlovák Szocialista Köztársaság) komoly úticél volt a magyarok számára. A magyar állampolgárok odajártak tornacipőért és a másik komoly árucikkért, a „nájlon” vagy „szilon” ingekért. (Ezen kívül futott még a Hardmuth ceruza, a Versatil töltőceruza és a radír!) Az ingek egyikéről sem tudom, hogy mit jelentett, de szinte mindenki járt ilyen egészségtelen vackokban egy időben. Emlékszem, a tömött villamosokon, buszokon milyen kellemes illatot árasztottak az ezekbe bújt izzadó emberek a forró nyarakon.<br />'
            + 'A behozatali mennyiség azonban limitálva volt. A vonaton hazafelé mindenkin tornacipő volt, egy másik pár a csomagban, a többi pedig itt-ott eldugva. A határ előtt mindenki heves pakolásba kezdett, hogy minél kevésbé legyen feltűnő a mikrodózisos csempészet. A makrodózisos változatot csak a diplomata útlevéllel utazók, avagy sportolók tehették meg, persze hallgatólagos hatósági engedéllyel. És meg is tették.<br />'
            + 'A határ előtt már a vonaton megjelentek a csehszlovák uniformisba bújt szakemberek, és minden fülkébe berobbantak:<br />'
            + '<i>„Dobrí gyeny, csehszkoszlovenszká pászová kontrolá! Máguk mágyárok?”</i></br>'
            + 'Mindenki hallgatott, minél kevésbé próbálván felhívni önmagára a figyelmet. Aztán lett, ami lett. Vagy megtalálták és elvették, néha még le is szállítottak embereket a komoly nemzetközi csehszlovák-magyar határátkelőnél, néha meg a próbálkozók nyertek és hazavihették az összes nájlon és szilon inget.<br />'
            + '(N. Z. közlése, a beküldő helyesírását követve)',
      'A borítóképen: 2006.119.65. és 2007.5.325. leltári számú férfi ingek<br />'
            + 'A kép forrása: Szabadtéri Néprajzi Múzeum',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/2006_2007.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Nejlon_ing_COVER_KEP.jpg'),
    slug: 'nejlon-inget-cseszkobol',
    tags: [
      '#csempészet',
      '#ideiglenes határátlépés',
    ],
    maps: [],
  },
  {
    title: 'Régit az új helyére ',
    lead: 'Zilahi tányér Romániából az 1970-es években? ',
    content: [
      'A ’70-es években jártunk Romániában gyűjtőkhöz, tőlük vettük a tárgyakat. Nagyváradon megismerkedtünk egy asszonnyal, aki megengedte, hogy amiket nem merünk áthozni a határon, vagy visszaküldik őket, akkor azokat nála le tudtuk tenni. Nagyvárad az nem volt olyan messze a határtól. Mikor indultunk hazafelé a határtól messze még nagy mellényünk volt, de már Nagyvárad közelében elkezdett remegni a lábunk, hogy hogy visszük át a tárgyakat. Mert volt olyan korábban, hogy egy faszenes vasalóval visszaküldtek mondván, hogy az régiség. Hát meg volt annak is a furfangja, hogy hogyan kellett ezeket a tárgyakat áthozni. Volt ott a közelben egy bizományi bolt, amit úgy hívtak, hogy Konszignáció. Emlékszem, hogy 13 lej volt egy egyszerű, régies tányér. Abból vettünk 10-15 darabot, amihez a vásárlás után kaptunk egy kis cédulát, hogy ebből a boltból van. Aztán abból hármat-négyet kivettünk és egy-egy Zilahi tányért vagy valamit úgy közé tettünk, hogy ha nézi a vámos vagy a határőr, akkor ne lássa. A papír meg ott volt a kezünkben, hogy ezeket a Konszignációban vettük. Olyat azért, amibe dátum volt, azt nem mertünk beletenni. De ezt így bevették. Megszámolták, hogy 13 darab, nem több, ezek aztán így átjöhettek.<br />'
            + '(R.Á. interjúja alapján készült szerkesztett változat)',
      'A borítóképen: a történetben szereplő tányérok<br />'
            + 'A kép forrása: Kmellár Viktória felvétele (2020)',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/tanyerok.jpeg'),
    coverImage: require('@/assets/images/border_coverImages/Regit_az_uj_COVER_KEP.jpg'),
    slug: 'regit-az-uj-helyere',
    tags: [
      '#csempészet',
      '#határfrász',
      '#ideigelenes határétlépés',
    ],
    maps: [],
  },
  {
    title: 'Turnésztorik',
    lead: 'A Kaláka együttes határtörténetei',
    content: [
      'A Kaláka  együttes 1969-ben alakul Budapesten, sokszínű zenéjük alapját a versek adják. Az együttes  25. évi jubileumára Kányádi Sándor  ezeket a sorokat írta: „A Kaláka együttes elegáns, más muzsikusokkal össze nem téveszthető zenei tálcán nyújtja a verset a hallgatóknak. Nem ráerőszakolják a maguk szerzeményeit, hanem kimuzsikálják a Gutenberg óta könyvekbe száműzött, >>szív-némaságra<<született s ítélt versekből a maguk olvasata szerinti >>eredeti dallamot<<.”<br />'
            + 'Az együttes alapításakor nem tervezték, hogy külföldi turnékon is részt vesznek majd, csupán a barátaik számára akartak zenélni. 2019-ben azonban több, mint harmincszor léptek át valamilyen országhatárt. Az együttes 51 éves fennállása alatt milyen emlékezetes és izgalmas határátkeléssel összehozható esetek történtek meg az együttes tagjaival? Az együttes egyik alapítótagja, Gryllus Dániel elmeséléséből kiderül.',
    ],
    quote: '',
    image: require('@/assets/images/border_coverImages/Turnesztorik_COVER_KEP.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Turnesztorik_COVER_KEP.jpg'),
    images: [
      {
        src: require('@/assets/images/border_stories/skanzen_gryllusd.mp4'),
        thumbnail: require('@/assets/images/border_coverImages/Turnesztorik_COVER_KEP.jpg'),
        thumbnailWidth: '220px',
      },
    ],
    slug: 'turnesztorik',
    tags: [
      '#ideiglenes határátlépés',
      '#Kaláka együttes',
      '#GryllusDániel',
    ],
    maps: [],
  },
  {
    title: 'Ukrán, román, litván vétkesek',
    lead: 'Volt, aki takarók, más a híd alatt próbálkozott ',
    content: [
      'Zalai Hírlap<br />'
            + '2005. szeptember (61. évfolyam, 204-229. szám)<br />'
            + '2005-09-28 / 227. szám',
      'Nagykanizsa (v. a.) - Hat ukrán, valamint egy litván és egy román állampolgárral szemben intézkedtek a határőrök az elmúlt napokban Rédicsen, illetve Letenyén. Mindannyiukat tiltott határátlépés kísérlete közben fülelték le. <br />'
            + 'A rédicsi határátkelőn a hét végén hármasával akadtak horogra az ukránok. Előbb egy kilépésre jelentkező, litván forgalmi rendszámú kamion utasterének átvizsgálása járt eredménnyel, a vezetőfülke mögötti pihenőhelyen ugyanis takarókkal letakarva három nő rejtőzködött. Őket tiltott határátlépés kísérlete, a teherautót vezető litván sofőrt pedig ember- csempészet alapos gyanúja miatt vonták eljárás alá. Ugyanitt és nem sokkal később a kilépő kamionterminál területén is előállítottak három „kóborló” ukrán nőt: a vizsgálat megállapításai szerint ők is jogsértő módon szerettek volna innen a határ túloldalára, Szlovéniába jutni.<br />'
            + 'Letenyénél a Mura-folyó, illetve az éber járőr akadályozott meg ebben egy román férfit, aki a Horvátországba szökés előtt a város melletti közúti híd alatt próbált bátorságot gyűjteni. A tiltott határ- átlépés kísérletének gyanúja.',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/ukrán_román.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Ukran_roman_vetkesek_COVER_KEP.jpg'),
    slug: 'ukran-roman-litvan-vetkesek',
    tags: [
      '#csempészet',
      '#sajtóanyag',
    ],
    maps: [],
  },
  {
    title: 'Útlevél két szemszínnel',
    lead: 'Mit írjak az útlevembe, ha különböző színűek a szemeim?',
    content: [
      'Tudni kell rólam, hogy a szemeim különböző színűek, az egyik barna a másik zöldes-szürkés-kékes. 1988 óta van útlevelem, és akkoriban az útlevélben is megjelenítették a szem színét. Természetesen a kérőben meg is kellett jelölni. Egyet. Ennek ellenére mi beírtuk a zöldet és a barnát is. Izgatottan vártuk, hogy megjöjjön az útlevél, mi is lesz benne. Beleírták. Mindkettőt.<br />'
            + 'Aztán eljött a nyár, amikor egyik ismerősünk elvitt minket családostul Bécsbe, ahova akkor már lehetett menni vásárolgatni... Amikor mentünk kifelé a határon, útlevélellenőrzés történt. Megnézték az ismerősét, megköszönték, visszaadták. Megnézték édesapámét, megköszönték, visszaadták. Majd édesanyámét, testvéremét, végül maradt az enyém. Megnézték, majd megnézték még egyszer, meg még egyszer, csodálkozón vetették össze a valósággal, vigyorogtak egyet, és visszaadták, jó utat kívántak és elbocsátottak. Szerintem még nem találkoztak különböző színű szeművel. Vagy olyannal, aki beírta.<br />'
            + '(S. P. közlése, a beküldő helyesírását követve)',
      'A borítóképen: Női profil (a kép csupán illusztráció)<br />'
            + 'A kép forrása: Fortepan',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/fortepan_78140.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Utlevel_ket_szemszinnel_COVER_KEP.jpg'),
    slug: 'ultevel-ket-szemszinnel',
    tags: [
      '#útlevél',
      '#ideiglenes határátlépés',
    ],
    maps: [],
  },
  {
    title: 'Üzleti szellem, romantika és humor a csempészésben ',
    lead: 'Vasút, hajó és hátizsák — Adalékok a Csempészet technikájához ',
    content: [
      'Szózat<br />'
            + '1925. február (7. évfolyam, 26-48. szám)<br />'
            + '1925-02-28 / 48. szám',
      '(A Szózat tudósítójától.) Legutóbb ismertettük részletesen a csempészet technikai eszközeit, felsorolva néhány trükköt és fogást, amelyekkel ennek a meglehetősen viruló üzletágnak a mívelői a vámhatóságokat kijátszani igyekeznek. Ezek az ismertetett „modus vivendi“-k, mondanunk sem kell, csak csekély töredékei a csempészek szellemi és fizikai fegyvertárának s ha most olvasóinkat újabb csempészfogásokkal ismertetjük meg, senki se gondolja, hogy ezzel azután az idevonatkozó adatok gyűjteménye lezárult. Annál kevésbé, mert a szakmában egyre új és új „produktumok“ kerülnek forgalomba és legföljebb arról lehet szó, hogy a vámhatóság bővülő tapasztalatai és fokozott ébersége segítségével csökkenti ezeknek az üzelmeknek a számát, azt azonban egyelőre bizony nem várhatjuk, hogy ezek a visszaélések teljesen megszűnjenek. A zsidó leleményesség a vám lefizetésének elkerülésével járó. sokszor igen busás hasznú reménye mindig új és új praktikákat kelt életre, állandó gondot adva derék pénzügyőreinknek.',
      '<b>Schwarz „házi szemlét” tartat</b><br />'
            + 'Vannak szállítócégek, amelyek mindig készen vannak rá, hogy szolgálataikat egyik vagy másik vámiszonyban szenvedő kereskedőnek felajánlják. Ezek tudják is mindig, hogy hová és kihez forduljanak. Rendszerint csak akkor szokták fegyvertársaikat megtagadni, amikor már megvan a baj és az üzleti hírnév, no meg természetesen a lerovandó bírságok által a cég kasszája közvetlen veszedelem fenyegeti. <br />'
            + 'Favorit-rendszere volt egyidőben ezeknek a zúgszállítóknak „a házi szemle“ — nevezzük ezeknek egyikét Schwarcnak, hisz valószínű, hogy akad közöttük ilyen is — szóval ama bizonyos Schwarc a vámhivatalnál egy előkelő cég nevében házi szemlét kér. A házi szemle alatt azt kell érteni, hogy a vámárúkat pénzügyőri kísérettel az illető céghez viszik: ott tartják meg a szemlét, vagyis az árúkat ott vámolják el. Ezt természetesen csak jónevű cégekkel szemben teszi meg a vámhivatal. <br />'
            + 'Schwarc tehát házi szemlét kér és az árúkat lerakatja annak a cégnek az udvarában, amelynek nevében a szemlét kérte. A kiszállt pénzügyőrök az illető cég nevében készített hamis bevallás alapján elvámolták az árút és abban a hiszemben, hogy az illető jónevű cég képviselőjével állnak szemben, a vámbevallást Schwarcnál hagyták, hogy másnap könyveltesse el. Schwarc azután az ekkép vámkezelt árút szépen elszállította - mondjuk a Rombach utcába. <br />'
            + 'A házi szemléhez hasonlatos az előjegyzési raktárra való könyvelés módszere. Ennél is Schwarc működik közre egy kereskedő megbízásából, akinek árúja érkezett a fővámhivatalba, de bizonyos „alaki okok“ folytán nem tud hozzájutni. Schwarc erre megint csak egy előkelő cég nevében hamis bevallást készít és kéri, hogy az árút előjegyzési raktárra könyveljék el. Jónevű és meghízható cégek ugyanis vámhitelt élveznék, ami azt jelenti, hogy a vámot nem tételenként, hanem negyed- vagy félévenként utólag fizetik. Tehát egy ilyen cég számlájára ..könyvelteti el“ Schwarc az árút s így a csalásra csak akkor jönnek rá. amikor a számla kiegyenlítésére kerül a sor és az illető cég tiltakozik az ismeretlen tehertétel ellen.',
      '<b>Berlini zsibvásár — Budapesten </b><br />'
            + 'Annakidején, amikor Németországban a papírpénz zuhanása s mindennemű árú- és értékcikkek forgatagos hossz-ja következtében a középosztály anyagi helyzete szinte máról holnapra omlott össze, csapatostól lepték meg Berlint a Budapeströl fetrándult „kezdő zsidó“ galicianerek. S míg előrehaladottabb fajtestvéreik a magasabb, régiókban tarolták a gazdasági katasztrófa aratását, ők vagonszámra- vásárolták össze azokat a ruhaneműeket, melyeket a középosztályhoz tartozók adogattak el szorult helyzetükben. <br />'
            + 'A „zsákmányt“ azután összecsomagolták, még pedig kisebb-nagyobb kofferekbe, amelyeket kézipodgyász gyanánt adtak fel a vasútra. Az ócskaruhákat tartalmazó kofferek szépen meg is érkeztek Budapestre. <br />'
            + 'Nem volt más hátra, mint azt a kis útipodgyászt a pályaudvarról elszállítani. A koffereket szállító vonatok megérkezése után nagyszámú zsidóifjonc szivárgott be a pályaudvarra, ahol az egyes koffereket mint saját útiholmijukat kiváltották és egyenkint gyorsan elszállítotok. Később gyanús lett ez a feltűnő „zsidógyerek- invázió“ — mint informátorunk mondotta — és a megindult vizsgálat hamarosan kiderítette az ócskaruha-manővert. Kiderült, hogy több vagonra ment a becsempészett ócskaruha mennyisége, amit kofferokba tagolva „hoztak le“ Budapestre. Megállapítást nyert, hogy az ügyet néhány gazdag Teleki téri ószeres finanszírozta.<br />',
      '<b>A boszorkányládák</b></br>'
            + 'Ami a csempészet csakugyan szoros értelemben vett „technikáját“ illeti, vagyis a technikai jellegű fogásokat, a legjellemzőbb példát statuálta erre egy szállító, aki valóságos boszorkányládákat készíttetett a csempészáruk számára. Tíz ilyen ládája volt az élelmes szállítónak, ezek többnyire utón voltak. Olyasfélék voltak ezek a ládák, mint a kettősfenekű kulacs, amelyik, ha elcsavarják a fejét, bor helyett álomitalt ad és viszont. <br />'
            + 'A ládába gyapjufonalat és harisnyát csomagoltak úgy hogy mindegyik ládában felesbe rakták el a kétféle árut. Most már bármelyik oldalról nyitották is ki a ládákat a kiváncsi fináncok, mindenütt csak gyapjufonalat találtak. Tudni kell ugyanis, hogy a gyapjú vámja jóval alacsonyabb, mint a harisnyáé. <br />'
            + 'Amikor a trükkre rájöttek, a szállító ettől kezdve mind a kétféle árút bevallotta, de — hogy a veszett fejszének legalább a nyele maradjon meg — kevesebb harisnyát vallott be, mint gyapjút. Ideig-óráig ez a fogás is bevált.<br />',
      '<b>Csempészet szárazon és vizen</b><br />'
            + 'Nemcsak szállítók űzték azonban ezt a nemes mesterséget, hanem kevésbé merkantil jellegű lények, akik részben megbízásból, részben pedig a saját szakállukra dolgoznak. így a vasúti étkező- és hálókocsik személyzete, A pénzügyőrök erre való tekintettel mindenkor figyelik a veszteglő vágányra tolt kocsikat épp úgy, mint a Budapestre érkező hajókat is. De az utóbbiakat a legnagyobb éberséggel és nem ok nélkül. Mert a hajókon és a hajósok által virágzik a legigazabban a csempészet. A legtöbben nem tudják például, hogy miért mennek a hajók oly gyakran a hajógyárba. Nem annyira javítás céljából, hanem hogy ott rakják ki az árút, vagy ami valószínűbb és gyakoribb — hogy új rejteket építsenek a csempészárú számára. <br />'
            + 'Ugyancsak a hajósok teszik meg, hogy nem Budapesten, hanem Mohácson rakják ki az árút és azután Dunapentelén vagy Érden vasútra adják és így juttatják el a fővárosba, ahol a „haverek“ már várják.  <br />'
            + 'Hirhedt csempész terep az ausztriai határvidék, a megszállt Nyugat-Magyarország határvonala, ahol szinte napról napra fedeznek fel kisebb-nagyobb manővereket. A legkedveltebb módszer az, hogy átcsempészik az árúkat a határvonalon, még pedig előre elkészített postacsomagokban és ezeket a csomagokat valamelyik belföldi postahivatalnál feladva továbbítják. Az így átcsempészett árú többnyire csipke és selyem. Vagy a Fertő tón hozzák át az árúkat és azután szekéren viszik befelé. <br />'
            + 'A trianoni határ falvainak lakossága legnagyobbrészt csempészetből él. Ausztriába bort és egyéb szeszárút, Ausztriából pedig különböző iparcikkeket csempésznek. <br />'
            + 'Sok textilárut hoznak a megszállt Felvidékről, még pedig hátizsákban. Ezeket a gyalogcsempészeket többnyire Budapesten „kapcsolják le“. Akadtak közöttük olyanok, akik budapesti kereskedők számlájával igazolták a náluk levő árút. <br />'
            + 'Egy alkalommal Vaskőnél jöttek át csempészek a határon, akik a hatvani vonaton utaztak tovább. Hatvanban budapesti pénzügyőrök figyelmesek lettek rájuk. Az egyik csempész, amikor látta, hogy mindjárt nyakom csípik, leugrott a robogó vonatról. A pénzügyőr sem volt rest, utána vetette magát, még pedig oly szerencsésen, hogy egyenesen a csempész hátizsákjára pottyant. Ez azonban rendkívül erős fickó volt és vagy ötven lépésre cipelte a hátán a fináncot, azután birkózni kezdett vele és csak nagynehezen sikerült a harcias csempészt leszerelni.',
      '<b>A tolvajnak nézett csempész</b><br />'
            + 'Egy éjjel a Ferencváros valamelyik sötét utcáján posztoló rendőr megállított és igazolásra szólított egy fiatalembert, aki hátán gyanús csomagot cipelt. A gyanús alakot — szabólegény volt az istenadta — előállította az őrszobára, ahol felbontották a csomagot. Hét darab gumikabátot találtak benne. A szabólegény esküdözött, hogy ő nem tolvaj, a kabátokat nem lopta, hanem egy ösmerősétől kapta azokat —kivasalás céljából. <br />'
            + 'Az őrszobán nem hitték el ezt a „mesét“, a szabólegényt ott tartották reggelig, amikor is átvitték a főkapitányságra. Ott igazolták és megállapították, hogy csakugyan ártatlan: a kabátokat egy borbélylegénytől kapta, akinél, azok bizománybán voltak. A további vizsgálat során kiderült, hogy a hét gumiköpeny csempészett árú. Egy hajóslegény hozta Bécsből és a hajóról úgy lopta ki, hogy egy hétig minden nap j kabátban szállt ki a hajóról és kabát nélkül tért vissza.<br />',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/üzleti szellem.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Uzleti_szellem_COVER_KEP.jpg'),
    slug: 'uzleti-szellem-romantika-es-humor-a-csempeszetben',
    tags: [
      '#csempészet',
      '#sajtóanyag',
    ],
    maps: [],
  },
  {
    title: 'Vadnyugat Burgenlandban ',
    lead: '4 hatottja és 22 sebesültje van az osztrák „csempész- frontnak“',
    content: [
      'Uj Nemzedék<br />'
            + '1932. október (14. évfolyam, 220-245. szám)<br />'
            + '1932-10-28 / 243. szám<br />'
            + 'Sopron, október 27. ',
      'Az Osztrák-magyar vámháboru nemcsak ellentétes gazdasági érdekeik harca a zöldasztal mellett, de valóságos háború, amelynek halottai és áldozatai vannak. A halottak a csempészeik guerilla hadseregéből kerültek ki,<br />'
            + '<b>áldozatai pedig sopronmegyei állattenyésztő gazdák, akik nem tudnak egy állatot sem exportálni az osztrák piacra.</b><br />'
            + 'A valamikor messzeföldön híres sopronmegyei állattenyésztés — sorvad, pusztul. <br />'
            + 'Az osztrák-magyar vámháboru kitörése óta mintha a határ nem is két kulturáltam, közt futna, hanem két balkáni országot választana el egymástól. Napról-napra puskaropogás, néha egy- egy halott, nagyon gyakran veres sebesülések, parázs tűzharc az osztrák határőrök között. Az őszi ködbe temetkező határmenti falvak népe ijedten hallgatja az éjszakát felverő puskaropogást, a jajkiáltásokat, a menekülők és üldözők zaját.<br />'
            + '<b>A csempészfront Sopron tájékán kezdődik és tart szakadatlanul Kőszegig.</b><br />'
            + 'Sopron táján bort, Und és Zsira község táján marhákat, Kőszeg körül ugyancsak marhákat csempésznek. A csempészek szervezete kitűnő... Fegyverrel is el vannak látva. Sopron utcáin lépten-nyomon találkozunk a faputtonyos csempészekkel, akik az olcsó, de jó soproni bort szállítják a burgenlandi szomjasaknak. Rengeteg soproni bort exportáltak ezek a smuglerek, úgy hogy a gazdákat meg is szabadították az értékesítés gondjaitól. Mutatkozik is hatása a nagy borcsempészésnek, mert mig azelőtt 30 fillérért mérték a soproni Pónzichter Busenschankokban a bort, addig most 60 filléren alul alig lehet kapni. A város gondjait is enyhítette a csempészek tevékenysége, mert<br />'
            + '<b>a munkanélküliek nem jönnek a város nyakára, hanem felcsapnak csempésznek.</b><br />'
            + 'A soproni borcsempészetnél nagyobb szabású a sopromregyei és vasmegyei állatcsempészés. Marhát és lovat visznek ki. Akinek ilyen nagyobb tőkét igénylő vállalkozáshoz hiányzik a pénze, megelégszik azzal, hogy malacot csempész. A 3 pengős malacot odaát 10 pengőért veszik- Egyesek zsákba rejtve, mások pólyásbabának öltöztetve csempészik ki a malacot. Mind a két esetben fontos szerepet játszik a rum. Ezzel altatják el a malacot, hogy visítása el ne árulja a csempészt. A csempészet meggátlására az osztrákok drákói rendszabályokat léptettek életbe. A fináncok, akik képtelenek voltak feltartóztatni a csempészfrontot,<br />'
            + '<b>segítő csapatokat kaptak, 240 főnyi sorkatonaságot és mintegy 20—25 tisztet.</b><br />'
            + 'Ezek nemcsak manlicherrel,  hanem gépfegyverrel is fel vannak szerelve a burgenlandi, ugyancsak fegyveres csempészbandák ellen. <br />'
            + 'Valóságos embervadászat folyik itt a határon. Az első áldozat Pintér József 33 éves, 3 gyermekes családapa volt. A burgenlandi Somfalvára igyekezett haza. Az osztrák fináncok agyonlőtték. Amikor mohón nekiestek a halott ember hátizsákjának, hogy kikutassák, mit csempészett, 10 kiló uborkát találtak benne. <br />'
            + '10 kiló uborkáért kellett meghalnia 3 gyerek apjának. <br />'
            + 'Az Ausztriához csatolt határmenti községek lakossága, amely az osztrák fináncok brutális eljárása miatt amúgy is fel volt háborodva, forrongani kezdett a somfalvai eset miatt. A temetés napján forradalomszerű tüntetésre gyűlt össze Somfalván a burgenlandi községek elkeseredett lakossága. Az elkeseredés egyre izóbb a burgenlandi lakosság körében, amelyet<br />'
            + '<b>szinte éhhalálra ítélt az osztrákok kíméletlen hajszája.</b><br />'
            + 'A megszállott területi lakosság ugyanis Sopronban szerezte be a zöldséget, baromfit, a tojást és a tejtermékeket, amelyet a bécsi Naschmarkton adott el megfelelő haszon mellett. Ettől most elesett.<br />'
            + 'Alig két hete újabb két halottja volt az osztrák határőrök és csempészek összeütközésének. A burgenlandi Füles község határában lőttek agyon két marhacsempészt, majd Kőszeg táján ért életveszélyes lövés egy burgenlandi gazdát.<br />'
            + '<b>Összesen 4 halott szerepel eddig az osztrák-magyar vámháboru veszteséglistáján.</b><br />'
            + 'Ezek csak a halottak! Jóval nagyobb ennél még a sebesültek száma. 22 sebesülésről kerültek nyilvánosságra az adatok. Közöttük 4—5 osztrák finánc is van, akiket viszont a csempészek lőttek le. <br />'
            + 'Valóságos wild West élet . . . Ember- vadászat életre-halálra. Nemrég a határra kivezényelt osztrák katonaságot felváltották. A katonák ugyanis összejátszottak a csempészekkel — bosszúból. <br />'
            + '<b>A fináncok 10 százalékot kapnak a csempészeknél lefoglalt értékekből, ők egyetlen fillért sem. </b><br />'
            + 'Ezért! Sokat beszélnek mostanában egy csempésztrükkről. A kőszegi erdőnél az osztrák fináncok figyelmesek lettek két csempészre, akik egy vén, csontos tehenet akartak áthajtani a határon. Rajtuk ütöttek ... Alkudozás kezdődött a fináncok és csempészek között, a fináncok azonban hajthatatlanok voltak, bekísérték a csempészeket a tehénnel együtt. Csak erre várt öt másik csempész, akik a fináncok távozása után mintegy<br />'
            + '<b>30 darab gömbölyűre hízott magyar marhát és tejeshusu borjut hajtott át a határon.</b><br />'
            + 'Rablóromantika ... Az osztrák agráriusok mégis hajhatatlanok. Nem veszik észre, hogy konok elzárkózási politikájukkal, Bécs alatt két órányira, Balkánt teremtettek saját hazájukban — puskaropogással, halottakkal, sebesültekkel granicsárok és csempészkomitácsik jajával... Bécs alatt két órányira Európa szivében',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/vadnuygat.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Vadnyugat_burgenlandban_COVER_KEP.jpg'),
    slug: 'vadnyugat-burgenlandban',
    tags: [
      '#csempészet',
      '#sajtóanyag',
    ],
    maps: [],
  },
  {
    title: 'Zoknipénz',
    lead: 'Mire elég 70 USD?',
    content: [
      'Ekkortájt indult be az a „betegség”, amit csak határbetegségnek neveztem el. Már akkor is, de később aztán még inkább, amikor az emberek már nyugat felé is utazhattak. A határ felé közeledve az ember gyomorszája táján egyre fokozódó nyomás alakult ki, amely a határhoz egyre közelebb érve egyre erősebb görcsben nyilvánult meg. A nyugati utakra akkoriban a 30 napos eltávra rengeteg, azaz 70 USD átváltására volt lehetőségünk. Hivatalosan! Azonban a feketepiac működött, és a feketepiaci áron vásárolt dollárokat, márkákat az ember gondosan elrejtette az alsóneműjében vagy a zoknijában. Ha megtalálták, akkor irány haza, pénz veszve, és 15 évig nincs újabb utazás. Ha megúszta az ember, akkor a határon átérve a gyomorgörcs egy pillanat alatt elmúlt.<br />'
            + '(N. Z. közlése, a beküldő helyesírását követve)',
      'A borítóképen: Közúti határellenőrzés Sopronban az 1970-es évek során (a kép csupán illusztráció)<br />'
            + 'A kép forrása: Fortepan',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/fortepan_66757.jpg'),
    coverImage: require('@/assets/images/border_coverImages/Zoknipenz_COVER_KEP.jpg'),
    slug: 'zoknipenz',
    tags: [
      '#határbetegség',
      '#ideiglenes határátlépés',
    ],
    maps: [],
  },
  {
    title: 'Névtelen',
    lead: 'A tipikus rejtekhelyeket a vámőrök könnyen felismerték a rendszerváltás előtt..',
    content: [
      'A kollégám még a rendszerváltás előtt egyszer szolgálatban volt, amikor a kilépő oldalon teljesítette a szolgálatot. Egyszer csak egy román bácsi ment hazafele, kalapban rendesen. Odament a kollégám, köszöntötte, hogy<br />'
      + '– „Jó napot kívánok bátyja!” – és megemelte a sapkáját úgy, ahogy illik.<br />'
      + '– „Hát honnan tetszik jönni?” – mondja, hogy Magyarországról.<br />'
      + '– „Hát tiszteljen már meg maga is azzal, hogy megemeli nekem a kalapját akkor, ha már én is levettem!” Erre csak megemelte a kalapját és egy fél kiló kávé esett ki belőle.<br />'
      + '(N.I. interjúja alapján készült szerkesztett változat)',
      'A borítóképen: Kávéfőző<br />'
      + 'A kép forrása: Fortepan',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/A_TIPIKUS_REJTEKHELYET_COVER_KEP.jpg'),
    coverImage: require('@/assets/images/border_stories/A_TIPIKUS_REJTEKHELYET_COVER_KEP.jpg'),
    slug: 'a-tipikus-rejtekhelyeket',
    tags: [
      '#csempészet',
      '#ideiglenes határátlépés',
    ],
    maps: [],
  },
  {
    title: 'A-ból B-be',
    lead: 'Egy letelepedés története 1980-ban',
    content: [
      'Amíg élek, számon fogom tartani ezt a napot, szeptember tizenhetedikét. Ezerkilencszáznyolcvannyolcban ezen a napon léptem át a román-magyar határt.<br />'
      + 'Tizenöt éves voltam, most pedig negyvenhét, két évvel idősebb, mint amennyi apám volt akkor. <br />'
      + 'Több mint kétszer annyit éltem már itt, mint ott, mégis azóta is és örökre meghatároz az első tizenöt év.<br />',
      'Itt az áhított kivándorló útlevél fényképes oldala. Egy nő adta át, hosszan kevergette a négy útlevelet, mint egy kártyapaklit, és közben arról szónokolt, hogy biztos nem gondoltuk mi jól meg ezt, ki tudja mi fog történni velünk ott Ungáriában. Amíg beszélt, én szépen lakkozott lábkörmeit néztem. “Talia: In crestere.” A szerv igazolta, hogy még növésben vagyok.<br />'
      + 'Mellette ott a vízum, azzal a sokat jelentő szóval "végleg", és a beléptető pecsét, aminek a hajnali csattanását se fogom soha elfelejteni.',
      'A teljes írás a következő : <a href="http://gyorgydragoman.com/?p=600" target="_blank">http://gyorgydragoman.com/?p=600</a><br />'
      + '(Dragomán György írása)',
      '<i>A borítóképen: Dragomán György útlevele</i><br />'
      + '<i>A kép forrása: Dragomán György tulajdona</i>',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/A-BOL-B-BE_COVER_KEP.jpg'),
    coverImage: require('@/assets/images/border_stories/A-BOL-B-BE_COVER_KEP.jpg'),
    slug: 'a-bol-b-be',
    tags: [
      '#végleges határátlépés',
      '#útlevél',
      '#DragománGyörgy',
    ],
    maps: [],
  },
  {
    title: 'Határsértőket fogtak a rendőrök Sarkad külterületén',
    lead: 'A magukat afgán állampolgárnak valló külföldieket előállították',
    content: [
      'BEOL a Békés megyei hírportál<br />'
      + '2020. 09. 22. 08:12<br />'
      + '<a href="https://www.beol.hu/kek-hirek/helyi-kek-hirek/hatarsertoket-fogtak-a-rendorok-sarkad-kulteruleten-3059849/?fbclid=IwAR18R9PRP2aCTXuvTylDK613yOtUigLNQinw0PXXtPjuEi5LvrGRPp-WLXk" target="_blank">https://www.beol.hu/kek-hirek/helyi-kek-hirek/hatarsertoket-fogtak-a-rendorok-sarkad-kulteruleten-3059849/?fbclid=IwAR18R9PRP2aCTXuvTylDK613yOtUigLNQinw0PXXtPjuEi5LvrGRPp-WLXk</a>',
      'Sarkad külterületén, kedden 7 óra 30 perc körül feltartóztattak a rendőrök három külföldit. A férfiak afgán állampolgárnak vallották magukat. Nem tudták hitelt érdemlően igazolni a személyazonosságukat és azt, hogy jogszerűen tartózkodnak Magyarországon. A rendőrjárőrök előállították a határsértőket a Kötegyán Határrendészeti Kirendeltségre, ahol idegenrendészeti eljárás indul ellenük.',
    ],
    quote: '',
    image: require('@/assets/images/border_stories/határsértők.jpg'),
    coverImage: require('@/assets/images/border_coverImages/határsértők.jpg'),
    slug: 'hatarsertoket-fogtak-a-rendorok-sarkad-kulteruleten',
    tags: [
      '#határátlépés',
      '#sajtóanyag',
    ],
    maps: [],
  },
];

export default articles;
