import Vue from 'vue';
import VueRouter from 'vue-router';
import Home from '../views/Home.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Főoldal',
    component: Home,
  },
  {
    path: '/virtualis-kiallitasok',
    name: 'Virtuális kiállítások',
    component: () => import('../views/VirtualExhibition'),
  },
  {
    path: '/az-en-tortenetem/:slug?',
    name: 'Az én történetem',
    component: () => import('../views/MyStory'),
  },
  {
    path: '/a-nagy-haboru-emlekei',
    name: 'A nagy háború emlékei',
    component: () => import('../views/BigWar'),
  },
  {
    path: '/hatartortenetek/:slug?',
    name: 'Határtörténetek',
    component: () => import('../views/BorderStories'),
  },
  {
    path: '/virtualis-kiallitasok/trianoni-arvak/:slug?',
    name: 'Trianon árvái',
    component: () => import('../views/TrianonOrphans'),
  },
  {
    path: '/virtualis-kiallitasok/mutasd-a-maszkod/:slug?',
    name: 'Mutasd a maszkod!',
    component: () => import('../views/ShowYourMask'),
  },
];

const router = new VueRouter({
  mode: 'hash',
  base: process.env.BASE_URL,
  linkActiveClass: 'is-active',
  routes,
  scrollBehavior() {
    return { x: 0, y: 0 };
  },
});

export default router;
