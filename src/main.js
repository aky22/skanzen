import Vue from 'vue';
import VuePlyr from 'vue-plyr';
import VueCarousel from '@chenfengyuan/vue-carousel';
import VueSilentbox from 'vue-silentbox';
import App from './App.vue';
import router from './router';
import store from './store';
import vuetify from './plugins/vuetify';

Vue.config.productionTip = false;
Vue.use(VuePlyr, {
  plyr: {
    fullscreen: { enabled: false },
  },
});

Vue.use(VueCarousel);
Vue.use(VueSilentbox);

new Vue({
  router,
  store,
  vuetify,
  render: (h) => h(App),
}).$mount('#app');
